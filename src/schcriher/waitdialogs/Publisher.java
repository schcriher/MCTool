package schcriher.waitdialogs;

import java.awt.Frame;
import java.util.List;
import java.util.Stack;

import javax.swing.SwingWorker;

public abstract class Publisher extends SwingWorker<Void, Publish> {

	protected final Frame mcparent;
	protected final String title;
	protected final ProgressWaitDialog dialog;

	private final Stack<Integer> minorExtends = new Stack<Integer>();

	public Publisher(Frame mcparent, String title){
		this.mcparent = mcparent;
		this.title = title;
		dialog = new ProgressWaitDialog(this.mcparent, this.title);
	}

	public void setVisible() {
		dialog.setVisible(true);
	}

	public void publish(Publish.Type type) throws InterruptedException {
		publish(type, 1, null);
	}

	public void publish(Publish.Type type, int value) throws InterruptedException {
		publish(type, value, null);
	}

	public void publish(Publish.Type type, String text) throws InterruptedException {
		publish(type, 1, text);
	}

	public void publish(Publish.Type type, int value, String text) throws InterruptedException {
		super.publish(new Publish(type, value, text));
		failIfInterrupted();
	}

    @Override
    protected void process(List<Publish> chunks) {
    	if (dialog.isCancelled()) {
    		cancel(true);
    	}
    	for (Publish chunk: chunks) {
    		switch (chunk.type) {
    		case MAJOR:
    			dialog.setMajorCount(chunk.value);
    			if (chunk.text != null) {
    				dialog.setText(chunk.text);
    			}
    			break;

			case MINOR:
				synchronized (minorExtends) {
					dialog.setMinorCount(chunk.value);
					minorExtends.clear();
					if (chunk.text != null) {
	    				dialog.setSubText(chunk.text);
	    			}
				}
				break;

			case MINOREXTEND:
				synchronized (minorExtends) {
					dialog.extendMinorCount(chunk.value);
					minorExtends.push(chunk.value);
					if (chunk.text != null) {
	    				dialog.setSubText(chunk.text);
	    			}
				}
				break;

			case MAJORSTEP:
				dialog.incrementMajor(chunk.value);
				if (chunk.text != null) {
    				dialog.setText(chunk.text);
    			}
				break;

			case MINORSTEP:
				synchronized (minorExtends) {
					dialog.incrementMinor(chunk.value);
					int offset = chunk.value;
					do {
						if (minorExtends.empty()) {
							offset = 0;
						} else {
							int value = minorExtends.pop() - offset;
							if (value > 0) {
								minorExtends.push(value);
								offset = 0;
							} else {
								offset = -value;
							}
						}
					} while (offset > 0);
					if (chunk.text != null) {
	    				dialog.setSubText(chunk.text);
	    			}
				}
				break;

			case SETINFO:
				dialog.setInfo(chunk.text);
				break;

			case SETTEXT:
				dialog.setText(chunk.text);
				break;

			case SETSUBTEXT:
				dialog.setSubText(chunk.text);
				break;

			case MINORFLUSH:
				synchronized (minorExtends) {
					for (int i = 0; i < chunk.value; i++) {
						if (minorExtends.empty()) {
							dialog.incrementMajor(1);
						} else {
							dialog.incrementMinor(minorExtends.pop());
						}
					}
					dialog.setSubText(chunk.text);
				}
				break;

			case COMPLETE:
				dialog.complete();
				if (chunk.text != null) {
    				dialog.setText(chunk.text);
    			}
    		}
    	}
    }

    @Override
    protected void done() {
    	dialog.dispose();
    }

    private static void failIfInterrupted() throws InterruptedException {
    	if (Thread.currentThread().isInterrupted()) {
    		throw new InterruptedException("Interrupted");
    	}
    }

    @Override
	protected abstract Void doInBackground() throws Exception;
}