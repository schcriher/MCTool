package schcriher.waitdialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Frame;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import schcriher.Resources;

public class WaitDialog extends JDialog implements Runnable {

	private static final long serialVersionUID = 1L;
	private static final boolean modal = true;

	private final JPanel panel;
	private final JLabel text;
	private final JLabel info;

	public WaitDialog(Frame mcparent, String title) {
		super(mcparent, title, modal);

		text = new JLabel(title);
		text.setForeground(Color.LIGHT_GRAY);
		text.setHorizontalAlignment(SwingConstants.CENTER);
		text.setVerticalAlignment(SwingConstants.CENTER);
		Dimension size = text.getPreferredSize();
		int width = size.width + 56;
		size.width = (width < Resources.WIDTH_PROGRESS) ? width : Resources.WIDTH_PROGRESS;
		text.setPreferredSize(size);

		info = new JLabel("« procesando »");
		info.setForeground(Color.GRAY);
		info.setHorizontalAlignment(SwingConstants.CENTER);
		info.setVerticalAlignment(SwingConstants.CENTER);

		panel = new JPanel(new BorderLayout());
		panel.setBorder(new EmptyBorder(18, 18, 18, 18));
		panel.setBackground(Color.DARK_GRAY);
		panel.add(text, BorderLayout.CENTER);
		panel.add(info, BorderLayout.SOUTH);

		getContentPane().add(panel);

		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setUndecorated(true);
		setResizable(false);
		pack();
		setLocationRelativeTo(mcparent);
	}

	public void setText(String t) {
		text.setText(t);
	}

	public void setInfo(String t) {
		info.setText(t);
	}

	@Override
	public void run() {
		setVisible(true);
	}
}