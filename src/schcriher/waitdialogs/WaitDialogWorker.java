package schcriher.waitdialogs;

import java.awt.Frame;
import java.util.List;

import javax.swing.SwingWorker;

import schcriher.generic.TracerProgress;

public class WaitDialogWorker extends SwingWorker<Void, String> implements TracerProgress {

	private final WaitDialog dialog;
	volatile private long total = 0;
	volatile private long current = 0;

	public WaitDialogWorker(Frame mcparent, String string){
		dialog = new WaitDialog(mcparent, string);
	}

	public void setVisible() {
		dialog.setVisible(true);
	}

	@Override
	protected void done() {
		dialog.dispose();
	}

	@Override
	protected void process(List<String> chunks) {
		for (String text: chunks) {
			if (text.startsWith("§")) {
				dialog.setInfo(text.substring(1));
			} else {
				dialog.setText(text);
			}
		}
	}

	@Override
	protected Void doInBackground() throws Exception {
		return null;
	}

	@Override
	public void text(String text) {
		publish(text);
	}

	@Override
	public void count(long count) {
		total = count;
		current = 0;
		setProgress();
	}

	@Override
	public void chuck(long chuck) {
		current += chuck;
		setProgress();
	}

	private void setProgress() {
		publish(String.format("§%d %%", (int)(100.0 * current / total)));
	}
}