package schcriher.waitdialogs;

public class Publish {

	public enum Type {
		MAJOR,
		MINOR,
		MINOREXTEND,
		MAJORSTEP,
		MINORSTEP,
		SETINFO,
		SETTEXT,     //  \ Texto de la barra
		SETSUBTEXT,  //  /
		MINORFLUSH,
		COMPLETE;    // == MAJORFLUSH
	}

	public final int value;
	public final String text;
	public final Type type;

	public Publish(Type type, int value, String text) {
		this.type = type;
		this.value = value;
		this.text = text;
	}
}