package schcriher.waitdialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

import schcriher.LAD;
import schcriher.Resources;
import schcriher.utils.Utils;

public class ProgressWaitDialog extends JDialog implements ActionListener, Runnable {

	private static final long serialVersionUID = 1L;
	private static final boolean modal = true;
	private final Frame mcparent;

	private final JPanel panel;
	private final JLabel title;
	private final JLabel info;
	private final JButton cancel;
	private final JProgressBar bar;

	private String text = "";     //  \ Texto de la barra
	private String subText = "";  //  /
	private String sepText = ": ";

	private final int majorStep = 1_000_000;
	//private int majorCount; -> bar.getMaximum()
	private int minorStep;
	private int minorCount;
	private int localProgress;
	//private int totalProgress; -> bar.getValue()

	volatile private boolean isCancelled;

	public ProgressWaitDialog(Frame mcparent, String strTitle) {
		super(mcparent, strTitle, modal);
		this.mcparent = mcparent;

		panel = new JPanel(new BorderLayout());
		panel.setBorder(new EmptyBorder(10, 10, 10, 10));
		panel.setBackground(Color.DARK_GRAY);

		title = new JLabel(strTitle);
		title.setBorder(new EmptyBorder(0, 0, 4, 0));
		title.setForeground(Color.LIGHT_GRAY);

		bar = new JProgressBar(0, 0);
		bar.setIndeterminate(false);
		bar.setBorderPainted(false);
		bar.setStringPainted(true);
		bar.setString(" ");

		info = new JLabel(" ");
		info.setBorder(new EmptyBorder(4, 0, 0, 0));
		info.setForeground(Color.LIGHT_GRAY);

		cancel = new JButton();
		cancel.setIcon(Resources.PROCESS_STOP_ICON);
		cancel.setToolTipText("Cancelar");
		cancel.addActionListener(this);
		cancel.setBorderPainted(false);
		cancel.setFocusable(false);
		cancel.setMargin(new Insets(0, 5, 0, 0));
		cancel.setBorder(null);

		Utils.setterPreferredSize(title,  Resources.WIDTH_PROGRESS, -1);
		Utils.setterPreferredSize(bar,    Resources.WIDTH_PROGRESS, -1);
		Utils.setterPreferredSize(info,   Resources.WIDTH_PROGRESS, -1);
		Utils.setterPreferredSize(cancel, 30,    -1);

		panel.add(title,  BorderLayout.NORTH);
		panel.add(bar,    BorderLayout.CENTER);
		panel.add(info,   BorderLayout.SOUTH);
		panel.add(cancel, BorderLayout.EAST);

		getContentPane().add(panel);

		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setUndecorated(true);
		setResizable(false);
		pack();
		setLocationRelativeTo(mcparent);
	}

	public boolean isCancelled() {
		return isCancelled;
	}

	public void setText(String str) {
		text = (str == null) ? "" : str;
		setString();
	}

	public void setSubText(String str) {
		subText = (str == null) ? "" : str;
		setString();
	}

	public void setString() {
		bar.setString(text + (subText.equals("") ? "" : sepText) + subText);
	}

	public void setSepText(String str) {
		sepText = (str == null) ? "" : str;
	}

	public void setInfo(String str) {
		info.setText(str);
	}

	public void setMajorCount(int count) {
		bar.setMaximum((count + 1) * majorStep);
		bar.setValue(0);
		localProgress = 0;
		minorCount = 0;
		minorStep = 0;
	}

	public void setMinorCount(int count) {
		if (localProgress > 0 && localProgress < majorStep) {
			bar.setValue(bar.getValue() + (majorStep - localProgress));
		}
		minorCount = count;
		minorStep = majorStep / minorCount;
		localProgress = 0;
	}

	public void extendMinorCount(int count) {
		int base = bar.getValue() - localProgress;
		int n = (localProgress + getOffset()) / minorStep;
		minorCount += count;
		minorStep = majorStep / minorCount;
		localProgress = n * minorStep;
		bar.setValue(base + localProgress);
	}

	public void incrementMajor() {
		incrementMajor(1);
	}

	public void incrementMajor(int n) {
		if (n > 0) {
			int progress = majorStep - localProgress + (n - 1) * majorStep;
			if (localProgress + progress <= bar.getMaximum()) {
				bar.setValue(bar.getValue() + progress);
				localProgress = 0;
			} else {
				throw new RuntimeException(String.format("The major count has been exceeded, n=%s, progress=%s", n, progress));
			}
		}
	}

	public void incrementMinor() {
		incrementMinor(1);
	}

	public void incrementMinor(int n) {
		if (n >= 0) {
			int progress = minorStep * n;
			if (localProgress + progress <= majorStep) {
				localProgress += progress;
				int offset = getOffset();
				localProgress += offset;
				bar.setValue(bar.getValue() + progress + offset);
			} else {
				double n_exceeded = (localProgress + progress - majorStep) / (double)minorStep;
				String error = "The minor count has been exceeded, localProgress=%s, n=%s, progress=%s, n_exceeded=%.3f";
				throw new RuntimeException(String.format(error, localProgress, n, progress, n_exceeded));
			}
		}
	}

	public int getOffset() {
		int offset = majorStep - localProgress;
		return (offset < minorStep) ? offset : 0;
	}

	public int complete() {
		int progress = bar.getMaximum() - bar.getValue();
		bar.setValue(bar.getMaximum());
		localProgress = majorStep;
		return progress;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (LAD.showConfirmDialogYesCancel(mcparent, "¿Realmente desea cancelar?")) {
			if (bar.getValue() < bar.getMaximum()) {
				isCancelled = true;
				cancel.setEnabled(false);
				bar.setEnabled(false);
			}
		}
	}

	@Override
	public void run() {
		setVisible(true);
	}
}