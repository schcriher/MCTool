package schcriher.utils;

import java.awt.Dimension;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.swing.JComponent;

public class Utils {

	public static void setterPreferredSize(JComponent component, int width, int height) {
		Dimension d = component.getPreferredSize();
		if (width > 0) {
			d.width = width;
		}
		if (height > 0) {
			d.height = height;
		}
		component.setPreferredSize(d);
	}

	public static int safeLongToInt(long value) {
		int result = (int) value;
		if (result != value) {
			throw new IllegalArgumentException("The long value " + value + " is not within range of the int type");
		}
		return result;
	}

	public static List<Integer> integerArrayToList(int[] array) {
		List<Integer> list;
		if (array == null) {
			list = new ArrayList<Integer>(0);
		} else {
			list = new ArrayList<Integer>(array.length);
			for (int i : array)
				list.add(i);
		}
		return list;
	}

	public static List<Integer> rangeList(int stop) {
		return rangeList(0, stop);
	}

	public static List<Integer> rangeList(int start, int stop) {
		return integerArrayToList(range(start, stop));
	}

	public static int[] range(int stop) {
		return range(0, stop);
	}

	public static int[] range(int start, int stop) {
		int count = stop - start;
		int[] array = new int[count];
		for (int i = 0; i < count; i++)
			array[i] = start + i;
		return array;
	}

	public static String regexForPartialName(String name, int init) {
		/*
		 * name = development
		 * init = 3
		 * regex = dev(?:(?<=v)e(?:(?<=e)l(?:(?<=l)o(?:(?<=o)p(?:(?<=p)m(?:(?<=m)e(?:(?<=e)n(?:(?<=n)t)?)?)?)?)?)?)?)?
		 *
		 * La regex no contiene ningun grupo capturable
		 */
		StringBuilder regex = new StringBuilder();
		StringBuilder close = new StringBuilder();
		regex.append(name.substring(0, init));
		char c0 = name.charAt(init - 1);
		for (char c1: name.substring(init).toCharArray()) {
			regex.append(String.format("(?:(?<=%s)%s", c0, c1));
			close.append(")?");
			c0 = c1;
		}
		regex.append(close);
		return regex.toString();
	}

	public static enum OS {
		WINDOWS, MACOS, SOLARIS, LINUX, UNKNOWN;
	}

	public static OS getPlatform() {
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.contains("win")) {
			return OS.WINDOWS;
		}
		if (osName.contains("mac")) {
			return OS.MACOS;
		}
		if (osName.contains("linux")) {
			return OS.LINUX;
		}
		if (osName.contains("unix")) {
			return OS.LINUX;
		}
		return OS.UNKNOWN;
	}

	public static File getWorkingDirectory() {
		String userHome = System.getProperty("user.home", ".");
		File workingDirectory = null;

		switch (getPlatform()){
		case SOLARIS:
		case LINUX:
			workingDirectory = new File(userHome, ".minecraft/");
			break;
		case WINDOWS:
			String applicationData = System.getenv("APPDATA");
			String folder = applicationData != null ? applicationData : userHome;
			workingDirectory = new File(folder, ".minecraft/");
			break;
		case MACOS:
			workingDirectory = new File(userHome, "Library/Application Support/minecraft");
			break;
		default:
			workingDirectory = new File(userHome, "minecraft/");
		}

		workingDirectory.mkdirs();
		return workingDirectory;
	}

	// FORGE <= 3.2.3.108 (for MC 1.2.5) contain "minecraftforge_credits.txt"
	// FORGE >= 3.2.4.110 (for MC 1.2.5) contain "MinecraftForge-Credits.txt"
	static final Pattern MCJAR_FORGE = Pattern.compile("minecraftforge.?credits\\.txt", Pattern.CASE_INSENSITIVE);
	static final Pattern MCJSON_FORGE_LIBRARY = Pattern.compile("\\s*\"name\":\\s*\"net.minecraftforge:[^\"]+\".*");
	static final Pattern MCJSON_LAUNCH_WRAPPER = Pattern.compile("\\s*\"mainClass\":\\s*\"net.minecraft.launchwrapper.Launch\".*");

	public static boolean isForgeInstalled(File fVersions) {
		if (fVersions.isDirectory()) {
			for (File d: fVersions.listFiles()) {
				File fjar = new File(d.getPath(), d.getName() + ".jar");
				try (
					FileInputStream     fis = new FileInputStream(fjar);
					BufferedInputStream bis = new BufferedInputStream(fis);
					ZipInputStream      zis = new ZipInputStream(bis)
				){
					boolean isForgeFound = false;
					ZipEntry entry;
					while ((entry = zis.getNextEntry()) != null) {
						if (!entry.isDirectory()) {
							if (MCJAR_FORGE.matcher(entry.getName()).matches()) {
								isForgeFound = true;
								return true;
							}
						}
					}
					if (!isForgeFound) {
						File fjson = new File(d, d.getName() + ".json");
						try (
							FileReader     fr = new FileReader(fjson);
							BufferedReader br = new BufferedReader(fr);
						){
							boolean isLaunchWrapperFound = false;
							boolean isForgeLibraryFound = false;
							String linea;
							while((linea = br.readLine()) != null) {
								if (MCJSON_LAUNCH_WRAPPER.matcher(linea).matches()) {
									isLaunchWrapperFound = true;
								}
								else if (MCJSON_FORGE_LIBRARY.matcher(linea).matches()) {
									isForgeLibraryFound = true;
								}
								if (isLaunchWrapperFound && isForgeLibraryFound) {
									return true;
								}
							}
						}
						catch (Exception e) {}
					}
				}
				catch (Exception e) {}
			}
		}
		return false;
	}

	public static boolean useWatchService(File file) {
		if (getPlatform() == OS.WINDOWS) {
			char drive = file.toString().toUpperCase().charAt(0);
			String hdsn = getWindowsHardDiskSerialNumber(drive);
			try {
				if (Integer.parseInt(hdsn) != 0) {
					return true;
				}
			} catch(Exception e) {}
			return false;
		} else {
			return true;
		}
	}

	private static String getWindowsHardDiskSerialNumber(char drive) {
		// http://www.rgagnon.com/javadetails/java-0580.html
		String result = "";
		try {
			File file = File.createTempFile("whdsn", ".vbs");
			FileWriter fw = new java.io.FileWriter(file);
			fw.write("Set objFSO = CreateObject(\"Scripting.FileSystemObject\")\n");
			fw.write("Set colDrives = objFSO.Drives\n");
			fw.write("Set objDrive = colDrives.item(\"" + drive + "\")\n");
			fw.write("Wscript.Echo objDrive.SerialNumber");
			fw.close();
			Process p = Runtime.getRuntime().exec("cscript //NoLogo " + file.getPath());
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while ((line = input.readLine()) != null)
				result += line;
			input.close();
			if (!file.delete())
				file.deleteOnExit();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result.trim();
	}
}