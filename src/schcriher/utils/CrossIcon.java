package schcriher.utils;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

import javax.swing.Icon;

public class CrossIcon implements Icon {

	private final int size;
	private final int[] xPoints;
	private final int[] yPoints;

	public CrossIcon(int size) {
		this.size = size;

		double P1 = 1.0/8;
		double P2 = 2.0/8;
		double P3 = 3.0/8;
		double P4 = 4.0/8;
		double P5 = 5.0/8;
		double P6 = 6.0/8;
		double P7 = 7.0/8;

		int s = size - 1;

		int p1 = (int) Math.round(s * P1);
		int p2 = (int) Math.round(s * P2);
		int p3 = (int) Math.round(s * P3);
		int p4 = (int) Math.round(s * P4);
		int p5 = (int) Math.round(s * P5);
		int p6 = (int) Math.round(s * P6);
		int p7 = (int) Math.round(s * P7);

		xPoints = new int[] {p1, p3, p1, p2, p4, p6, p7, p5, p7, p6, p4, p2, p1};
		yPoints = new int[] {p2, p4, p6, p7, p5, p7, p6, p4, p2, p1, p3, p1, p2};
	}

	@Override
	public int getIconWidth() {
		return size;
	}

	@Override
	public int getIconHeight() {
		return size;
	}

	@Override
	public void paintIcon(Component c, Graphics g, int x, int y) {
		g.translate(x, y);
		boolean enabled = c.isEnabled();

		g.setColor(enabled ? Color.PINK : Color.GRAY);
		g.drawPolygon(xPoints, yPoints, xPoints.length);

		g.setColor(enabled ? Color.RED : Color.DARK_GRAY);
		g.fillPolygon(xPoints, yPoints, xPoints.length);

		g.translate(-x, -y);
	}
}