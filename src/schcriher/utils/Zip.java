package schcriher.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import schcriher.Resources;
import schcriher.generic.DummyTracerProgress;
import schcriher.generic.TracerProgress;

public class Zip {
	private static final String CNAME = Zip.class.getName();
	private static final Logger LOGGER = Logger.getLogger(CNAME);

	public static boolean compressor(File fIn, File fOut) {
		return compressor(fIn, fOut, new DummyTracerProgress(), false);
	}

	public static boolean compressor(File fIn, File fOut, TracerProgress tracer) {
		return compressor(fIn, fOut, tracer, false);
	}

	private static boolean compressor(File fIn, File fOut, TracerProgress tracer, boolean retry) {
		boolean success;
		try (
			final FileOutputStream     fos = new FileOutputStream(fOut);
			final BufferedOutputStream bos = new BufferedOutputStream(fos);
			final ZipOutputStream      zos = new ZipOutputStream(bos);
		){
			if (!(tracer instanceof DummyTracerProgress))
				tracer.count(Archives.getFullSize(fIn));

			if (fIn.isDirectory()) {
				for (File f: fIn.listFiles()) {
					compressor(f, "", zos, tracer);
				}
			} else {
				compressor(fIn, "", zos, tracer);
			}
			success = fOut.exists();
		}
		catch (Exception e) {
			LOGGER.throwing(CNAME, "compressor", e);
			success = false;
		}
		LOGGER.fine(String.format("compressor %b (retry %b): %s > %s", success, retry, fIn, fOut));
		if (!success && !retry) {
			Archives.recursively(fIn, Archives.Operation.SET_READABLE);
			Archives.recursively(fOut, Archives.Operation.DELETE);
			return compressor(fIn, fOut, tracer, true);
		}
		return success;
	}

	private static void compressor(File file, String path, ZipOutputStream zos, TracerProgress tracer) throws IOException {
		path = path + file.getName() + (file.isDirectory() ? "/" : "");
		ZipEntry entry = new ZipEntry(path);
		entry.setTime(file.lastModified());
		zos.putNextEntry(entry);

		if (file.isDirectory()) {
			for (File f: file.listFiles()) {
				compressor(f, path, zos, tracer);
			}
		}
		else {
			byte[] buffer = new byte[Resources.BUFFER];
			int read;
			try (
				FileInputStream     fis = new FileInputStream(file);
				BufferedInputStream bis = new BufferedInputStream(fis);
			){
				while ((read = bis.read(buffer)) > 0) {
					zos.write(buffer, 0, read);
					tracer.chuck(read);
				}
			}
			catch (Exception e) {
				throw e;
			}
		}
	}

	public static boolean decompressor(File fIn, File fOut) {
		return decompressor(fIn, fOut, new DummyTracerProgress(), false);
	}

	public static boolean decompressor(File fIn, File fOut, TracerProgress tracer) {
		return decompressor(fIn, fOut, tracer, false);
	}

	private static boolean decompressor(File fIn, File fOut, TracerProgress tracer, boolean retry) {
		boolean success = true;
		try (
			final FileInputStream     fis = new FileInputStream(fIn);
			final BufferedInputStream bis = new BufferedInputStream(fis);
			final ZipInputStream      zis = new ZipInputStream(bis);
		){
			if (!(tracer instanceof DummyTracerProgress))
				tracer.count(fIn.length());

			byte[] buffer = new byte[Resources.BUFFER];
			ZipEntry entry;
			File file;
			while ((entry = zis.getNextEntry()) != null) {
				String path = entry.getName();
				if (!path.equals("")) {
					file = new File(fOut, path);
					if (entry.isDirectory()) {
						file.mkdirs();
					} else {
						file.getParentFile().mkdirs();
						try (
							final FileOutputStream     fos = new FileOutputStream(file);
							final BufferedOutputStream bos = new BufferedOutputStream(fos);
						){
							int read;
							while ((read = zis.read(buffer)) > 0) {
								bos.write(buffer, 0, read);
								tracer.chuck(read);
							}
						}
					}
					file.setLastModified(entry.getTime());
				}
			}
		}
		catch (Exception e) {
			success = false;
			LOGGER.throwing(CNAME, "decompressor", e);
		}
		LOGGER.fine(String.format("decompressor %b (retry %b): %s > %s", success, retry, fIn, fOut));
		if (!success && !retry) {
			Archives.recursively(fIn, Archives.Operation.SET_READABLE);
			Archives.recursively(fOut, Archives.Operation.DELETE);
			return decompressor(fIn, fOut, tracer, true);
		}
		return success;
	}
}