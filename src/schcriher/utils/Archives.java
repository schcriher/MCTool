package schcriher.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Logger;

import schcriher.Resources;
import schcriher.generic.AlphabeticalOrder;

public class Archives {

	private static final String CNAME = Archives.class.getName();
	private static final Logger LOGGER = Logger.getLogger(CNAME);

	public static String extractExtension(String name) {
		int offset = getExtension(name).length() + 1; // '1' por el punto
		int length = name.length();
		int end = offset > 1 ? length - offset : length;
		return name.substring(0, end);
	}

	public static String getExtension(String path) {
		return getNameExtension(new File(path).getName());
	}

	public static String getExtension(File path) {
		return getNameExtension(path.getName());
	}

	public static String getNameExtension(String name) {
		String extension = "";
		int i = name.lastIndexOf('.');
		if (i > 0 &&  i < name.length() - 1) {
		    extension = name.substring(i + 1);
		}
		return extension;
	}

	public static long getFullSize(File file) {
		if (file.isDirectory()) {
			long size = 0L;
			for (File f: file.listFiles()) {
				size += getFullSize(f);
			}
			return size;
		}
		return file.length();
	}

	public static enum Operation {
		SET_READABLE,
		SET_WRITABLE,
		SET_EXECUTE,
		DELETE;
		public boolean make(File file) {
			boolean success = false;
			try {
				switch (this) {
				case SET_READABLE: success = file.setReadable(true);   break;
				case SET_EXECUTE:  success = file.setExecutable(true); break;
				case SET_WRITABLE: success = file.setWritable(true);   break;
				case DELETE:       success = file.delete();            break;
				}
			}
			catch (Exception e) {
				success = false;
			}
			LOGGER.fine(String.format("%s %b: %s", name(), success, file));
			return success;
		}
	}

	public static boolean recursively(File file, Operation operation) {
		if (file.isDirectory()) {
			for (File f: file.listFiles()) {
				if (!recursively(f, operation)) {
					return false;
				}
			}
		}
		return operation.make(file);
	}

	public static boolean recursivelyWitoutOwner(File directory, Operation operation) {
		for (File f: directory.listFiles()) {
			if (!recursively(f, operation)) {
				return false;
			}
		}
		return true;
	}

	public static boolean move(File src, File dst) {
		return move(src, dst, false);
	}

	public static boolean move(File src, File dst, boolean force) {
		if (!src.equals(dst)) {
			return move(src, dst, force, false);
		} else {
			return false;
		}
	}

	private static boolean move(File src, File dst, boolean force, boolean retry) {
		if (force) {
			recursively(dst, Operation.DELETE);
		} else if (dst.exists()) {
			LOGGER.fine(String.format("move false (by no force: already exist): %s > %s", src, dst));
			return false;
		}
		boolean success;
		try {
			Files.move(src.toPath(), dst.toPath());
			success = dst.exists();
		} catch (Exception e) {
			success = false;
		}
		if (!success && !retry) {
			recursively(src, Operation.SET_READABLE);
			recursively(src, Operation.SET_WRITABLE);
			return move(src, dst, force, true);
		}
		LOGGER.fine(String.format("move %b%s: %s > %s", success, retry ? " (retry)" : "", src, dst));
		return success;
	}

	public static boolean copy(File src, File dst) {
		return copy(src, dst, false);
	}

	public static boolean copy(File src, File dst, boolean force) {
		if (!src.equals(dst)) {
			return copy(src, dst, force, false);
		} else {
			return false;
		}
	}

	private static boolean copy(File src, File dst, boolean force, boolean retry) {
		if (force) {
			recursively(dst, Operation.DELETE);
		} else if (dst.exists()) {
			LOGGER.fine(String.format("copy false (by no force: already exist): %s > %s", src, dst));
			return false;
		}
		boolean success;
		try {
			Files.copy(src.toPath(), dst.toPath());
			success = true;
		} catch (Exception e) {
			success = false;
		}
		if (!success && !retry) {
			recursively(src, Operation.SET_READABLE);
			recursively(src, Operation.SET_WRITABLE);
			return copy(src, dst, force, true);
		}
		LOGGER.fine(String.format("copy %b%s: %s > %s", success, retry ? " (retry)" : "", src, dst));
		return success;
	}

	public static boolean equalFiles(File fA, File fB) {
		boolean match = false;
		try (
			FileInputStream      fisA = new FileInputStream(fA);
			FileInputStream      fisB = new FileInputStream(fB);
			BufferedInputStream  bisA = new BufferedInputStream(fisA);
			BufferedInputStream  bisB = new BufferedInputStream(fisB);
		){
			byte[] bufferA = new byte[Resources.BUFFER];
			byte[] bufferB = new byte[Resources.BUFFER];
			int readA;
			int readB;
			do {
				readA = bisA.read(bufferA);
				readB = bisB.read(bufferB);
				match = readA == readB && Arrays.equals(bufferA, bufferB);
			}
			while (match && readA > 0);
		}
		catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return match;
	}

	private static SortedSet<String> listAllRelPathFiles(File dir) {
		SortedSet<String> relPaths = new TreeSet<String>(new AlphabeticalOrder<String>());
		Queue<File> folders = new LinkedList<File>();
		Path base = dir.toPath();

		folders.add(dir);
		while (!folders.isEmpty()) {
			for (File f: folders.poll().listFiles()) {
				if (f.isDirectory()) {
					folders.add(f);
				} else if (f.isFile()) {
					relPaths.add(base.relativize(f.toPath()).toString());
				}
			}
		}
		return relPaths;
	}

	public static boolean equalFolders(File fA, File fB) {
		SortedSet<String> ss1 = listAllRelPathFiles(fA);
		SortedSet<String> ss2 = listAllRelPathFiles(fB);
		if (ss1.equals(ss2)) {
			File fa, fb;
			for (String subPath: ss1) {
				fa = new File(fA, subPath);
				fb = new File(fB, subPath);
				if (!equalFiles(fa, fb)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public static void emptyFolderRemove(File dir) {
		List<File> folders = new ArrayList<File>();
		Queue<File> trace = new LinkedList<File>();

		folders.add(dir);
		trace.add(dir);

		while (!trace.isEmpty()) {
			for (File f: trace.poll().listFiles()) {
				if (f.isDirectory()) {
					trace.add(f);
					folders.add(f);
				}
			}
		}

		Collections.reverse(folders);
		for (File f: folders) {
			if (f.listFiles().length == 0) {
				Operation.DELETE.make(f);
			}
		}
	}
}