package schcriher.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import schcriher.Resources;

public class MD5 {

	public static String getHex(String string) throws NoSuchAlgorithmException {
		return byteToHex(getByte(string));
	}

	public static byte[] getByte(String string) throws NoSuchAlgorithmException {
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		return md5.digest(string.getBytes(Charset.forName("UTF8")));
	}

	public static String getHex(File file) throws NoSuchAlgorithmException, IOException {
		return byteToHex(getByte(file));
	}

	public static byte[] getByte(File file) throws NoSuchAlgorithmException, IOException {
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		byte[] buffer = new byte[Resources.BUFFER];
		int n;
		try (
			FileInputStream     fis = new FileInputStream(file);
			BufferedInputStream bis = new BufferedInputStream(fis);
		){
			while ((n = bis.read(buffer)) > 0)
				md5.update(buffer, 0, n);
		}
		return md5.digest();
	}

	public static String byteToHex(byte[] bytes) {
		final StringBuilder hex = new StringBuilder(2 * bytes.length);
		for (byte b: bytes) {
			hex.append(String.format("%02x", b));
		}
		return hex.toString();
	}
}