package schcriher.utils;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

import javax.swing.Icon;
import javax.swing.SwingConstants;

public class ArrowIcon implements Icon, SwingConstants {

	private final int width;
	private final int height;
	private final int direction;

	private final int[] xPoints;
	private final int[] yPoints;

	public ArrowIcon(int width, int height, int direction) {
		this.width = width;
		this.height = height;
		this.direction = direction;

		double X1 =  4.0/27;
		double X2 = 11.0/27;
		double X3 = 16.0/27;
		double X4 = 23.0/27;

		double Y1 =  7.0/22;
		double Y2 = 11.0/22;
		double Y3 = 15.0/22;

		int w = width - 1;
		int h = height - 1;

		int x0 = 0;
		int x1 = (int) Math.round(w * X1);
		int x2 = (int) Math.round(w * X2);
		int x3 = (int) Math.round(w * X3);
		int x4 = (int) Math.round(w * X4);
		int xm = w;

		int y0 = 0;
		int y1 = (int) Math.round(h * Y1);
		int y2 = (int) Math.round(h * Y2);
		int y3 = (int) Math.round(h * Y3);
		int ym = h;

		if (direction == EAST) {
			xPoints = new int[] {x0+1, x1, x0, x2, x1, x3, xm, x3+1, x1+1, x2+1, x0+1};
			yPoints = new int[] {y1+1, y2, y3, y3, ym, ym, y2, y0+1, y0+1, y1+1, y1+1};
		}
		else if (direction == WEST) {
			xPoints = new int[] {x0+1, x2+1, x4+1, x3+1, xm+1, x4+1, xm,   x3,   x4,   x2,   x0+1};
			yPoints = new int[] {y2,   ym,   ym,   y3,   y3,   y2,   y1+1, y1+1, y0+1, y0+1, y2  };
		}
		else {
			throw new RuntimeException("Not implemented the direction " + direction);
		}
	}

	public int getDirection() {
		return direction;
	}

	@Override
	public int getIconWidth() {
		return width;
	}

	@Override
	public int getIconHeight() {
		return height;
	}

	@Override
	public void paintIcon(Component c, Graphics g, int x, int y) {
		if (c.isEnabled()) {
			g.setColor(c.getForeground());
		} else {
			g.setColor(Color.LIGHT_GRAY);
		}
		g.translate(x, y);
		g.fillPolygon(xPoints, yPoints, xPoints.length);
		g.translate(-x, -y);
	}
}