package schcriher.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Combinations<T> implements Iterable<List<T>>, Iterator<List<T>> {

	private final List<T> pool;
	private final int[] indices;
	private final int n;
	private final int r;
	private int end;

	public Combinations(List<T> pool, int r) {
		if (r < 0)
			throw new IllegalArgumentException("r must be greater than zero");
		this.pool = pool;
		this.n = pool.size();
		this.r = r;
		this.end = n > 0 ? 1 : 0;
		this.indices = new int[r];
		for (int i = 0; i < r; i++)
			this.indices[i] = i;
	}

	private List<T> getCombination() {
		List<T> com = new ArrayList<T>(r);
		for (int i: indices) {
			com.add(pool.get(i));
		}
		return com;
	}

	@Override
	public boolean hasNext() {
		return !(end == 0 && indices[r - 1] == n || r > n);
	}

	/*  https://docs.python.org/3.2/library/itertools.html#itertools.combinations
		def combinations(iterable, r):
		    # combinations('ABCD', 2) --> AB AC AD BC BD CD
		    # combinations(range(4), 3) --> 012 013 023 123
		    pool = tuple(iterable)
		    n = len(pool)
		    if r > n:
		        return
		    indices = list(range(r))
		    yield tuple(pool[i] for i in indices)
		    while True:
		        for i in reversed(range(r)):
		            if indices[i] != i + n - r:
		                break
		        else:
		            return
		        indices[i] += 1
		        for j in range(i+1, r):
		            indices[j] = indices[j-1] + 1
		        yield tuple(pool[i] for i in indices)
	 */

	@Override
	public List<T> next() {
		List<T> com = getCombination();
		end = r - 1;
		while (indices[end] == end + n - r) {
			if (end == 0)
				break;
			end--;
		}
		indices[end]++;
		for (int j = end + 1; j < r; j++) {
			indices[j] = indices[j - 1] + 1;
		}
		return com;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Iterator<List<T>> iterator() {
		return new Combinations<T>(pool, r);
	}
}