package schcriher.mods;

import java.awt.Component;
import java.io.File;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class FileNameCellRenderer extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 1L;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		value = ((File)value).getName();
		return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	};
}