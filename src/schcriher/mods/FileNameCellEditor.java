package schcriher.mods;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

import schcriher.LAD;
import schcriher.utils.Archives;

public class FileNameCellEditor extends AbstractCellEditor implements TableCellEditor {

	private static final long serialVersionUID = 1L;
	private final JTextField textField;
	private final Mods mods;
	private String extwp;
	private File file;

	public FileNameCellEditor(Mods mods) {
		this.mods = mods;
		textField = new JTextField();
		textField.setBorder(null);
	}

	// TableCellEditor

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int rowIndex, int columnIndex) {
		if (!isSelected) {
            return null;
        }
		file = (File)value;
		extwp = "." + Archives.getExtension(file);
		textField.setText(file.getName());
		return textField;
	}

    // AbstractCellEditor

	@Override
	public boolean isCellEditable(EventObject e) {
        if (e instanceof MouseEvent) {
            return ((MouseEvent)e).getClickCount() >= 2;
        }
        return true;
    }

    @Override
    public boolean stopCellEditing() {
    	String newName = getNewName();
    	File newFile = new File(file.getParentFile(), newName);
    	if (!newName.equals(file.getName()) && newFile.exists()) {
    		LAD.showNameErrorMessage(mods.core.mcparent, newName);
    		return false;
    	}
    	fireEditingStopped();
    	return true;
    }

    private String getNewName() {
    	String name = textField.getText();
		if (extwp.length() > 1) { // 1='.'
			if (!name.toLowerCase().endsWith(extwp.toLowerCase())) {
				name = name + extwp;
				textField.setText(name);
			}
		}
		return name;
	}

	@Override
    public Object getCellEditorValue() {
    	String newName = getNewName();
    	if (!newName.equals(file.getName())) {
    		File newFile = new File(file.getParentFile(), newName);
    		if (Archives.move(file, newFile, true)) {
    			file = newFile;
    		} else {
    			LAD.showRenameErrorMessageNotLog(mods.core.mcparent, file, newFile);
    		}
    	}
    	return file;
    }
}