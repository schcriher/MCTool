package schcriher.mods;

import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.JTable;

import schcriher.Resources;

public class ModsTable extends JTable {

	private static final long serialVersionUID = 1L;
	private final ModsTableModel model;

	public ModsTable(ModsTableModel model) {
		super(model);
		this.model = model;
	}

	@Override
	public String getToolTipText(MouseEvent e) {
		String tip = null;
		try {
			Point p = e.getPoint();
			int rowIndex = rowAtPoint(p);
			if (rowIndex >= 0) {
				int columnIndex = columnAtPoint(p);
				String path = model.getFile(rowIndex).getPath();
				boolean lock = model.getLockRow(rowIndex);

				if (columnIndex == model.getColumnIndex("AVAILABLE")) {
					String l = lock ? Resources.LOCKED_BY_ERRORS : "";
					String d = model.getAvailable(rowIndex) ? "Disponible en Minecraft" : "No disponible en Minecraft";
					tip = String.format("<html>%s%s</html>", l, d);
				}
				else if (columnIndex == model.getColumnIndex("FILE")) {
					String l = lock ? Resources.LOCKED_BY_ERRORS : "Doble click para editar el nombre<br/>";
					tip = String.format("<html>%s<i>%s</i></html>", l, path);
				}
			}
		}
		catch (Exception ex) {}
		return tip;
	}
}