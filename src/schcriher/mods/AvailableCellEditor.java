package schcriher.mods;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellEditor;

public class AvailableCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {

	private static final long serialVersionUID = 1L;
	private final JCheckBox checkBox;
	private final Mods mods;
	private int rowIndex;

	public AvailableCellEditor(Mods mods) {
		this.mods = mods;
		checkBox = new JCheckBox();
		checkBox.addActionListener(this);
		checkBox.setBorderPainted(false);
		checkBox.setHorizontalAlignment(SwingConstants.CENTER);
	}

	// TableCellEditor

	@Override  /* #1 */
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		checkBox.setSelected((boolean) value);
		rowIndex = row;
		return checkBox;
	}

	// ActionListener

	@Override  /* #2 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == checkBox) {
			boolean available = checkBox.isSelected();
			boolean success = mods.setAvailable(available, rowIndex);
			if (!success) {
				checkBox.setSelected(!available);
			}
			fireEditingStopped();
		}
	}

	// AbstractCellEditor

	@Override  /* #3 */
	public Object getCellEditorValue() {
		return checkBox.isSelected();
	}
}