package schcriher.mods;

import java.awt.CardLayout;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import schcriher.DuplicateRemover;
import schcriher.Exporter;
import schcriher.LAD;
import schcriher.Resources;
import schcriher.Schcriher;
import schcriher.generic.AlphabeticalOrder;
import schcriher.generic.UnselectionTableMouseListener;
import schcriher.utils.Archives;

public class Mods extends JPanel implements ListSelectionListener {

	// http://minecraft.gamepedia.com/Mods/Installing_forge_mods

	// TODO Agregar boton para desactivar/activar todos los mods (al desactivar recordar los que estaban activados)
	// Aplicar lo mismo a las tres solapas, usar como boton el encabezado de la tabla, cambiar el "§" por una "D"

	private static final long serialVersionUID = 1L;

	private static final String CNAME = Mods.class.getName();
	private static final Logger LOGGER = Logger.getLogger(CNAME);

	private ModsTableModel model;
	private ModsTable table;

	public final Schcriher core;

    public Mods(Schcriher core) {
    	long start = System.currentTimeMillis();
    	this.core = core;
        initComponents();
        listFiles();
        LOGGER.fine(String.format("Schcriher Mods Initialized in %dms", System.currentTimeMillis() - start));
    }

    public void listFiles() {
    	model.clear();
    	try {
    		listMods(core.fMods, true);
    		listMods(core.fModsBackups, false);
    	}
    	catch (Exception e) {
    		LAD.showErrorMessage(core.mcparent, e.getLocalizedMessage());
    	}
    }

	private void listMods(File file, boolean enable) throws Exception {
		if (!file.exists()) {
    		file.mkdirs();
    	}
		if (!file.isDirectory()) {
    		throw new IOException("Could not create the folder " + file.getAbsolutePath());
    	}
		List<File> files = Arrays.asList(file.listFiles(new ModsFileFilter()));
		Collections.sort(files, new AlphabeticalOrder<File>());
		for (File f: files) {
			// Se asume que son todos Mods válidos, el boton de importación hace las comprobaciones
			model.addRow(enable, f, false);
		}
	}

	private void initComponents() {
		CardLayout layoutMods = new CardLayout(0, 0);
		setLayout(layoutMods);

		model = new ModsTableModel();
		table = new ModsTable(model);
		table.addMouseListener(new UnselectionTableMouseListener());
		table.setFillsViewportHeight(true);
		table.setGridColor(Resources.TABLE_GRID_COLOR);
		table.setShowGrid(true);
		table.setShowVerticalLines(false);
		table.setRowSelectionAllowed(true);
		table.setColumnSelectionAllowed(false);
		table.getSelectionModel().addListSelectionListener(this);
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setMaxWidth(27);
		table.getColumnModel().getColumn(0).setCellEditor(new AvailableCellEditor(this));
		table.getColumnModel().getColumn(1).setCellEditor(new FileNameCellEditor(this));
		table.getColumnModel().getColumn(1).setCellRenderer(new FileNameCellRenderer());
		add(new JScrollPane(table), "tableMods");

		JLabel label = new JLabel("<html><body><center>Debe instalar <b>Forge</b> para utilizar esta función<br />" +
				"<a href=http://files.minecraftforge.net/ style='text-decoration:none;'>http://files.minecraftforge.net/</a>" +
				"</center><br /><br /></body></html>");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setVerticalAlignment(SwingConstants.CENTER);
		add(label, "labelMods");

		layoutMods.show(this, core.forgeInstalled ? "tableMods" : "labelMods");
	}

	public String getName(int rowIndex) {
		return model.getFile(rowIndex).getName();
	}

    public boolean setAvailable(boolean available, int rowIndex) {
    	if (!model.getLockRow(rowIndex)) {
    		File oldFile = model.getFile(rowIndex);
    		File newFile = new File(available ? core.fMods : core.fModsBackups, oldFile.getName());
    		if (!oldFile.equals(newFile)) {
    			if (Archives.move(oldFile, newFile, true)) {
    				model.setAvailable(available, rowIndex);
    				model.setFile(newFile, rowIndex);
    				return true;
    			} else {
    				model.setLockRow(true, rowIndex);
    				LAD.showRenameErrorMessageNotLog(core.mcparent, oldFile, newFile);
    			}
    		}
    	}
    	return false;
    }

    public void deleteRepeated() {
    	List<File> files = new ArrayList<File>();
    	int n = model.getRowCount();
    	for (int i = 0; i < n; i++)
    		files.add(model.getFile(i));
    	DuplicateRemover remover = new DuplicateRemover(core.mcparent, files);
    	remover.execute();
    	remover.setVisible();
    	if (remover.removed()) {
    		listFiles();
    	}
    }

    public void deleteSelected() {
    	if (LAD.showConfirmDialogYesNo(core.mcparent, "¿Eliminar los archivos seleccionados?")) {
    		List<Integer> deleteRowIndexes = new ArrayList<Integer>();
    		List<String> nonDelete = new ArrayList<String>();

    		for (int i: table.getSelectedRows()) {
    			File file = model.getFile(i);
    			if (Archives.Operation.DELETE.make(file)) {
    				deleteRowIndexes.add(i);
    			} else {
    				model.setLockRow(true, i);
    				nonDelete.add(file.getPath());
    			}
    		}
    		model.removeRows(deleteRowIndexes);

    		if (!nonDelete.isEmpty()) {
    			nonDelete.add(0, "Los siguientes archivos no se pudieron eliminar:");
    			nonDelete.add(1, "");
    			LAD.showErrorMessage(core.mcparent, nonDelete);
    		}
    	}
    }

    public void exportSelected(File dst) {
    	final List<File> src = new ArrayList<File>();
    	for (int i: table.getSelectedRows()) {
    		src.add(model.getFile(i));
    	}
    	Exporter exporter = new Exporter(core.mcparent, dst, src);
    	exporter.execute();
    	exporter.setVisible();
    }

	@Override
	public void valueChanged(ListSelectionEvent e) {
		core.setEnabledExportButton(table.getSelectedRowCount() > 0);
	}
}