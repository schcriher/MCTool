package schcriher.mods;

import java.io.File;
import java.io.FileFilter;

public class ModsFileFilter implements FileFilter {

	@Override
	public boolean accept(File file) {
		if (file.isFile()) {
			String name = file.getName().toLowerCase();
			return name.endsWith(".jar") || name.endsWith(".zip");
		}
		return false;
	}
}