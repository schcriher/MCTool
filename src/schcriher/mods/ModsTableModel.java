package schcriher.mods;

import java.io.File;

import schcriher.generic.TableModel;

public class ModsTableModel extends TableModel {

	private static final long serialVersionUID = 1L;

	public ModsTableModel() {
		super(new Object[][] {
				{"AVAILABLE", " §",                                        Boolean.class, true,  true},
				{"FILE",      "Nombre de los archivos de mods para Forge", File.class,    true,  true},
				{"LOCKROW",   "",                                          Boolean.class, false, false},
		});
	}

	// Getter

	public boolean getAvailable(int rowIndex){
		return (boolean)getter(rowIndex, "AVAILABLE");
	}

	public File getFile(int rowIndex){
		return (File)getter(rowIndex, "FILE");
	}

	public boolean getLockRow(int rowIndex){
		return (boolean)getter(rowIndex, "LOCKROW");
	}

	// Setter

	public void setAvailable(boolean available, int rowIndex){
		setter(available, rowIndex, "AVAILABLE");
	}

	public void setFile(File file, int rowIndex){
		setter(file, rowIndex, "FILE");
	}

	public void setLockRow(boolean lock, int rowIndex){
		setter(lock, rowIndex, "LOCKROW");
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (getLockRow(rowIndex)) {
			return false;
		}
		return super.isCellEditable(rowIndex, columnIndex);
	}
}