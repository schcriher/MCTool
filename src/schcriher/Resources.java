package schcriher;

import java.awt.Color;
import java.io.InputStream;
import java.net.URL;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import schcriher.utils.ArrowIcon;
import schcriher.utils.CrossIcon;

public class Resources {

	private static final Class<?> CLASS = Resources.class;
	private static final String CNAME = CLASS.getName();
	private static final Logger LOGGER = Logger.getLogger(CNAME);

	public static final String VERSION;
	public static final String LOCKED_BY_ERRORS = "<b>BLOQUEADO POR ERRORES DE PERMISOS</b><br/>";

	public static final Color TABLE_GRID_COLOR = new Color(212, 212, 212);

	public static final int WIDTH = 850;   // 854
	public static final int HEIGHT = 500;  // 480

	public static final int WIDTH_PROGRESS = 650;

	public static final int BUFFER = 4096;

	public static final Icon PROCESS_STOP_ICON;
	public static final Icon ARROW_RIGHT_ICON;
	public static final Icon ARROW_LEFT_ICON;
	public static final Icon FAVICON;

	static {
		VERSION = getVersion();
		PROCESS_STOP_ICON = getImageIcon("process-stop.png", new CrossIcon(25));
		ARROW_RIGHT_ICON = getImageIcon("arrow-right.png", new ArrowIcon(26, 21, SwingConstants.EAST));
		ARROW_LEFT_ICON = getImageIcon("arrow-left.png", new ArrowIcon(26, 21, SwingConstants.WEST));
		FAVICON = getImageIcon("favicon.png", null);
	}

	public static String getVersion() {
		try {
			String name = CLASS.getSimpleName() + ".class";
			URL url = CLASS.getResource(name);
			String protocol = url.getProtocol().toLowerCase();
			String path = url.toString();

			int offset;
			switch (protocol) {
			case "jar":  offset = path.lastIndexOf("!") + 1;          break;
			case "file": offset = path.length() - CNAME.length() - 7; break;
			default:
				return "";
			}
			path = path.substring(0, offset) + "/META-INF/MANIFEST.MF";

			Manifest manifest = new Manifest(new URL(path).openStream());
			Attributes attr = manifest.getMainAttributes();
			char[] v = attr.getValue("Schcriher-MCTool-Version").toCharArray();
			return String.format("%s.%s.%s", v[0], v[1], v[2]);
		}
		catch (Exception e) {}
		return "";
	}

	public static Icon getImageIcon(String name, Icon defaultIcon) {
		try {
			InputStream in = getResourceAsStream("resources/" + name);
			Icon icon = new ImageIcon(ImageIO.read(in));
			in.close();
			return icon;
		} catch (Exception e) {
			System.err.print("ERROR CARGANDO " + name + ": ");
			e.printStackTrace();
			LOGGER.throwing(CNAME, name, e);
		}
		return defaultIcon;
	}

	public static InputStream getResourceAsStream(String path) {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream stream;
		if (loader != null){
			stream = loader.getResourceAsStream(path);
			if (stream != null){
				return stream;
			}
		}
		loader = Resources.class.getClassLoader();
		if (loader != null){
			stream = loader.getResourceAsStream(path);
			if (stream != null){
				return stream;
			}
		}
		return ClassLoader.getSystemResourceAsStream(path);
	}
}