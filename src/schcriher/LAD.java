package schcriher;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class LAD { // Loggers And Dialogs

	private static final Logger LOGGER = Logger.getLogger(LAD.class.getName());
	private static final String LINESEP = System.getProperty("line.separator");

	////

	public static JFileChooser getDirectoriesChooser(String title) {
		return getDirectoriesChooser(title, null);
	}

	public static JFileChooser getDirectoriesChooser(String title, String approveButtonToolTipText) {
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle(title);
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);
		chooser.setApproveButtonToolTipText(approveButtonToolTipText);
		return chooser;
	}

	////

	public static boolean showConfirmDialogYesCancel(Frame parent, Collection<String> strings) {
		return showConfirmDialogYesCancel(parent, getTextArea(getText(strings)));
	}

	public static boolean showConfirmDialogYesCancel(Frame parent, String question, Collection<Component> components) {
		return showConfirmDialogYesCancel(parent, makeMessage(question, components));
	}

	public static boolean showConfirmDialogYesCancel(Frame parent, String question, Component... components) {
		return showConfirmDialogYesCancel(parent, makeMessage(question, Arrays.asList(components)));
	}

	public static boolean showConfirmDialogYesCancel(Frame parent, Component question) {
		return JOptionPane.showConfirmDialog(parent, question, "Confirm", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION;
	}

	////

	public static boolean showConfirmDialogYesNo(Frame parent, Collection<String> strings) {
		return showConfirmDialogYesNo(parent, getTextArea(getText(strings)));
	}

	public static boolean showConfirmDialogYesNo(Frame parent, String question, Collection<Component> components) {
		return showConfirmDialogYesNo(parent, makeMessage(question, components));
	}

	public static boolean showConfirmDialogYesNo(Frame parent, String question, Component... components) {
		return showConfirmDialogYesNo(parent, makeMessage(question, Arrays.asList(components)));
	}

	public static boolean showConfirmDialogYesNo(Frame parent, Component question) {
		return JOptionPane.showConfirmDialog(parent, question, "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
	}

	/* --------------------------------------------------------------------- */

	public static void showNameErrorMessage(Frame parent, File file) {
		showNameErrorMessage(parent, file.getName());
	}

	public static void showNameErrorMessage(Frame parent, String name) {
		String message = String.format("Ya existe el nombre '%s'", name);
		LOGGER.fine(message);
		JOptionPane.showMessageDialog(parent, message, "Error", JOptionPane.ERROR_MESSAGE);
	}

	public static void showRenameErrorMessageNotLog(Frame parent, File oldPath, File newPath) {
		String message = String.format("No se pudo renombrar %s a %s", oldPath, newPath);
		JOptionPane.showMessageDialog(parent, getTextArea(message), "Error", JOptionPane.ERROR_MESSAGE);
	}

	public static void showErrorMessageNotLog(Frame parent, String message) {
		JOptionPane.showMessageDialog(parent, getTextArea(message), "Error", JOptionPane.ERROR_MESSAGE);
	}

	////

	public static void showInformationMessage(Frame parent, Collection<String> strings) {
		String str = getText(strings);
		showInformationMessage(parent, str, getTextArea(str));
	}

	public static void showInformationMessage(Frame parent, String message, Collection<Component> components) {
		showInformationMessage(parent, message, makeMessage(message, components));
	}

	public static void showInformationMessage(Frame parent, String message, Component... components) {
		showInformationMessage(parent, message, makeMessage(message, Arrays.asList(components)));
	}

	public static void showInformationMessage(Frame parent, String message, Component components) {
		LOGGER.warning(message);
		JOptionPane.showMessageDialog(parent, components, "Information", JOptionPane.INFORMATION_MESSAGE);
	}

	////

	public static void showErrorMessage(Frame parent, Collection<String> strings) {
		String str = getText(strings);
		showErrorMessage(parent, str, getTextArea(str));
	}

	public static void showErrorMessage(Frame parent, String message, Collection<Component> components) {
		showErrorMessage(parent, message, makeMessage(message, components));
	}

	public static void showErrorMessage(Frame parent, String message, Component... components) {
		showErrorMessage(parent, message, makeMessage(message, Arrays.asList(components)));
	}

	public static void showErrorMessage(Frame parent, String message, Component components) {
		LOGGER.warning(message);
		JOptionPane.showMessageDialog(parent, components, "Error", JOptionPane.ERROR_MESSAGE);
	}

	////

	private static Component makeMessage(String message, Collection<Component> components) {
		Component subPanel;
		int c = 0;
		JPanel componentsPanel = new JPanel(new GridLayout(components.size(), 1, 0, 0));
		componentsPanel.setBorder(new EmptyBorder(10, 0, 0, 0));
		for (Component component: components) {
			if (component != null) {
				componentsPanel.add(component);
				c += 1;
			}
		}
		componentsPanel.validate();
		Dimension d = componentsPanel.getPreferredSize();
		if (d.height > Resources.HEIGHT) {
			d.height = Resources.HEIGHT;
			JScrollPane scroll = new JScrollPane(componentsPanel);
			scroll.getViewport().setPreferredSize(d);
			scroll.setBorder(new EmptyBorder(10, 0, 0, 0));
			scroll.getVerticalScrollBar().setUnitIncrement(componentsPanel.getPreferredSize().height / c);
			subPanel = scroll;
		} else {
			subPanel = componentsPanel;
		}
		Component main;
		if (c > 0) {
			JPanel mainPanel = new JPanel(new BorderLayout());
			mainPanel.setBorder(new EmptyBorder(5, 1, 5, 8));
			mainPanel.add(new JLabel(message), BorderLayout.NORTH);
			mainPanel.add(subPanel, BorderLayout.CENTER);
			main = mainPanel;
		} else {
			main = new JLabel(message);
		}
		return main;
	}

	public static String getText(Collection<String> strings) {
		StringBuilder text = new StringBuilder();
		for (String s: strings) {
			text.append(s);
			text.append(LINESEP);
		}
		return text.toString();
	}

	public static Component getTextArea(String message) {
		Component component;
		JTextArea textArea = new JTextArea(message);
		textArea.setText(message);
		textArea.setColumns(80);
		textArea.setOpaque(false);
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setSize(textArea.getPreferredSize().width, 1);
		textArea.validate();
		Dimension d = textArea.getPreferredSize();
		if (d.height > Resources.HEIGHT) {
			d.height = Resources.HEIGHT;
			JScrollPane scroll = new JScrollPane(textArea);
			scroll.getViewport().setPreferredSize(d);
			component = scroll;
		} else {
			component = textArea;
		}
		return component;
	}
}