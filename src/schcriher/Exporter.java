package schcriher;

import static schcriher.waitdialogs.Publish.Type.COMPLETE;
import static schcriher.waitdialogs.Publish.Type.MAJOR;
import static schcriher.waitdialogs.Publish.Type.MAJORSTEP;

import java.awt.Frame;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import schcriher.utils.Archives;
import schcriher.utils.Zip;
import schcriher.waitdialogs.Publisher;

public class Exporter extends Publisher {

	private final static String title = "Exportador";
	private final Frame mcparent;
	private final File dstRoot;
	private final List<File> srcList;

	public Exporter(Frame mcparent, File dstRoot, List<File> srcList) {
		super(mcparent, title);
		this.mcparent = mcparent;
		this.dstRoot = dstRoot;
		this.srcList = srcList;
	}

	@Override
	protected Void doInBackground() throws Exception {
		publish(MAJOR, srcList.size() + 1, "Comenzando...");

		final List<String> errorList = new ArrayList<String>();
		final List<File> dstList = new ArrayList<File>();

		for (File f: srcList) {
			dstList.add(new File(dstRoot, f.getName() + (f.isDirectory() ? ".zip" : "")));
		}
		if (Filenames.checkExistenceAndAskNewnames(mcparent, dstList)) {
			int n = srcList.size();
			for (int i = 0; i < n; i++) {
				File dst = dstList.get(i);
				if (dst != null) {
					publish(MAJORSTEP, dst.getName());
					File src = srcList.get(i);
					if (src.isDirectory()) {
						if (!Zip.compressor(src, dst)) {
							errorList.add(src.getPath());
						}
					} else {
						if (!Archives.copy(src, dst)) {
							errorList.add(src.getPath());
						}
					}
				} else {
					publish(MAJORSTEP);
				}
			}
		}
		publish(MAJORSTEP, "Finalizando...");
		if (!errorList.isEmpty()) {
			Collections.sort(errorList);
			errorList.add(0, "Los siguientes archivos no se pudieron exportar:");
			errorList.add(1, "");
			LAD.showErrorMessage(mcparent, errorList);
		}
		publish(COMPLETE, "Finalizado");

		return null;
	}
}