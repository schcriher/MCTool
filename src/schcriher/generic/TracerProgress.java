package schcriher.generic;

public interface TracerProgress {

	void text(String text);

	void count(long count);

	void chuck(long chuck);

}