package schcriher.generic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

public abstract class TableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private static final String META = "<META key=%s, name=%s, type=%s, visible=%b, editable=%b, dataColumnIndex=%d, tableColumnIndex=%d>";

	protected final List<List<Object>> data = new ArrayList<List<Object>>();
	protected final Map<String, Meta> mMeta = new HashMap<String, Meta>();
	protected final List<Meta> lMeta = new ArrayList<Meta>();

	private final List<Integer> tableToDataIndex = new ArrayList<Integer>();

	private int dataColumnCount = 0;
	private int tableColumnCount = 0;

	public class Meta {

		public final String key;
		public final String name;
		public final Class<?> type;
		public final boolean visible;
		public final boolean editable;
		public final int dataColumnIndex;
		public final int tableColumnIndex;

		public Meta(Object... data) {
			key = (String) data[0];
			name = (String) data[1];
			type = (Class<?>) data[2];
			visible = (boolean) data[3];
			editable = (boolean) data[4];
			dataColumnIndex = dataColumnCount++;
			if (visible) {
				tableColumnIndex = tableColumnCount++;
				tableToDataIndex.add(dataColumnIndex);
			} else {
				tableColumnIndex = -1;
			}
		}

		@Override
		public String toString() {
			return String.format(META, key, name, type, visible, editable, dataColumnIndex, tableColumnIndex);
		}
	}

	public TableModel(Object[][] columns) {
		for (Object[] c: columns) {
			Meta m = new Meta(c);
			lMeta.add(m);         // lMeta.get(dataColumnIndex)
			mMeta.put(m.key, m);  // mMeta.get("KEY")
		}
	}

	@Override
	public String toString() {
		return String.format("%s: rows=%s, tableCols=%s, dataCols=%s", getClass().getSimpleName(), getRowCount(), getColumnCount(), lMeta.size());
	}

	@Override
	public int getColumnCount() {
		return tableColumnCount;
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return _getter(rowIndex, tableToDataIndex.get(columnIndex));
	}

	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		_setter(value, rowIndex, tableToDataIndex.get(columnIndex));
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return lMeta.get(tableToDataIndex.get(columnIndex)).editable;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return lMeta.get(tableToDataIndex.get(columnIndex)).type;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return lMeta.get(tableToDataIndex.get(columnIndex)).name;
	}

	public void addRow(Object... rowData) {
		addRow(Arrays.asList(rowData));
	}

	public void addRow(List<Object> rowData) {
		validate(rowData);
		int insertedRow = data.size();
		data.add(rowData);
		fireTableRowsInserted(insertedRow, insertedRow);
	}

	private void validate(List<Object> rowData) {
		int n = rowData.size();
		if (n != dataColumnCount) {
			throw new IllegalArgumentException("You must add " + dataColumnCount + " items per row");
		}
		for (int i = 0; i < n; i++) {
			Class<?> type = lMeta.get(i).type;
			if (rowData.get(i).getClass() != type) {
				throw new IllegalArgumentException("The column " + i + " must be type " + type);
			}
		}
	}

	public void removeRow(int rowIndex) {
		data.remove(rowIndex);
		fireTableRowsDeleted(rowIndex, rowIndex);
	}

	public void removeRows(Integer[] rowIndexes) {
		removeRows(Arrays.asList(rowIndexes));
	}

	public void removeRows(List<Integer> rowIndexes) {
		Collections.reverse(rowIndexes);
		for (int i: rowIndexes) {
			removeRow(i);
		}
	}

	public void clear() {
		if (!data.isEmpty()) {
			int lastRow = getRowCount() - 1;
			data.clear();
			fireTableRowsDeleted(0, lastRow);
		}
	}

	public int getColumnIndex(String key) {
		try {
			return mMeta.get(key).tableColumnIndex;
		} catch (NullPointerException e) {
			throw new RuntimeException(key + " unknown key");
		}
	}

	public Object getter(int rowIndex, String key) {
		return _getter(rowIndex, mMeta.get(key).dataColumnIndex);
	}

	public void setter(Object value, int rowIndex, String key) {
		_setter(value, rowIndex, mMeta.get(key).dataColumnIndex);
	}

	private Object _getter(int rowIndex, int dataColumnIndex) {
		return data.get(rowIndex).get(dataColumnIndex);
	}

	private void _setter(Object value, int rowIndex, int dataColumnIndex) {
		List<Object> rowData = data.get(rowIndex);
		if (value != rowData.get(dataColumnIndex)) {
			rowData.set(dataColumnIndex, value);
			Meta meta = lMeta.get(dataColumnIndex);
			if (meta.visible) {
				fireTableCellUpdated(rowIndex, meta.tableColumnIndex);
			}
		}
	}
}