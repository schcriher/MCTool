package schcriher.generic;

public class DummyTracerProgress implements TracerProgress {

	@Override
	public void text(String text) {}

	@Override
	public void count(long count) {}

	@Override
	public void chuck(long chuck) {}

}