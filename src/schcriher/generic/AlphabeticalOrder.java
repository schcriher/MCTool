package schcriher.generic;

import java.text.Normalizer;
import java.util.Comparator;
import java.util.regex.Pattern;

public class AlphabeticalOrder<T> implements Comparator<T> {

	// http://www.unicode.org/reports/tr44/#General_Category_Values
	private static final Pattern REMOVE = Pattern.compile("[\\p{IsM}\\p{IsP}\\p{IsC}\\p{IsLm}\\p{IsSk}]+");
	private static final Pattern SPACE = Pattern.compile("[\\p{IsZ}]+");

	@Override
	public int compare(T a, T b) {
		return normalize(a).compareTo(normalize(b));
	}

	public String normalize(T obj) {
		String text = obj.toString().toLowerCase();
		text = Normalizer.normalize(text, Normalizer.Form.NFKD);
		text = REMOVE.matcher(text).replaceAll("");
		text = SPACE.matcher(text).replaceAll(" ");
		return text;
	}
}