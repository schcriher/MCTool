package schcriher.generic;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTable;
import javax.swing.SwingUtilities;

public class UnselectionTableMouseListener implements MouseListener {

	@Override
	public void mouseClicked(MouseEvent e) {
		JTable table = (JTable) e.getSource();
		boolean outSide = table.rowAtPoint(e.getPoint()) == -1;
		boolean rightButton = SwingUtilities.isRightMouseButton(e);
		if (outSide || rightButton) {
			table.getSelectionModel().clearSelection();
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}
}