package schcriher.worlds;

import java.awt.Component;
import java.io.File;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class DirNameCellRenderer extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 1L;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (value instanceof File) {
			File f = ((File)value).getParentFile();
			value = f.getName();
		}
		return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	};
}