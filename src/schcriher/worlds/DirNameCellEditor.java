package schcriher.worlds;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

import schcriher.LAD;
import schcriher.utils.Archives;

public class DirNameCellEditor extends AbstractCellEditor implements TableCellEditor {

	private static final long serialVersionUID = 1L;
	private final JTextField dirName;
	private final Worlds worlds;
	private File fLevelDat;
	private int rowIndex;

	public DirNameCellEditor(Worlds worlds) {
		this.worlds = worlds;
		dirName = new JTextField();
		dirName.setBorder(null);
	}

	// TableCellEditor

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		if (!isSelected) {
            return null;
        }
		fLevelDat = (File)value;
		dirName.setText(fLevelDat.getParentFile().getName());
		rowIndex = row;
		return dirName;
	}

    // AbstractCellEditor

	@Override
	public boolean isCellEditable(EventObject e) {
        if (e instanceof MouseEvent) {
            return ((MouseEvent)e).getClickCount() >= 2;
        }
        return true;
    }

    @Override
    public boolean stopCellEditing() {
    	String newName = dirName.getText();
    	if (!newName.equals(fLevelDat.getParentFile().getName()) && worlds.dirNames.contains(newName)) {
    		LAD.showNameErrorMessage(worlds.core.mcparent, newName);
    		return false;
    	}
    	fireEditingStopped();
    	return true;
    }

    @Override
    public Object getCellEditorValue() {
    	File oldDir = fLevelDat.getParentFile();
    	String oldName = oldDir.getName();
    	String newName = dirName.getText();
    	if (!newName.equals(oldName)) {
    		File newDir = new File(oldDir.getParentFile(), newName);
    		if (Archives.move(oldDir, newDir, true)) {
    			if (worlds.setNameBackupsTableModelCurrent(newName)) {
    				fLevelDat = new File(newDir, fLevelDat.getName());
    			} else {
    				Archives.move(newDir, oldDir, true);
    				worlds.setWorldsTableModelLockRow(rowIndex, true);
    				LAD.showErrorMessage(worlds.core.mcparent, "Se cancelaron todos los cambios");
    			}
    		} else {
    			LAD.showRenameErrorMessageNotLog(worlds.core.mcparent, oldDir, newDir);
    		}
    	}
    	return fLevelDat;
    }
}