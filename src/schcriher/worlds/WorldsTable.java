package schcriher.worlds;

import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.JTable;

import schcriher.Resources;

public class WorldsTable extends JTable {

	private static final long serialVersionUID = 1L;
	private final WorldsTableModel model;

	public WorldsTable(WorldsTableModel model) {
		super(model);
		this.model = model;
	}

	@Override
	public String getToolTipText(MouseEvent e) {
		String tip = null;
		try {
			Point p = e.getPoint();
			int rowIndex = rowAtPoint(p);
			if (rowIndex >= 0) {
				int columnIndex = columnAtPoint(p);
				String path = model.getLevelFile(rowIndex).getPath();
				boolean lock = model.getLockRow(rowIndex);

				if (columnIndex == model.getColumnIndex("AVAILABLE")) {
					String l = lock ? Resources.LOCKED_BY_ERRORS : "";
					String d = model.getAvailable(rowIndex) ? "Disponible en Minecraft" : "No disponible en Minecraft";
					tip = String.format("<html>%s%s</html>", l, d);
				}
				else if (columnIndex == model.getColumnIndex("LEVELFILE")) {
					String l = lock ? Resources.LOCKED_BY_ERRORS : "Doble click para editar el nombre<br/>";
					tip = String.format("<html>%s<i>%s</i></html>", l, path);
				}
				else if (columnIndex == model.getColumnIndex("WORLDNAME")) {
					String l = lock ? Resources.LOCKED_BY_ERRORS : "Doble click para editar los datos<br/>";
					tip = String.format("<html>%s<i>%s</i></html>", l, path);
				}
			}
		}
		catch (Exception ex) {}
		return tip;
	}
}