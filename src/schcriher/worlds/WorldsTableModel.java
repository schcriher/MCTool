package schcriher.worlds;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import schcriher.generic.TableModel;

public class WorldsTableModel extends TableModel {

	private static final long serialVersionUID = 1L;

	public WorldsTableModel() {
		super(new Object[][] {
				{"AVAILABLE", " §",                   Boolean.class, true,  true},
				{"LEVELFILE", "Nombre de la carpeta", File.class,    true,  true},
				{"WORLDNAME", "Nombre del mundo",     String.class,  true,  true},
				{"LOCKROW",   "",                     Boolean.class, false, false},
		});
	}

	// Getter

	public boolean getAvailable(int rowIndex){
		return (boolean)getter(rowIndex, "AVAILABLE");
	}

	public File getLevelFile(int rowIndex){
		return (File)getter(rowIndex, "LEVELFILE");
	}

	public List<File> getAllLevelFiles(){
		int nrow = getRowCount();
		List<File> list = new ArrayList<File>(nrow);
		for (int i = 0; i < nrow; i++) {
			list.add(getLevelFile(i));
		}
		return list;
	}

	public String getDirName(int rowIndex){
		return getDirNameFile(rowIndex).getName();
	}

	public File getDirNameFile(int rowIndex){
		return getLevelFile(rowIndex).getParentFile();
	}

	public List<File> getAllDirNameFiles(){
		int nrow = getRowCount();
		List<File> list = new ArrayList<File>(nrow);
		for (int i = 0; i < nrow; i++) {
			list.add(getDirNameFile(i));
		}
		return list;
	}

	public String getWorldName(int rowIndex){
		return (String)getter(rowIndex, "WORLDNAME");
	}

	public boolean getLockRow(int rowIndex){
		return (boolean)getter(rowIndex, "LOCKROW");
	}

	// Setter

	public void setAvailable(boolean available, int rowIndex){
		setter(available, rowIndex, "AVAILABLE");
	}

	public void setLevelFile(File levelFile, int rowIndex){
		setter(levelFile, rowIndex, "LEVELFILE");
	}

	public void setWorldName(String worldName, int rowIndex){
		setter(worldName, rowIndex, "WORLDNAME");
	}

	public void setLockRow(boolean lock, int rowIndex){
		setter(lock, rowIndex, "LOCKROW");
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (getLockRow(rowIndex)) {
			return false;
		}
		return super.isCellEditable(rowIndex, columnIndex);
	}
}