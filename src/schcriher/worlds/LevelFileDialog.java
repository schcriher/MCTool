package schcriher.worlds;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import schcriher.LAD;
import schcriher.nbt.NBT;
import schcriher.nbt.TAG_Byte;
import schcriher.nbt.TAG_Compound;
import schcriher.nbt.TAG_Int;

public class LevelFileDialog extends JDialog implements ActionListener, ChangeListener {

	/*http://minecraft.gamepedia.com/Talk:Transportation#Speed_formula*/
	public final static double INTERNAL_SPEED_TO_BLOCKS_PER_SECOND = 43.17180778680964;

	private static final long serialVersionUID = 1L;
	private static final boolean modal = true;
	private static final String title = "Editor de mundo";

	private final JPanel contentPanel = new JPanel();
	private final Frame mcparent;
	private final URI fSaves;

	private int XpTotal = 0;
	private float foodExhaustionLevel = 4F;
	private boolean saved = true;
	private String newLevelName;
	private File levelFile;

	private final JTextField LevelName = new JTextField();
	private final JLabel LastPlayed = new JLabel();
	private final JLabel levelFilePath = new JLabel();

	private final SpinnerNumberModel XpLevelXpPModel = new SpinnerNumberModel(0.0, 0.0, Integer.MAX_VALUE, 1);  // init, min, max, step
	private final SpinnerNumberModel ScoreModel = new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1);
	private final SpinnerNumberModel foodLevelModel = new SpinnerNumberModel(20, 0, 20, 1);
	private final SpinnerNumberModel foodSaturationLevelModel = new SpinnerNumberModel(0.0, 0.0, 20, 1);
	private final SpinnerNumberModel walkSpeedModel = new SpinnerNumberModel(0.01, 0.01, 100, 0.1);
	private final SpinnerNumberModel flySpeedModel = new SpinnerNumberModel(0.01, 0.01, 100, 0.05);

	private final JSpinner XpLevelXpP = new JSpinner(XpLevelXpPModel);
	private final JSpinner Score = new JSpinner(ScoreModel);
	private final JSpinner foodLevel = new JSpinner(foodLevelModel);
	private final JSpinner foodSaturationLevel = new JSpinner(foodSaturationLevelModel);
	private final JSpinner walkSpeed = new JSpinner(walkSpeedModel);
	private final JSpinner flySpeed = new JSpinner(flySpeedModel);

	private final JToggleButton allowCommands = new JToggleButton("Cheats");
	private final JToggleButton hardcore = new JToggleButton("Hardcore");

	private final JComboBox<DIFFICULTY> Difficulty = new JComboBox<DIFFICULTY>(DIFFICULTY.values());
	private final JComboBox<GAMETYPE> GameType = new JComboBox<GAMETYPE>(GAMETYPE.values());

	private final JButton resetButton = new JButton("Restablecer");
	private final JButton cancelButton = new JButton("Cancelar");
	private final JButton okButton = new JButton("Aceptar");

	enum DIFFICULTY {
		PEACEFUL (0, "Pacífico", false),
		EASY     (1, "Fácil",    false),
		NORMAL   (2, "Normal",   true),
		HARD     (3, "Difícil",  false);

		private final TAG_Byte value;
		private final String usertext;
		private final boolean isDefault;

		DIFFICULTY(int value, String usertext, boolean isDefault) {
			this.value = new TAG_Byte("Difficulty", (byte)value);
			this.usertext = usertext;
			this.isDefault = isDefault;
		}

		private static Map<Byte, DIFFICULTY> map = new HashMap<Byte, DIFFICULTY>();
		private static DIFFICULTY defaultDifficulty;
		static {
			for (DIFFICULTY d : values()) {
				map.put((byte)d.tag().getData(), d);
				if (d.isDefault) {
					defaultDifficulty = d;
				}
			}
	    }

		public static DIFFICULTY get(byte data) {
			if (map.containsKey(data)) {
				return map.get(data);
			} else {
				return defaultDifficulty;
			}
		}

		public static DIFFICULTY getDefault() {
			return defaultDifficulty;
		}

		public TAG_Byte tag() {
			return value;
		}

		public byte value() {
			return (byte)value.getData();
		}

		@Override
		public String toString() {
			return usertext;
		}
	}

	enum GAMETYPE {
		SURVIVAL  (0, "Supervivencia", true),
		CREATIVE  (1, "Creativo",      false),
		ADVENTURE (2, "Aventura",      false),
		SPECTATOR (3, "Espectador",    false);

		private final TAG_Int value;
		private final String usertext;
		private final boolean isDefault;

		GAMETYPE(int value, String usertext, boolean isDefault) {
			this.value = new TAG_Int("GameType", value);
			this.usertext = usertext;
			this.isDefault = isDefault;
		}

		private static Map<Integer, GAMETYPE> map = new HashMap<Integer, GAMETYPE>();
		private static GAMETYPE defaultGameType;
		static {
			for (GAMETYPE gt : values()) {
				map.put((int)gt.tag().getData(), gt);
				if (gt.isDefault) {
					defaultGameType = gt;
				}
			}
	    }

		public static GAMETYPE get(int data) {
			if (map.containsKey(data)) {
				return map.get(data);
			} else {
				return defaultGameType;
			}
		}

		public static GAMETYPE getDefault() {
			return defaultGameType;
		}

		public TAG_Int tag() {
			return value;
		}

		public byte value() {
			return (byte)value.getData();
		}

		@Override
		public String toString() {
			return usertext;
		}
	}

	public LevelFileDialog(Frame mcparent, URI fSaves) {
		super(mcparent, title, modal);
		this.mcparent = mcparent;
		this.fSaves = fSaves;
		initComponents();
		pack();
		setLocationRelativeTo(mcparent);
	}

	private void initComponents() {
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();

		layout.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0};

		contentPanel.setLayout(layout);
		contentPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		//////////////////////////////////////////////////

        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 7;
        c.gridheight = 1;
        c.weightx = 0.0;
        c.weighty = 1.0;
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.NONE;
        c.insets = new Insets(5, 5, 10, 5);
        c.ipadx = 0;
        c.ipady = 0;

        // Data → LastPlayed: The Unix time when the level was last loaded.
        LastPlayed.setForeground(Color.GRAY);
		contentPanel.add(LastPlayed, c);

		// Data → LevelName: The name of the level. TAG_String
        c.gridy = 1;
        c.gridwidth = 1;
        c.insets.bottom = 15;
        JLabel LevelNameLabel = new JLabel("<html><b>Nombre</b></html>");
        LevelNameLabel.setToolTipText("Data.LevelName");
		contentPanel.add(LevelNameLabel, c);

		// Player → XpLevel: The level shown on the XP bar. TAG_Int
		// Player → XpP: The progress/percent across the XP bar to the next level. TAG_Float
		// Player → Score: The Score displayed upon death. TAG_Int
		c.gridy = 2;
		c.insets.bottom = 5;
		contentPanel.add(new JLabel("<html><b>Logros</b></html>"), c);

		// Player → foodLevel: The value of the hunger bar; 20 is full. See Hunger. TAG_Int
		// Player → foodSaturationLevel: See Hunger. TAG_Float
		c.gridy = 3;
		contentPanel.add(new JLabel("<html><b>Vitalidad</b></html>"), c);

		// Player → abilities → walkSpeed: The walking speed, always 0.1. TAG_Float
		// Player → abilities → flySpeed: The flying speed, always 0.05. TAG_Float
		c.gridy = 4;
		contentPanel.add(new JLabel("<html><b>Velocidad</b></html>"), c);

		// Data → DIFFICULTY: The current difficulty setting. 0 is Peaceful, 1 is Easy, 2 is Normal, and 3 is Hard. Defaults to 2. TAG_Byte
		// Player → GameType: The game mode of the player. 0 is Survival, 1 is Creative, 2 is Adventure and 3 is Spectator. TAG_Int
		c.gridy = 5;
		contentPanel.add(new JLabel("<html><b>Jugabilidad</b></html>"), c);

		//////////////////////////////////////////////////

		c.gridx = 1;
        c.gridwidth = 5;
        c.weightx = 1.0;
        c.insets.bottom = 15;
		c.fill = GridBagConstraints.HORIZONTAL;

        // Data → LevelName: The name of the level. TAG_String
		c.gridy = 1;
		contentPanel.add(LevelName, c);

		// Data → allowCommands: 1 or 0 (true/false) - true if cheats are enabled. TAG_Byte
		// Data → hardcore: 1 or 0 (true/false) - true if the player must delete their world on death in singleplayer.
		//                  Affects all three game modes. TAG_Byte
        JPanel panelToggleButton = new JPanel(new GridLayout(1, 2, 3, 0));
        c.gridwidth = 1;
		c.gridx = 6;
		contentPanel.add(panelToggleButton, c);
        panelToggleButton.add(allowCommands);
        panelToggleButton.add(hardcore);

        hardcore.addActionListener(this); // Para advertir de su activación

		/////////////////////////

        c.weightx = 0.0;
		c.gridy = 2;
		c.fill = GridBagConstraints.NONE;
		c.insets.bottom = 5;

		// Player → XpLevel: The level shown on the XP bar. TAG_Int
		// Player → XpP: The progress/percent across the XP bar to the next level. TAG_Float
		c.gridx = 2;
		JLabel XpLevelXpPLabel = new JLabel("Experiencia");
		XpLevelXpPLabel.setToolTipText("Data.Player.XpLevel + Data.Player.XpP");
		contentPanel.add(XpLevelXpPLabel, c);

		// Player → Score: The Score displayed upon death. TAG_Int
		c.gridx = 5;
		JLabel ScoreLabel = new JLabel("Puntuación");
		ScoreLabel.setToolTipText("Data.Player.Score");
		contentPanel.add(ScoreLabel, c);

		c.weightx = 1.0;
		c.fill = GridBagConstraints.HORIZONTAL;

		// Player → XpLevel: The level shown on the XP bar. TAG_Int
		// Player → XpP: The progress/percent across the XP bar to the next level. TAG_Float
		c.gridx = 3;
		contentPanel.add(XpLevelXpP, c);

		// Player → Score: The Score displayed upon death. TAG_Int
		c.gridx = 6;
		contentPanel.add(Score, c);

		/////////////////////////

		c.gridy = 3;
		c.fill = GridBagConstraints.NONE;

		// Player → foodLevel: The value of the hunger bar; 20 is full. See Hunger. TAG_Int
		c.gridx = 2;
		JLabel foodLevelLabel = new JLabel("Alimentos");
		foodLevelLabel.setToolTipText("Data.Player.foodLevel");
		contentPanel.add(foodLevelLabel, c);

		// Player → foodSaturationLevel: See Hunger. TAG_Float
		c.gridx = 5;
		JLabel foodSaturationLevelLabel = new JLabel("Saturación");
		foodSaturationLevelLabel.setToolTipText("Data.Player.foodSaturationLevel");
		contentPanel.add(foodSaturationLevelLabel, c);

		c.weightx = 1.0;
		c.fill = GridBagConstraints.HORIZONTAL;

		// Player → foodLevel: The value of the hunger bar; 20 is full. See Hunger. TAG_Int
		c.gridx = 3;
		foodLevel.setToolTipText("De cero (0) a veinte (20)");
		foodLevel.addChangeListener(this);
		contentPanel.add(foodLevel, c);

		// Player → foodSaturationLevel: See Hunger. TAG_Float
		c.gridx = 6;
		foodSaturationLevel.setToolTipText("De cero (0) al valor de los alimentos (foodLevel)");
		contentPanel.add(foodSaturationLevel, c);

		/////////////////////////

		c.gridy = 4;
        c.weightx = 0.0;
		c.fill = GridBagConstraints.NONE;

		// Player → abilities → walkSpeed: The walking speed, always 0.1. TAG_Float
		c.gridx = 2;
		JLabel walkSpeedLabel = new JLabel("En tierra");
		walkSpeedLabel.setToolTipText("Data.Player.abilities.walkSpeed");
		contentPanel.add(walkSpeedLabel, c);

		// Player → abilities → flySpeed: The flying speed, always 0.05. TAG_Float
		c.gridx = 5;
		JLabel flySpeedLabel = new JLabel("En vuelo");
		flySpeedLabel.setToolTipText("Data.Player.abilities.flySpeed");
		contentPanel.add(flySpeedLabel, c);

		c.weightx = 1.0;
		c.fill = GridBagConstraints.HORIZONTAL;

		// Player → abilities → walkSpeed: The walking speed, always 0.1. TAG_Float
		c.gridx = 3;
		walkSpeed.setToolTipText("En bloques o metros por segundo [b/s, m/s]");
		contentPanel.add(walkSpeed, c);

		// Player → abilities → flySpeed: The flying speed, always 0.05. TAG_Float
		c.gridx = 6;
		flySpeed.setToolTipText("En bloques o metros por segundo [b/s, m/s]");
		contentPanel.add(flySpeed, c);

		/////////////////////////

		c.gridy = 5;
        c.weightx = 0.0;
		c.fill = GridBagConstraints.NONE;

		// Data → Difficulty: The current difficulty setting. 0 is Peaceful, 1 is Easy, 2 is Normal, and 3 is Hard. Defaults to 2. TAG_Byte
		c.gridx = 2;
		JLabel DifficultyLabel = new JLabel("Dificultad");
		DifficultyLabel.setToolTipText("Data.Difficulty");
		contentPanel.add(DifficultyLabel, c);
		// Data → GameType: The default game mode for the singleplayer player when they initially spawn. 0 is Survival Mode, 1 is Creative
		//                  Mode, 2 is Adventure Mode, 3 is Spectator Mode. Note: Singleplayer worlds do not use this field to save which
		//                  game mode the player is currently in.
		// Player → playerGameType: The game mode of the player. 0 is Survival, 1 is Creative, 2 is Adventure and 3 is Spectator. TAG_Int
		c.gridx = 5;
		JLabel GameTypeLabel = new JLabel("Modo");
		GameTypeLabel.setToolTipText("<html>Data.GameType (initially spawn)<br/>Data.Player.playerGameType</html>");
		contentPanel.add(GameTypeLabel, c);

		c.weightx = 1.0;
		c.fill = GridBagConstraints.HORIZONTAL;

		// Data → Difficulty: The current difficulty setting. 0 is Peaceful, 1 is Easy, 2 is Normal, and 3 is Hard. Defaults to 2. TAG_Byte
		Difficulty.setPreferredSize(new Dimension(160, Difficulty.getPreferredSize().height));
		c.gridx = 3;
		contentPanel.add(Difficulty, c);

		// Player → GameType: The game mode of the player. 0 is Survival, 1 is Creative, 2 is Adventure and 3 is Spectator. TAG_Int
		GameType.setPreferredSize(new Dimension(160, GameType.getPreferredSize().height));
		c.gridx = 6;
		contentPanel.add(GameType, c);

		/////////////////////////

		c.gridy = 2;
        c.weightx = 1.0;
		c.fill = GridBagConstraints.BOTH;

		c.gridx = 1;
		contentPanel.add(Box.createHorizontalStrut(5), c);

		c.gridx = 4;
		contentPanel.add(Box.createHorizontalStrut(10), c);

		//////////////////////////////////////////////////

		c.gridy = 6;
		c.insets.top = 15;
		c.fill = GridBagConstraints.NONE;

		levelFilePath.setForeground(Color.GRAY);
        c.gridx = 1;
        c.gridwidth = 5;
        c.weightx = 1.0;
        contentPanel.add(levelFilePath, c);

        c.gridwidth = 1;
        c.weightx = 0.0;

        resetButton.setActionCommand("RESET");
        resetButton.addActionListener(this);
        c.gridx = 0;
        contentPanel.add(resetButton, c);

        JPanel panelButton = new JPanel(new GridLayout(1, 2, 3, 0));
        c.gridx = 6;
        c.fill = GridBagConstraints.HORIZONTAL;
        contentPanel.add(panelButton, c);

        cancelButton.setActionCommand("CANCEL");
        cancelButton.addActionListener(this);
        panelButton.add(cancelButton);

        okButton.setActionCommand("OK");
        okButton.addActionListener(this);
        panelButton.add(okButton);
	}

	private int getXpTotal() {
		return XpTotal;
	}

	private void setXpTotal(int val) {
		XpTotal = val;
		XpLevelXpP.setToolTipText("Experiencia total acumulada desde la última muerte: " + val);
	}

	private void reset() {
		LevelName.setText(levelFile != null ? levelFile.getParentFile().getName() : "Nuevo mundo");

		allowCommands.setSelected(false);
		hardcore.setSelected(false);

		setXpTotal(0);
		XpLevelXpP.setValue(0.0);
		Score.setValue(0);

		foodLevel.setValue(20);
		foodSaturationLevel.setValue(20.0);
		foodSaturationLevelModel.setMaximum(20.0);
		foodExhaustionLevel = 4F;

		walkSpeed.setValue(0.1F * INTERNAL_SPEED_TO_BLOCKS_PER_SECOND);
		flySpeed.setValue(0.05F * INTERNAL_SPEED_TO_BLOCKS_PER_SECOND);

		Difficulty.setSelectedItem(DIFFICULTY.NORMAL);
		GameType.setSelectedItem(GAMETYPE.SURVIVAL);
	}

	void load(File file) {
		try {
			if (!file.canRead()) {
				file.setReadable(true);
        	}
			levelFile = file;
			levelFilePath.setText(fSaves.relativize(levelFile.toURI()).getPath());
			levelFilePath.setToolTipText(levelFile.getPath());
			NBT nbt = new NBT();
			nbt.load(levelFile);

			TAG_Compound rootTag = nbt.getRootTagCompound();
			TAG_Compound dataTag = rootTag.getCompound("Data");
			TAG_Compound playerTag = dataTag.getCompound("Player");
			TAG_Compound abilitiesTag = playerTag.getCompound("abilities");

			LevelName.setText(dataTag.getString("LevelName", file.getParentFile().getName()));
			newLevelName = LevelName.getText();

			allowCommands.setSelected(dataTag.getBoolean("allowCommands", false));
			hardcore.setSelected(dataTag.getBoolean("hardcore", false));

			setXpTotal(playerTag.getInt("XpTotal"));
			XpLevelXpP.setValue((double)(playerTag.getInt("XpLevel", 0) + playerTag.getFloat("XpP", 0)));
			Score.setValue(playerTag.getInt("Score", 0));

			int foodLevelValue = playerTag.getInt("foodLevel", 20);
			foodLevel.setValue(foodLevelValue);
			foodSaturationLevel.setValue((double)playerTag.getFloat("foodSaturationLevel", 20F));
			foodSaturationLevelModel.setMaximum((double)foodLevelValue);
			if (foodSaturationLevelModel.getNextValue() == null) {
				foodSaturationLevelModel.setValue(foodSaturationLevelModel.getMaximum());
			}
			foodExhaustionLevel = playerTag.getFloat("foodExhaustionLevel", 4F);

			walkSpeed.setValue(abilitiesTag.getFloat("walkSpeed", 0.1F) * INTERNAL_SPEED_TO_BLOCKS_PER_SECOND);
			flySpeed.setValue(abilitiesTag.getFloat("flySpeed", 0.05F) * INTERNAL_SPEED_TO_BLOCKS_PER_SECOND);

			if (dataTag.contains("Difficulty")) {
				Difficulty.setSelectedItem(DIFFICULTY.get(dataTag.getByte("Difficulty")));
			} else {
				Difficulty.setSelectedItem(DIFFICULTY.getDefault());
			}

			if (playerTag.contains("playerGameType")) {
				GameType.setSelectedItem(GAMETYPE.get(playerTag.getInt("playerGameType")));
			} else {
				GameType.setSelectedItem(GAMETYPE.getDefault());
			}

			Date LastPlayedDate = new Date(dataTag.getLong("LastPlayed", 0));
			LastPlayed.setText(String.format("Jugado por última vez el %1$tA %1$te de %1$tB del %1$tY a las %1$tT (%1$TZ)", LastPlayedDate));
		}
		catch (Exception e) {
			LAD.showErrorMessage(mcparent, e.getLocalizedMessage());
		}
	}

	private void write() {
		try {
        	if (!levelFile.canWrite()) {
        		levelFile.setWritable(true);
        	}
			NBT nbt = new NBT();
			nbt.load(levelFile);

			TAG_Compound rootTag = nbt.getRootTagCompound();
			TAG_Compound dataTag = rootTag.getCompound("Data");
			TAG_Compound playerTag = dataTag.getCompound("Player");
			TAG_Compound abilitiesTag = playerTag.getCompound("abilities");

			dataTag.putString("LevelName", LevelName.getText());
			newLevelName = LevelName.getText();

			dataTag.putBoolean("allowCommands", allowCommands.isSelected());
			dataTag.putBoolean("hardcore", hardcore.isSelected());

			playerTag.putInt("XpTotal", getXpTotal());
			float xplevelxpp = (float)XpLevelXpPModel.getNumber().doubleValue();
			playerTag.putInt("XpLevel", (int)xplevelxpp);
			playerTag.putFloat("XpP", xplevelxpp - (int)xplevelxpp);
			playerTag.putInt("Score", ScoreModel.getNumber().intValue());

			playerTag.putInt("foodLevel", foodLevelModel.getNumber().intValue());
			playerTag.putFloat("foodSaturationLevel", (float)foodSaturationLevelModel.getNumber().doubleValue());
			playerTag.putFloat("foodExhaustionLevel", foodExhaustionLevel);

			abilitiesTag.putFloat("walkSpeed", (float)(walkSpeedModel.getNumber().doubleValue() / INTERNAL_SPEED_TO_BLOCKS_PER_SECOND));
			abilitiesTag.putFloat("flySpeed", (float)(flySpeedModel.getNumber().doubleValue() / INTERNAL_SPEED_TO_BLOCKS_PER_SECOND));

			dataTag.put("Difficulty", ((DIFFICULTY)Difficulty.getSelectedItem()).tag());

			TAG_Int gt = ((GAMETYPE)GameType.getSelectedItem()).tag();
			dataTag.put("GameType", gt);
			playerTag.put("playerGameType", gt);

			nbt.write(levelFile);
			saved = true;
		}
		catch (Exception e) {
			saved = false;
			LAD.showErrorMessage(mcparent, e.getLocalizedMessage());
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == hardcore) {
			if (hardcore.isSelected()) {
				JLabel note = new JLabel("Si pierde, Minecraft borrará el mundo sin poder respawnearlo.");
				if (!LAD.showConfirmDialogYesNo(mcparent, "¿Seguro que quiere activar HARDCORE?", note)) {
					hardcore.setSelected(false);
				}
			}
		}
		else if (source == resetButton) {
			if (LAD.showConfirmDialogYesNo(mcparent, "¿Desea restablecer todos los valores a sus valores por defecto? ")) {
				reset();
			}
		}
		else if (source == okButton || source == cancelButton) {
			if (source == okButton) {
				if (LAD.showConfirmDialogYesCancel(mcparent, "¿Desea guardar los cambios? ")) {
					write();
				} else {
					return;
				}
			} else {
				if (!LAD.showConfirmDialogYesNo(mcparent, "¿Desea salir sin guardar los cambios? ")) {
					return;
				}
			}
			setVisible(false);
			dispose();
		}
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		Object source = e.getSource();
		if (source == foodLevel) {
			foodSaturationLevelModel.setMaximum(foodLevelModel.getNumber().doubleValue());
			if (foodSaturationLevelModel.getNextValue() == null) {
				foodSaturationLevelModel.setValue(foodSaturationLevelModel.getMaximum());
			}
		}
	}

	public String getLevelName() {
		return newLevelName;
	}

	public boolean isSaved() {
		return saved;
	}
}