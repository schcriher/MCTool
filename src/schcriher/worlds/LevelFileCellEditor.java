package schcriher.worlds;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.net.URI;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import schcriher.LAD;

public class LevelFileCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {

	private static final long serialVersionUID = 1L;
	private final JButton button;
	private final Worlds worlds;
	private final URI root;
	private LevelFileDialog dialog;
	private WorldsTableModel model;
	private Integer rowIndex;
	private String levelName;
	private File fLevelDat;

	public LevelFileCellEditor(Worlds worlds) {
		this.worlds = worlds;
		button = new JButton();
		button.addActionListener(this);
		button.setBorderPainted(false);
		root = worlds.core.fRoot.getParentFile().toURI();
	}

	// TableCellEditor

	@Override
	public boolean isCellEditable(EventObject e) {
        if (e instanceof MouseEvent) {
            return ((MouseEvent)e).getClickCount() >= 2;
        }
        return true;
    }

	@Override  /* #1 */
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		model = (WorldsTableModel)table.getModel();
		rowIndex = row;
		fLevelDat = model.getLevelFile(row);
		levelName = (String)value;
		button.setText(String.format("<html><i>« %s »</i></html>", levelName));
		return button;
	}

	// ActionListener

	@Override  /* #2 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == button) {
			dialog = new LevelFileDialog(worlds.core.mcparent, root);
			dialog.load(fLevelDat);
			dialog.setVisible(true);
			if (!dialog.isSaved()) {
				model.setLockRow(true, rowIndex);
				worlds.settingsForOneWorldSelected();
				LAD.showErrorMessage(worlds.core.mcparent, "Error editando los datos del mundo");
			}
			fireEditingStopped();
			dialog.dispose();
		}
	}

	// AbstractCellEditor

	@Override  /* #3 */
	public Object getCellEditorValue() {
		levelName = dialog.getLevelName();
		button.setText(null);
		return levelName;
	}
}