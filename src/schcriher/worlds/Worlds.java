package schcriher.worlds;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import schcriher.DuplicateRemover;
import schcriher.Exporter;
import schcriher.LAD;
import schcriher.Resources;
import schcriher.Schcriher;
import schcriher.generic.AlphabeticalOrder;
import schcriher.generic.DateTableCellRenderer;
import schcriher.generic.TracerProgress;
import schcriher.generic.UnselectionTableMouseListener;
import schcriher.nbt.NBT;
import schcriher.nbt.TAG_Compound;
import schcriher.utils.Archives;
import schcriher.utils.Archives.Operation;
import schcriher.utils.Utils;
import schcriher.utils.Zip;
import schcriher.waitdialogs.WaitDialogWorker;

public class Worlds extends JPanel implements ActionListener, ListSelectionListener {

	private static final long serialVersionUID = 1L;

	public static final Pattern patternNameFileBackup = Pattern.compile("(.+)(-\\d{8}-\\d{9}[+-]\\d{4}\\.zip)", Pattern.CASE_INSENSITIVE);
	public static final SimpleDateFormat dateFormatFileBackup = new SimpleDateFormat("-yyyyMMdd-HHmmssSSSZ'.zip'");
	public static final SimpleDateFormat dateFormatDisplayBackup = new SimpleDateFormat("'Copia del' dd/MM/yyyy 'a las' HH:mm:ss (z)");

	public final Map<String, BackupsTableModel> mapBackups = new HashMap<String, BackupsTableModel>();
	public final Set<String> dirNames = new HashSet<String>();

	private static final String CNAME = Worlds.class.getName();
	private static final Logger LOGGER = Logger.getLogger(CNAME);

	private JButton buttonCreate;
	private JButton buttonRestore;

	private WorldsTableModel modelWorlds;
	private BackupsTableModel modelBackupsEmpty;
	private JTable tableWorlds;
	private JTable tableBackups;

	public final boolean initialized;
	public final Schcriher core;

    public Worlds(Schcriher core) {
    	long start = System.currentTimeMillis();
    	this.core = core;
    	initComponents();
    	listFiles();
    	initialized = true;
    	LOGGER.fine(String.format("Schcriher Worlds Initialized in %dms", System.currentTimeMillis() - start));
    }

	public void listFiles() {
		modelWorlds.clear();
		mapBackups.clear();
		try {
    		listSaves(core.fSaves, true);
    		listSaves(core.fSavesBackups, false);
    		listSavesBackups();
    	}
    	catch (Exception e) {
    		LAD.showErrorMessage(core.mcparent, e.getLocalizedMessage());
    	}
	}

    private void listSaves(File file, boolean enable) throws Exception {
    	if (!file.exists()) {
    		file.mkdirs();
    	}
    	if (!file.isDirectory()) {
    		throw new IOException("Could not create the folder " + file.getAbsolutePath());
    	}
    	List<File> files = Arrays.asList(file.listFiles());
		Collections.sort(files, new AlphabeticalOrder<File>());
    	for (File f: files) {
    		addFileSave(f, enable);
    	}
    }

    private void addFileSave(File dirFile, boolean enable) {
    	if (dirFile.isDirectory()) {
    		try {
    			File fLevelDat;
    			NBT nbt;
    			try {
    				fLevelDat = new File(dirFile, "level.dat");
    				nbt = new NBT();
    				nbt.load(fLevelDat);
    			}
    			catch (Exception e1) {
    				try {
    					fLevelDat = new File(dirFile, "level.dat_old");
    					nbt = new NBT();
    					nbt.load(fLevelDat);
    				}
    				catch (Exception e2) {
    					return;  // Carpeta no válida
    				}
    			}
    			TAG_Compound rootTag = nbt.getRootTagCompound();
    			TAG_Compound dataTag = rootTag.getCompound("Data");
    			modelWorlds.addRow(enable, fLevelDat, dataTag.getString("LevelName"), false);
    			dirNames.add(dirFile.getName());
    		}
    		catch (Exception e) {
    			LOGGER.fine(e.getLocalizedMessage());
    		}
    	}
    }

    private void listSavesBackups() throws Exception {
    	if (!core.fSavesBackups.exists()) {
    		core.fSavesBackups.mkdirs();
    	}
    	if (!core.fSavesBackups.isDirectory()) {
    		throw new IOException("Could not create the folder " + core.fSavesBackups.getAbsolutePath());
    	}
    	List<File> files = Arrays.asList(core.fSavesBackups.listFiles());
		Collections.sort(files, new AlphabeticalOrder<File>());
    	for (File f: files) {
    		if (f.isFile()) {
    			Matcher m = patternNameFileBackup.matcher(f.getName());
    			if (m.matches()) {
    				try {
    					String dirName = m.group(1);
    					Date date = dateFormatFileBackup.parse(m.group(2));
    					addFileSaveBackup(dirName, f, date);
    				}
    				catch (Exception e) {
    					LOGGER.fine(e.getLocalizedMessage());
    				}
    			}
    		}
    	}
    }

    private void addFileSaveBackup(String dirName, File fSaveBackup, Date date) {
    	if (mapBackups.containsKey(dirName)) {
    		mapBackups.get(dirName).addRow(fSaveBackup, date);
    	} else {
    		BackupsTableModel model = new BackupsTableModel(dirName);
    		model.addRow(fSaveBackup, date);
    		mapBackups.put(dirName, model);
    		int srow = tableWorlds.getSelectedRow();
    		if (initialized && srow >= 0 && modelWorlds.getDirName(srow).equals(dirName)) {
    			tableBackups.setModel(model);
    			tableBackups.setEnabled(true);
    		}
    	}
    	if (!initialized && !dirNames.contains(dirName)) {
    		// Un backup sin su carpeta descomprimida
    		File newFile = new File(core.fSavesBackups, dirName);
    		if (Zip.decompressor(fSaveBackup, newFile)) {
    			addFileSave(newFile, false);
    			dirNames.add(dirName);
    		} else {
    			Archives.recursively(fSaveBackup, Operation.DELETE);
    		}
    	}
    }

    private void initComponents() {
        GridBagLayout layout = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();

        layout.columnWeights = new double[]{1.0, 0.0, 1.0};
        layout.rowWeights = new double[]{1.0, 0.0, 0.0, 1.0};

        setLayout(layout);

        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.weighty = 1.0;
        c.weightx = 1.0;
        c.insets = new Insets(0, 0, 0, 0);
        c.fill = GridBagConstraints.BOTH;

        modelWorlds = new WorldsTableModel();
        tableWorlds = new WorldsTable(modelWorlds);
        tableWorlds.addMouseListener(new UnselectionTableMouseListener());
        tableWorlds.setFillsViewportHeight(true);
        tableWorlds.setGridColor(Resources.TABLE_GRID_COLOR);
        tableWorlds.setShowGrid(true);
        tableWorlds.setShowVerticalLines(false);
        tableWorlds.setRowSelectionAllowed(true);
        tableWorlds.setColumnSelectionAllowed(false);
        tableWorlds.getSelectionModel().addListSelectionListener(this);
        tableWorlds.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        tableWorlds.getColumnModel().getColumn(0).setResizable(false);
        tableWorlds.getColumnModel().getColumn(0).setMaxWidth(27);
        tableWorlds.getColumnModel().getColumn(1).setCellRenderer(new DirNameCellRenderer());
        tableWorlds.getColumnModel().getColumn(0).setCellEditor(new AvailableCellEditor(this));
        tableWorlds.getColumnModel().getColumn(1).setCellEditor(new DirNameCellEditor(this));
        tableWorlds.getColumnModel().getColumn(2).setCellEditor(new LevelFileCellEditor(this));
        c.gridx = 0;
        c.gridheight = 4;
        add(new JScrollPane(tableWorlds), c);

        modelBackupsEmpty = new BackupsTableModel(null);
        tableBackups = new JTable(modelBackupsEmpty);
        tableBackups.addMouseListener(new UnselectionTableMouseListener());
        tableBackups.setFillsViewportHeight(true);
        tableBackups.setGridColor(Resources.TABLE_GRID_COLOR);
        tableBackups.setShowGrid(true);
        tableBackups.setShowVerticalLines(false);
        tableBackups.setRowSelectionAllowed(true);
        tableBackups.setColumnSelectionAllowed(false);
        tableBackups.getSelectionModel().addListSelectionListener(this);
        tableBackups.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        tableBackups.setDefaultRenderer(Date.class, new DateTableCellRenderer(dateFormatDisplayBackup));
        tableBackups.setAutoCreateColumnsFromModel(false);
        c.gridx = 2;
        c.gridheight = 4;
        add(new JScrollPane(tableBackups), c);

        Component vglue = Box.createVerticalGlue();
        c.gridx = 1;
        c.gridy = 0;
        c.gridheight = 1;
        c.weighty = 0.0;
        c.weightx = 0.0;
        c.insets = new Insets(0, 5, 0, 5);
        add(vglue, c);


        buttonCreate = new JButton();
        buttonCreate.setIcon(Resources.ARROW_RIGHT_ICON);
        buttonCreate.setToolTipText("Crear copia de seguridad");
        buttonCreate.addActionListener(this);
        c.insets = new Insets(0, 5, 5, 5);
        c.gridy = 1;
        add(buttonCreate, c);

        buttonRestore = new JButton();
        buttonRestore.setIcon(Resources.ARROW_LEFT_ICON);
        buttonRestore.setToolTipText("Restablecer copia de seguridad");
        buttonRestore.addActionListener(this);
        c.insets = new Insets(0, 5, 0, 5);
        c.gridy = 2;
        add(buttonRestore, c);

        c.gridx = 1;
        c.gridy = 3;
        add(vglue, c);

        settingsForNoneWorldSelected();
    }

    private boolean createSaveBackup(TracerProgress trace) {
    	List<String> errors = new ArrayList<String>();
    	for (int i: tableWorlds.getSelectedRows()) {
    		Date date = new Date();
        	File file = modelWorlds.getDirNameFile(i);
        	String dirName = modelWorlds.getDirName(i);
        	String dateName = dateFormatFileBackup.format(date);
        	File fSaveBackup = new File(core.fSavesBackups, dirName + dateName);
        	trace.text(dirName);
        	if (Zip.compressor(file, fSaveBackup, trace)) {
        		addFileSaveBackup(dirName, fSaveBackup, date);
        	} else {
        		modelWorlds.setLockRow(true, i);
        		errors.add(dirName);
        	}
    	}
    	if (!errors.isEmpty()) {
    		if (tableWorlds.getSelectedRowCount() == 1) {
    			errors.remove(0);
    			errors.add("Error creando la copia de seguridad");
    		} else {
    			errors.add(0, "Estos archivos tuvieron errores");
        		errors.add(1, "");
    		}
    		LAD.showErrorMessage(core.mcparent, errors);
    		return false;
    	}
    	return true;
    }

    private boolean restoreSaveBackup(TracerProgress trace) {
    	int rowIndexWorlds = tableWorlds.getSelectedRow();
    	int rowIndexBackups = tableBackups.getSelectedRow();
    	File dir = modelWorlds.getLevelFile(rowIndexWorlds).getParentFile();
    	File zip = ((BackupsTableModel)tableBackups.getModel()).getFile(rowIndexBackups);
    	Archives.recursively(dir, Archives.Operation.DELETE);
    	boolean success = Zip.decompressor(zip, dir, trace);
    	if (!success) {
    		modelWorlds.setLockRow(true, rowIndexWorlds);
    		LAD.showErrorMessage(core.mcparent, "Error restableciendo la copia de seguridad");
    	}
    	return success;
    }

    public boolean setAvailableWorlds(boolean available, int rowIndex) {
    	if (!modelWorlds.getLockRow(rowIndex)) {
    		File levelFile = modelWorlds.getLevelFile(rowIndex);
    		File oldFile = levelFile.getParentFile();
    		File newFile = new File(available ? core.fSaves : core.fSavesBackups, modelWorlds.getDirName(rowIndex));
    		if (!oldFile.equals(newFile)) {
    			if (Archives.move(oldFile, newFile, true)) {
    				modelWorlds.setAvailable(available, rowIndex);
    				modelWorlds.setLevelFile(new File(newFile, levelFile.getName()), rowIndex);
    				return true;
    			} else {
    				modelWorlds.setLockRow(true, rowIndex);
    				LAD.showRenameErrorMessageNotLog(core.mcparent, oldFile, newFile);
    			}
    		}
    	}
    	return false;
    }

    public boolean setNameBackupsTableModelCurrent(String newName) {
    	BackupsTableModel model = (BackupsTableModel)tableBackups.getModel();
    	Stack<File> changes = new Stack<File>();  // last-in-first-out (LIFO)
    	int rows = model.getRowCount();
    	for (int i = 0; i < rows; i++) {
    		File oldDir = model.getFile(i);
    		Date date = model.getDate(i);
    		String name = newName + Worlds.dateFormatFileBackup.format(date);
    		File newDir = new File(oldDir.getParentFile(), name);
    		if (Archives.move(oldDir, newDir, true)) {
    			changes.push(oldDir);
    			changes.push(newDir);
    			model.setFile(i, newDir);
    		} else {
    			// ERROR, revert changes
    			LAD.showRenameErrorMessageNotLog(core.mcparent, oldDir, newDir);
    			for (int j = i-1; j >= 0; j--) {
    				newDir = changes.pop();
    				oldDir = changes.pop();
    				Archives.move(newDir, oldDir, true);
    				model.setFile(j, oldDir);
    			}
    			return false;
    		}
    	}
    	String oldName = model.getName();
    	dirNames.remove(oldName);
    	mapBackups.remove(oldName);
    	dirNames.add(newName);
    	mapBackups.put(newName, model.setName(newName));
    	return true;
    }

    public void setWorldsTableModelLockRow(int rowIndex, boolean lock) {
    	modelWorlds.setLockRow(lock, rowIndex);
    }

	public void deleteRepeated() {
		List<File> files = new ArrayList<File>();
		List<File> folders = new ArrayList<File>();

    	int nw = modelWorlds.getRowCount();
    	for (int i = 0; i < nw; i++)
    		folders.add(modelWorlds.getDirNameFile(i));

    	for (BackupsTableModel model: mapBackups.values()) {
    		int nb = model.getRowCount();
    		for (int i = 0; i < nb; i++) {
    			files.add(model.getFile(i));
    		}
    	}

    	DuplicateRemover remover = new DuplicateRemover(core.mcparent, files, folders);
    	remover.execute();
    	remover.setVisible();
    	if (remover.removed()) {
    		listFiles();
    	}
    }

    public void deleteSelected() {
    	boolean isOneWorldSelected = tableWorlds.getSelectedRowCount() == 1;
    	boolean isBackupsSelected = tableBackups.getSelectedRowCount() > 0;
    	String question = null;
    	JCheckBox check = null;

    	if (isOneWorldSelected) {
    		if (isBackupsSelected) {
    			question = "¿Eliminar las copias de seguridad seleccionadas?";
    			check = new JCheckBox("Eliminar el mundo completamente");
    			check.setSelected(false);
    		} else {
    			question = "¿Eliminar el mundo seleccionado incluyendo sus copias de seguiridad?";
    		}
    	} else {
    		question = "¿Eliminar los mundos seleccionados incluyendo sus copias de seguiridad?";
    	}
    	if (LAD.showConfirmDialogYesCancel(core.mcparent, question, check)) {
    		if (isOneWorldSelected) {
    			int rowIndex = tableWorlds.getSelectedRow();
    			if (isBackupsSelected) {
    				if (check.isSelected()) {
    					if (!deleteFullWorld(rowIndex, true)) {
    						modelWorlds.setLockRow(true, rowIndex);
    					}
    				} else {
    					String dirName = modelWorlds.getDirName(rowIndex);
    					deleteBackups(dirName, tableBackups.getSelectedRows());
    				}
    			} else {
    				if (!deleteFullWorld(rowIndex, true)) {
    					modelWorlds.setLockRow(true, rowIndex);
    				}
    			}
    		} else {
    			int[] rows = tableWorlds.getSelectedRows();
    			for (int i = rows.length - 1; i >= 0; i--) {
    				if (!deleteFullWorld(rows[i], false)) {
    					modelWorlds.setLockRow(true, rows[i]);
    					break;
    				}
    			}
    		}
    	}
    	valueChanged(new ListSelectionEvent(this, -1, -1, false));
    }

    public boolean deleteFullWorld(int rowIndex, boolean unique) {
    	String dirName = modelWorlds.getDirName(rowIndex);
    	if (mapBackups.containsKey(dirName)) {
    		if (!deleteBackups(dirName, null)) {
    			return false;
    		}
    	}
    	File file = modelWorlds.getDirNameFile(rowIndex);
    	if (!Archives.recursively(file, Operation.DELETE)) {
    		if (!unique) {
    			String q = String.format("¿Desea continuar? El mundo '%s' no se pudo eliminar", file.getName());
    			if (!LAD.showConfirmDialogYesNo(core.mcparent, q)) {
    				return false;
    			}
    		} else {
    			LAD.showErrorMessage(core.mcparent, "No se pudo eliminar el mundo");
    			return false;
    		}
    	}
    	modelWorlds.removeRow(rowIndex);
    	return true;
    }

    public boolean deleteBackups(String dirName, int[] rowIndexes) {
    	List<Integer> deleteRowIndexes = new ArrayList<Integer>();
    	List<String> nonDelete = new ArrayList<String>();
    	BackupsTableModel model = mapBackups.get(dirName);
    	int nRow = model.getRowCount();

    	if (rowIndexes == null) {
    		rowIndexes = Utils.range(nRow);
    	}
    	for (int i: rowIndexes) {
    		File file = model.getFile(i);
    		if (Archives.Operation.DELETE.make(file)) {
    			deleteRowIndexes.add(i);
    		} else {
    			nonDelete.add(file.getPath());
    		}
    	}
    	if (!nonDelete.isEmpty()) {
    		nonDelete.add(0, "¿Desea continuar? Los siguientes archivos no se pudieron eliminar:");
    		nonDelete.add(1, "");
    		if (!LAD.showConfirmDialogYesNo(core.mcparent, nonDelete)) {
    			model.removeRows(deleteRowIndexes);  // estos archivos ya fueron eliminados
    			return false;
    		}
    	}
    	if (deleteRowIndexes.size() == nRow) {
    		if (model == tableBackups.getModel()) {
    			tableBackups.setModel(modelBackupsEmpty);
    		}
    		model.clear();
    		mapBackups.remove(dirName);
    	} else {
    		model.removeRows(deleteRowIndexes);
    	}
    	return true;
    }

    public void exportSelected(File dst) {
    	final List<File> src = new ArrayList<File>();
    	boolean worlds = true;
    	boolean backups = false;
    	if (tableBackups.getSelectedRowCount() > 0) {
    		String question = "Hay copias de seguridad seleccionadas. ¿Desea exportar de todos modos?";
    		final JCheckBox checkW = new JCheckBox("Incluir en la exportación el mundo seleccionado");
    		final JCheckBox checkB = new JCheckBox("Incluir en la exportación las copias de seguridad seleccionadas");
    		checkW.setSelected(false);
    		checkB.setSelected(true);
    		if (LAD.showConfirmDialogYesCancel(core.mcparent, question, checkW, checkB)) {
    			worlds = checkW.isSelected();
    			backups = checkB.isSelected();
    		} else {
    			return;
    		}
    	}
    	if (worlds) {
    		for (int i: tableWorlds.getSelectedRows()) {
    			src.add(modelWorlds.getLevelFile(i).getParentFile());
    		}
    	}
    	if (backups) {
    		BackupsTableModel model = (BackupsTableModel) tableBackups.getModel();
    		for (int i: tableBackups.getSelectedRows()) {
    			src.add(model.getFile(i));
    		}
    	}
    	Exporter exporter = new Exporter(core.mcparent, dst, src);
    	exporter.execute();
    	exporter.setVisible();
    }

    public void settingsForOneWorldSelected() {
    	if (modelWorlds.getLockRow(tableWorlds.getSelectedRow())) {
    		buttonCreate.setEnabled(false);
    		buttonRestore.setEnabled(false);
    		tableBackups.setEnabled(false);
    	} else {
    		buttonCreate.setEnabled(true);
    		if (tableBackups.getModel() == modelBackupsEmpty) {
    			tableBackups.setEnabled(false);
    			buttonRestore.setEnabled(false);
    		} else {
    			tableBackups.setEnabled(true);
    		}
    	}
    }

    public void settingsForNoneWorldSelected() {
    	settingsForNoneOrMultipleWorldSelected();
    	buttonCreate.setEnabled(false);
    }

    public void settingsForMultipleWorldSelected() {
    	settingsForNoneOrMultipleWorldSelected();
    	buttonCreate.setEnabled(true);
    }

    private void settingsForNoneOrMultipleWorldSelected() {
    	tableBackups.setModel(modelBackupsEmpty);
    	tableBackups.setEnabled(false);
    	buttonRestore.setEnabled(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    	Object source = e.getSource();
    	if (source == buttonCreate) {
    		String title;
    		String question;
    		if (tableWorlds.getSelectedRowCount() == 1) {
    			title = "Creando copia de seguridad";
    			question = "¿Crear copia de seguridad para el mundo seleccioando?";
    		} else {
    			title = "Creando copias de seguridad";
    			question = "¿Crear copia de seguridad para todos los mundos seleccioandos?";
    		}
    		if (LAD.showConfirmDialogYesNo(core.mcparent, question)) {
    			WaitDialogWorker worker = new WaitDialogWorker(core.mcparent, title) {
    				@Override
    				protected Void doInBackground() throws Exception {
    					createSaveBackup(this);
    					return null;
    				}
    			};
    			worker.execute();
    			worker.setVisible();
    		}
    	}
    	else if (source == buttonRestore) {
    		String question = "¿Volver a esta versión del mundo mas antigua?";
    		final JCheckBox check = new JCheckBox("Crear un copia de seguridad de la versión actual (¡será borrada!)");
    		check.setSelected(true);
    		if (LAD.showConfirmDialogYesCancel(core.mcparent, question, check)) {
    			WaitDialogWorker worker = new WaitDialogWorker(core.mcparent, "Creando copia de seguridad") {
    				@Override
    				protected Void doInBackground() throws Exception {
    					if (check.isSelected()) {
    						boolean success = createSaveBackup(this);
    						if (!success) {
    							return null;
    						}
    					}
    					publish("Restaurando mundo");
    					restoreSaveBackup(this);
    					return null;
    				}
    			};
    			worker.execute();
    			worker.setVisible();
    		}
    	}
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
    	if (!e.getValueIsAdjusting()) {
    		Object source = e.getSource();
    		if (source == tableWorlds.getSelectionModel()) {
    			int rows = tableWorlds.getSelectedRowCount();
    			if (rows == 1) {
    				String oldName = ((BackupsTableModel) tableBackups.getModel()).getName();
    				String newName = modelWorlds.getDirName(tableWorlds.getSelectedRow());
    				if (oldName != newName) {
    					if (mapBackups.containsKey(newName)) {
    						tableBackups.setModel(mapBackups.get(newName));
    					} else {
    						tableBackups.setModel(modelBackupsEmpty);
    					}
    				}
    				settingsForOneWorldSelected();
    			}
    			else if (rows > 1) {
    				settingsForMultipleWorldSelected();
    			}
    			else {
    				settingsForNoneWorldSelected();
    			}
    		}
    		else if (source == tableBackups.getSelectionModel()) {
    			if (tableBackups.getSelectedRowCount() == 1) {
    				buttonRestore.setEnabled(true);
    			} else {
    				buttonRestore.setEnabled(false);
    			}
    		}
    		core.setEnabledExportButton(tableWorlds.getSelectedRowCount() + tableBackups.getSelectedRowCount() > 0);
    	}
    }
}