package schcriher.worlds;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import schcriher.generic.TableModel;

public class BackupsTableModel extends TableModel {

	private static final long serialVersionUID = 1L;
	private String name;

	public BackupsTableModel(String name) {
		super(new Object[][] {
				{"FILE", null,                                                      File.class, false, false},
				{"DATE", "Fecha de las copias de seguridad del mundo seleccionado", Date.class, true,  false},
		});
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public BackupsTableModel setName(String name) {
		this.name = name;
		return this;
	}


	@Override
	public String toString() {
		return String.format("%s, name=%s", super.toString(), name);
	}

	// Getter

	public File getFile(int rowIndex) {
		return (File)getter(rowIndex, "FILE");
	}

	public List<File> getAllFiles() {
		int nrow = getRowCount();
		List<File> list = new ArrayList<File>(nrow);
		for (int i = 0; i < nrow; i++) {
			list.add(getFile(i));
		}
		return list;
	}

	public Date getDate(int rowIndex){
		return (Date)getter(rowIndex, "DATE");
	}

	// Setter

	public void setFile(int rowIndex, File file) {
		setter(file, rowIndex, "FILE");
	}

	public void setDate(int rowIndex, Date date) {
		setter(date, rowIndex, "DATE");
	}
}