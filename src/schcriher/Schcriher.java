package schcriher;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;

import schcriher.importer.Importer;
import schcriher.mods.Mods;
import schcriher.packs.Packs;
import schcriher.utils.Archives;
import schcriher.utils.Archives.Operation;
import schcriher.utils.Utils;
import schcriher.worlds.Worlds;

public class Schcriher extends JPanel implements ActionListener, ChangeListener, ComponentListener {

	private static final long serialVersionUID = 1L;
	private static final String CNAME = Schcriher.class.getName();
	private static final Logger LOGGER = Logger.getLogger(CNAME);

	public static final String VERSION = Resources.VERSION;
	public static final String TITLE = "MCTool " + VERSION + " by Schmidt Cristian Hernán";

	private final List<Boolean> changes = Collections.synchronizedList(new ArrayList<Boolean>(3));

	public final int TAB_WORLDS = 0;
	public final int TAB_PACKS = 1;
	public final int TAB_MODS = 2;

	public final JFrame mcparent;
	public final boolean initialized;
	public final boolean useWatchDogs;
	public final boolean forgeInstalled;

	public final File fRoot;
	public final File fConfig;

	public final File fMods;			// http://minecraft.gamepedia.com/Mods/Installing_forge_mods
	public final File fSaves;			// http://minecraft.gamepedia.com/Level_format
	public final File fTexturePacks;	// http://minecraft.gamepedia.com/Texture_pack    (MC <= 1.5)
	public final File fResourcePacks;	// http://minecraft.gamepedia.com/Resource_pack   (MC >= 1.6)

	public final File fBase;
	public final File fTemporal;
	public final File fModsBackups;
	public final File fSavesBackups;
	public final File fTexturePacksBackups;
	public final File fResourcePacksBackups;

	public final File fJavaTemporal;

	public final Worlds worlds;
	public final Packs packs;
	public final Mods mods;

	private JTabbedPane tabbedPane;

	private JLabel labelmctool;
	private JLabel labelWorlds;
	private JLabel labelPacks;
	private JLabel labelMods;

	private JPanel buttonsPanel;
	private JButton buttonImport;
	private JButton buttonExport;
	private JButton buttonDelete;

	public Schcriher(JFrame mcparent, File fMinecraft) throws Exception {
		super(true);
		LOGGER.fine("Schcriher Core Starting...");
		long start = System.currentTimeMillis();
		this.mcparent = mcparent;

		useWatchDogs = Utils.useWatchService(fMinecraft);
		forgeInstalled = Utils.isForgeInstalled(new File(fMinecraft, "versions"));

		fRoot = fMinecraft;
		fConfig = new File(fRoot, "config");

		fMods = new File(fRoot, "mods");
		fSaves = new File(fRoot, "saves");
		fTexturePacks = new File(fRoot, "texturepacks");
		fResourcePacks = new File(fRoot, "resourcepacks");

		fBase = new File(fRoot, "schcriher");
		fModsBackups = new File(fBase, "mods");
		fSavesBackups = new File(fBase, "saves");
		fTexturePacksBackups = new File(fBase, "texturepacks");
		fResourcePacksBackups = new File(fBase, "resourcepacks");

		File tmp = new File(fBase, "tmp");
		fTemporal = new File(tmp, "decompress");
		fJavaTemporal = new File(tmp, "javatmpdir");

		prepareFolders(fBase, false);
		prepareFolders(fTemporal, true);
		prepareFolders(fJavaTemporal, false);

		System.setProperty("java.io.tmpdir", fJavaTemporal.toString());

		worlds = new Worlds(this);
		packs = new Packs(this);
		mods = new Mods(this);

		initComponents();
		initWatchDogs();

		initialized = true;
		LOGGER.fine(String.format("Schcriher Core Initialized in %dms", System.currentTimeMillis() - start));
		Runtime.getRuntime().gc();
	}

	private void prepareFolders(File file, boolean clean) throws Exception {
		if (!file.exists()) {
			file.mkdirs();
		}
		if (clean) {
			Archives.recursivelyWitoutOwner(file, Operation.DELETE);
		}
		if (!file.isDirectory()) {
			throw new IOException("Could not create the folder " + file.getAbsolutePath());
		}
	}

	private void initComponents() {
		labelWorlds = new JLabel("Mundos");
		labelPacks = new JLabel("Paquetes");
		labelMods = new JLabel("Modificaciones");

		EmptyBorder b = new EmptyBorder(1, 5, 1, 5);
		labelWorlds.setBorder(b);
		labelPacks.setBorder(b);
		labelMods.setBorder(b);

		tabbedPane = new JTabbedPane();
		tabbedPane.setTabPlacement(SwingConstants.TOP);
		tabbedPane.addChangeListener(this);

		tabbedPane.insertTab(null, null, worlds, null, TAB_WORLDS);
		tabbedPane.insertTab(null, null, packs, null, TAB_PACKS);
		tabbedPane.insertTab(null, null, mods, null, TAB_MODS);

		tabbedPane.setTabComponentAt(TAB_WORLDS, labelWorlds);
		tabbedPane.setTabComponentAt(TAB_PACKS, labelPacks);
		tabbedPane.setTabComponentAt(TAB_MODS, labelMods);

		labelmctool = new JLabel(TITLE);
		labelmctool.setToolTipText("schcriher@gmail.com");
		labelmctool.setForeground(new Color(28, 146, 28));
		labelmctool.setFont(new Font("Tahoma", Font.BOLD, 12));

		Dimension labelmctoolSize = labelmctool.getPreferredSize();
		labelmctool.setBounds(0, 0, labelmctoolSize.width, labelmctoolSize.height);
		add(labelmctool);

		buttonsPanel = new JPanel(new GridLayout(1, 3, 6, 0));

		buttonImport = new JButton(" Importar ");
		buttonImport.addActionListener(this);
		buttonsPanel.add(buttonImport);

		buttonExport = new JButton(" Exportar ");
		buttonExport.addActionListener(this);
		buttonsPanel.add(buttonExport);

		buttonDelete = new JButton(" Eliminar ");
		buttonDelete.addActionListener(this);
		buttonsPanel.add(buttonDelete);

		Dimension buttonsPanelSize = buttonsPanel.getPreferredSize();
		buttonsPanel.setBounds(0, 0, buttonsPanelSize.width, buttonsPanelSize.height);
		add(buttonsPanel);

		setLayout(new BorderLayout());
		add(tabbedPane, BorderLayout.CENTER);
		setBorder(new EmptyBorder(5, 5, 5, 5));
		addComponentListener(this);

		buttonImport.setEnabled(true);
		buttonExport.setEnabled(false);
		buttonDelete.setEnabled(true);

		int htab0 = tabbedPane.getBoundsAt(0).height;
		int htab1 = tabbedPane.getBoundsAt(1).height;
		int htab2 = tabbedPane.getBoundsAt(2).height;
		int htabs = Math.max(Math.max(htab0, htab1), htab2);

		int hmin = Math.max(labelmctoolSize.height, buttonsPanelSize.height) + 2;

		if (hmin > htabs) {
			Dimension dw = labelWorlds.getPreferredSize();
			Dimension dp = labelPacks.getPreferredSize();
			Dimension dm = labelMods.getPreferredSize();
			dw.height = hmin;
			dp.height = hmin;
			dm.height = hmin;
			labelWorlds.setPreferredSize(dw);
			labelPacks.setPreferredSize(dp);
			labelMods.setPreferredSize(dm);
		}
	}

	private void initWatchDogs() {
		changes.add(false);
		changes.add(false);
		changes.add(false);

		if (useWatchDogs) {
			createWatchDog(fSaves, TAB_WORLDS);
			createWatchDog(fSavesBackups, TAB_WORLDS);

			createWatchDog(fTexturePacks, TAB_PACKS);
			createWatchDog(fTexturePacksBackups, TAB_PACKS);

			createWatchDog(fResourcePacks, TAB_PACKS);
			createWatchDog(fResourcePacksBackups, TAB_PACKS);

			createWatchDog(fMods, TAB_MODS);
			createWatchDog(fModsBackups, TAB_MODS);
		}
	}

	private void createWatchDog(File dir, int key) {
		try {
			ChangeDirInformer informer = new ChangeDirInformer(dir.toPath(), false, changes, key);
			new Thread(informer).start();
		}
		catch (Exception | java.lang.AssertionError e) {
			e.printStackTrace();
		}
	}

	public void setEnabledExportButton(boolean b) {
		buttonExport.setEnabled(b);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == buttonImport) {
			JFileChooser chooser = new JFileChooser();
			chooser.setDialogTitle("Elija los archivos a importar");
			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			chooser.setMultiSelectionEnabled(true);
			if (chooser.showOpenDialog(mcparent) == JFileChooser.APPROVE_OPTION) {
				Importer importer = new Importer(this, "Analizando e importando archivos", chooser.getSelectedFiles());
				importer.execute();
				importer.setVisible();
				if (importer.isRuntimeException()) {
					buttonImport.setEnabled(false);
					buttonImport.setToolTipText("Error con la biblioteca SevenZipJBinding");
				}
			}
		}
		else if (source == buttonExport) {
			int index = tabbedPane.getSelectedIndex();
			String title = "";
			String toolTip = null;
			switch (index) {
			case TAB_WORLDS:
				title = "Exportando mundos";
				toolTip = "Los mundos se exportarán en archivos *.zip";
				break;
			case TAB_PACKS:
				title = "Exportando paquetes";
				break;
			case TAB_MODS:
				title = "Exportando Modificaciones";
				break;
			}
			JFileChooser chooser = LAD.getDirectoriesChooser(title, toolTip);
			if (chooser.showSaveDialog(mcparent) == JFileChooser.APPROVE_OPTION) {
				File dst = chooser.getSelectedFile();
				dst = (dst != null) ? dst : chooser.getCurrentDirectory();
				switch (index) {
				case TAB_WORLDS: worlds.exportSelected(dst); break;
				case TAB_PACKS: packs.exportSelected(dst); break;
				case TAB_MODS: mods.exportSelected(dst); break;
				}
			}
		}
		else if (source == buttonDelete) {
			boolean selected = buttonExport.isEnabled();
			String question = "¿Qué desea eliminar en esta sección?";
			int messageType = JOptionPane.QUESTION_MESSAGE;
			int indexTab = tabbedPane.getSelectedIndex();

			Object[] options;
			if (selected) {
				options = new Object[] {"Configuraciones", "Repetidos", "Seleccionados", "Cancelar"};
			} else {
				options = new Object[] {"Configuraciones", "Repetidos", "Cancelar"};
			}

			int option = JOptionPane.showOptionDialog(mcparent, question, "Eliminar", 0, messageType, null, options, null);
			option = (!selected && option == 2) ? option + 1 : option;

			switch (option) {
			case 0:
				if (fConfig.isDirectory() && fConfig.listFiles().length > 0) {
					CfgRemover dialog = new CfgRemover(mcparent, fConfig);
					dialog.setVisible(true);
					Archives.emptyFolderRemove(fConfig);
				} else {
					LAD.showInformationMessage(mcparent, "No hay archivos de configuración");
				}
				break;

			case 1:
				switch (indexTab) {
				case TAB_WORLDS: worlds.deleteRepeated(); break;
				case TAB_PACKS: packs.deleteRepeated(); break;
				case TAB_MODS: mods.deleteRepeated(); break;
				}
				break;

			case 2:
				switch (indexTab) {
				case TAB_WORLDS: worlds.deleteSelected(); break;
				case TAB_PACKS: packs.deleteSelected(); break;
				case TAB_MODS: mods.deleteSelected(); break;
				}
				break;
			}
		}
		Runtime.getRuntime().gc();
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if (initialized) {
			ListSelectionEvent e2 = new ListSelectionEvent(e.getSource(), -1, -1, false);
			switch (tabbedPane.getSelectedIndex()) {
			case TAB_WORLDS:
				if (changes.get(TAB_WORLDS)) worlds.listFiles();
				worlds.valueChanged(e2);
				break;

			case TAB_PACKS:
				if (changes.get(TAB_PACKS)) packs.listFiles();
				packs.setTextureVisible();
				packs.valueChanged(e2);
				break;

			case TAB_MODS:
				if (changes.get(TAB_MODS)) mods.listFiles();
				mods.valueChanged(e2);
				break;
			}
		}
	}

	@Override
	public void componentResized(ComponentEvent e) {
		Rectangle tabbed = tabbedPane.getBounds();
		Rectangle buttons = buttonsPanel.getBounds();
		Rectangle label = labelmctool.getBounds();

		Rectangle tab0 = tabbedPane.getBoundsAt(0);
		Rectangle tab1 = tabbedPane.getBoundsAt(1);
		Rectangle tab2 = tabbedPane.getBoundsAt(2);

		int tabbedRBorder = 0;
		int labelOffset = 0;

		try {tabbedRBorder = tabbedPane.getBorder().getBorderInsets(tabbedPane).right;} catch (Exception ex) {}
		try {
			Insets labelBorder = labelmctool.getBorder().getBorderInsets(buttonsPanel);
			labelOffset = (int)(Math.round((labelBorder.right - labelBorder.left) / 2.0));
		} catch (Exception ex) {}

		int tabsWidth = tab0.width + tab1.width + tab2.width;
		int tabsHeight = Math.max(Math.max(tab0.height, tab1.height), tab2.height);

		int xb = tabbed.x + tabbed.width - buttons.width - tabbedRBorder;
		int yb = tabbed.y + (int)(Math.round((tabsHeight - buttons.height) / 2.0));

		int fl = (int)(Math.round((tabbed.width - buttons.width - tabsWidth - label.width) / 2.0));
		int xl = tabbed.x + tabsWidth + fl + labelOffset;
		int yl = tabbed.y + (int)(Math.round((tabsHeight - label.height) / 2.0));

		if (xl > tabsWidth) {
			labelmctool.setBounds(xl, yl, label.width, label.height);
			labelmctool.setVisible(true);
		} else {
			labelmctool.setVisible(false);
		}

		if (xb > tabsWidth) {
			buttonsPanel.setBounds(xb, yb, buttons.width, buttons.height);
			buttonsPanel.setVisible(true);
		} else {
			buttonsPanel.setVisible(false);
		}
	}

	@Override
	public void componentHidden(ComponentEvent e) {}

	@Override
	public void componentMoved(ComponentEvent e) {}

	@Override
	public void componentShown(ComponentEvent e) {}
}