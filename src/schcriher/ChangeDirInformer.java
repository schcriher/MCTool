package schcriher;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class ChangeDirInformer implements Runnable {

	private static final String CNAME = ChangeDirInformer.class.getName();
	private static final Logger LOGGER = Logger.getLogger(CNAME);

	private final Integer index;
	private final List<Boolean> changes;
	private final WatchService watcher;
	private final Map<WatchKey, Path> keys;
	private final boolean recursive;

	public ChangeDirInformer(Path dir, boolean recursive, List<Boolean> changes, int index) throws Exception {
		this.changes = changes;
		this.index = index;
		watcher = FileSystems.getDefault().newWatchService();
		keys = new HashMap<WatchKey, Path>();
		this.recursive = recursive;
		if (recursive) {
			registerAll(dir);
		} else {
			register(dir);
		}
	}

	private void register(Path dir) throws IOException {
		WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
		Path prev = keys.get(key);
		if (prev == null) {
			LOGGER.finest(String.format("register: %s", dir));
		} else if (!dir.equals(prev)) {
			LOGGER.finest(String.format("update: %s -> %s", prev, dir));
		}
		keys.put(key, dir);
	}

	private void registerAll(final Path start) throws IOException {
		Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				register(dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}

	@Override
	public void run() {
		boolean inform = false;
		while (true) {
			WatchKey key;
			try {key = watcher.take();} catch (InterruptedException e) {return;}

			Path dir = keys.get(key);
			if (dir == null) {
				continue;
			}

			for (WatchEvent<?> event: key.pollEvents()) {
				WatchEvent.Kind<?> kind = event.kind();
				if (kind == OVERFLOW) {
					continue;
				}
				if (kind == ENTRY_CREATE && recursive) {
					Path path = (Path)event.context();
					Path child = dir.resolve(path);
					try {
						if (Files.isDirectory(child, NOFOLLOW_LINKS)) {
							registerAll(child);
						}
					}
					catch (Exception e) {}
				}
				inform = true;
			}
			if (inform) {
				LOGGER.finest(String.format("change in %s", dir));
				changes.set(index, true);
				inform = false;
			}
			if (!key.reset()) {
				keys.remove(key);
				if (keys.isEmpty()) {
					break;
				}
			}
		}
	}
}