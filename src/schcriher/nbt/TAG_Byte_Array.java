package schcriher.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TAG_Byte_Array extends Tag {

    public byte[] data;

    public TAG_Byte_Array(String name) {
        super(name);
    }

    public TAG_Byte_Array(String name, byte[] data) {
        super(name);
        this.data = data;
    }

	@Override
	void load(DataInput in) throws TagException, IOException {
		int length = in.readInt();
        data = new byte[length];
        in.readFully(data);
	}

	@Override
	void write(DataOutput out) throws IOException {
		out.writeInt(data.length);
		out.write(data);
	}

	@Override
	public Object getData() {
		return data;
	}

	@Override
	public Tag copy() {
        byte[] copy = new byte[data.length];
        System.arraycopy(data, 0, copy, 0, data.length);
        return new TAG_Byte_Array(getName(), copy);
	}

    @Override
    public boolean equals(Object other) {
        if (super.equals(other)) {
        	TAG_Byte_Array o = (TAG_Byte_Array)other;
            return ((data == null && o.data == null) || (data != null && data.equals(o.data)));
        }
        return false;
    }

	@Override
	public String toString() {
		String name = getName();
		String append = name.length() > 0 ? "(\"" + name + "\")" : "";
		StringBuilder sb = new StringBuilder();
		for (byte b : data) {
			sb.append(String.format("%02X", b));
			sb.append(ITEMSEP);
		}
		return type.name() + append + ": " + sb.toString().trim();
	}
}