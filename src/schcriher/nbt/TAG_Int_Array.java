package schcriher.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TAG_Int_Array extends Tag {

    public int[] data;

    public TAG_Int_Array(String name) {
        super(name);
    }

    public TAG_Int_Array(String name, int[] data) {
        super(name);
        this.data = data;
    }

	@Override
	void load(DataInput in) throws TagException, IOException {
		int length = in.readInt();
        data = new int[length];
        for (int i = 0; i < length; i++) {
            data[i] = in.readInt();
        }
	}

	@Override
	void write(DataOutput out) throws IOException {
		out.writeInt(data.length);
        for (int element : data) {
        	out.writeInt(element);
        }
	}

	@Override
	public Object getData() {
		return data;
	}

	@Override
	public Tag copy() {
        int[] cp = new int[data.length];
        System.arraycopy(data, 0, cp, 0, data.length);
        return new TAG_Int_Array(getName(), cp);
	}

    @Override
    public boolean equals(Object other) {
        if (super.equals(other)) {
        	TAG_Int_Array o = (TAG_Int_Array)other;
            return ((data == null && o.data == null) || (data != null && data.equals(o.data)));
        }
        return false;
    }

	@Override
	public String toString() {
		String name = getName();
		String append = name.length() > 0 ? "(\"" + name + "\")" : "";
		StringBuilder sb = new StringBuilder();
		for (int b : data) {
			sb.append(b);
			sb.append(ITEMSEP);
		}
		return type.name() + append + ": " + sb.toString().trim();
	}
}