package schcriher.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TAG_Byte extends Tag {

	public byte data;

	public TAG_Byte(String name) {
        super(name);
    }

	public TAG_Byte(String name, byte data) {
        super(name);
        this.data = data;
    }

	@Override
	public void load(DataInput in) throws TagException, IOException {
		data = in.readByte();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeByte(data);
	}

	@Override
	public Object getData() {
		return data;
	}

	@Override
	public Tag copy() {
		return new TAG_Byte(getName(), data);
	}

    @Override
    public boolean equals(Object other) {
        if (super.equals(other)) {
        	TAG_Byte o = (TAG_Byte)other;
            return data == o.data;
        }
        return false;
    }
}