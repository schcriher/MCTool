package schcriher.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TAG_Double extends Tag {

	public double data;

	public TAG_Double(String name) {
		super(name);
	}

	public TAG_Double(String name, double data) {
		super(name);
		this.data = data;
	}

	@Override
	void load(DataInput in) throws TagException, IOException {
		data = in.readDouble();
	}

	@Override
	void write(DataOutput out) throws IOException {
		out.writeDouble(data);
	}

	@Override
	public Object getData() {
		return data;
	}

	@Override
	public Tag copy() {
		return new TAG_Double(getName(), data);
	}

	@Override
	public boolean equals(Object other) {
		if (super.equals(other)) {
			TAG_Double o = (TAG_Double)other;
			return data == o.data;
		}
		return false;
	}
}