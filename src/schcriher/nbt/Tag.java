package schcriher.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public abstract class Tag {

	public static Type DEFAULT_TYPE_FOR_TAG_LIST = Type.TAG_Byte;

	public static String LINESEP = System.getProperty("line.separator");
	public static String ITEMSEP = " ";
	public static String MARGIN = "   ";

	public final Type type = Type.valueOf(getClass().getSimpleName());
	private String name;

	public Tag(String name) {
        setName(name);
    }

	public final Tag setName(String name) {
		if (name == null) {
            this.name = "";
        } else {
            this.name = name;
        }
		return this; // Util en TAG_Compound -> void put(String, Tag)
	}

	public final String getName() {
		return name;
	}

	abstract void load(DataInput in) throws TagException, IOException;
	abstract void write(DataOutput out) throws IOException;
	abstract Object getData();
    abstract Tag copy();

    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof Tag)) {
            return false;
        }
        Tag o = (Tag)other;
        if (type.id() != o.type.id()) {
            return false;
        }
        if (!name.equals(o.getName())) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		String name = getName();
		String append = name.length() > 0 ? "(\"" + name + "\")" : "";
		return type.name() + append + ": " + getData();
	}

    public static Tag readTag(DataInput in) throws TagException, IOException {
        Type type = Type.id(in.readByte());
        if (type == Type.TAG_End) {
        	return new TAG_End();
        }
        String name = in.readUTF();
        Tag tag = newTag(type, name);
        tag.load(in);
        return tag;
    }

    public static void writeTag(Tag tag, DataOutput out) throws IOException {
        out.writeByte(tag.type.id());
        if (tag.type != Type.TAG_End) {
        	out.writeUTF(tag.getName());
            tag.write(out);
        }
    }

    public static Tag newTag(Type type, String name) throws TagException {
        switch (type) {
        case TAG_End:
            return new TAG_End();
        case TAG_Byte:
            return new TAG_Byte(name);
        case TAG_Short:
            return new TAG_Short(name);
        case TAG_Int:
            return new TAG_Int(name);
        case TAG_Long:
            return new TAG_Long(name);
        case TAG_Float:
            return new TAG_Float(name);
        case TAG_Double:
            return new TAG_Double(name);
        case TAG_Byte_Array:
            return new TAG_Byte_Array(name);
        case TAG_Int_Array:
            return new TAG_Int_Array(name);
        case TAG_String:
            return new TAG_String(name);
        case TAG_List:
            return new TAG_List<Tag>(name);
        case TAG_Compound:
            return new TAG_Compound(name);
        default:
        	throw new TagException("Invalid tag type " + type);
        }
    }
}