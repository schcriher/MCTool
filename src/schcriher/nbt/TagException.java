package schcriher.nbt;

public class TagException extends Exception {

	private static final long serialVersionUID = 1L;

	public TagException(String message) {
		super(message);
	}

	public TagException(Throwable cause) {
		super(cause);
	}

	public TagException(String message, Throwable cause) {
		super(message, cause);
	}
}