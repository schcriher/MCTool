package schcriher.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TAG_End extends Tag {

	public TAG_End() {
        super(null);
    }

    @Override
	void load(DataInput in) throws TagException, IOException {
    }

    @Override
	void write(DataOutput out) throws IOException {
    }

    @Override
    public String toString() {
        return type.name();
    }

    @Override
    public Tag copy() {
        return new TAG_End();
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other);
    }

	@Override
	public Object getData() {
		return null;
	}
}