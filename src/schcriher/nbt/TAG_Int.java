package schcriher.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TAG_Int extends Tag {

	public int data;

	public TAG_Int(String name) {
		super(name);
	}

	public TAG_Int(String name, int data) {
		super(name);
		this.data = data;
	}

	@Override
	void load(DataInput in) throws TagException, IOException {
		data = in.readInt();
	}

	@Override
	void write(DataOutput out) throws IOException {
		out.writeInt(data);
	}

	@Override
	public Object getData() {
		return data;
	}

	@Override
	public Tag copy() {
		return new TAG_Int(getName(), data);
	}

	@Override
	public boolean equals(Object other) {
		if (super.equals(other)) {
			TAG_Int o = (TAG_Int)other;
			return data == o.data;
		}
		return false;
	}
}