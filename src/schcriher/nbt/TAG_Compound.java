package schcriher.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TAG_Compound extends Tag {

	private final Map<String, Tag> tags = new HashMap<String, Tag>();

	public TAG_Compound() {
		super("");
	}

	public TAG_Compound(String name) {
		super(name);
	}

	@Override
	void load(DataInput in) throws TagException, IOException {
		tags.clear();
		Tag tag;
		while ((tag = Tag.readTag(in)).type != Type.TAG_End) {
			tags.put(tag.getName(), tag);
		}
	}

	@Override
	void write(DataOutput out) throws IOException {
		for (Tag tag : tags.values()) {
			Tag.writeTag(tag, out);
		}
		out.writeByte(Type.TAG_End.id());
	}

	@Override
	public Object getData() {
		return tags;
	}

	public Map<String, Tag> getMap() {
		return tags;
	}

	@Override
	public Tag copy() {
		TAG_Compound copy = new TAG_Compound(getName());
		for (String key : tags.keySet()) {
			copy.put(key, tags.get(key).copy());
		}
		return copy;
	}

	@Override
	public boolean equals(Object other) {
		if (super.equals(other)) {
			TAG_Compound o = (TAG_Compound) other;
			return tags.entrySet().equals(o.tags.entrySet());
		}
		return false;
	}

	@Override
	public String toString() {
		String name = getName();
		String append = name.length() > 0 ? "(\"" + name + "\")" : "";
		StringBuilder sb = new StringBuilder();
		int n = tags.size();
		String m = (n == 1) ? " entry" : " entries";
		sb.append(type.name() + append + ": " + n + m + LINESEP + "{" + LINESEP);
		for (Map.Entry<String, Tag> entry : tags.entrySet()) {
			sb.append(MARGIN);
			sb.append(entry.getValue().toString().replaceAll(LINESEP, LINESEP + MARGIN));
			sb.append(LINESEP);
		}
		sb.append("}");
		return sb.toString();
	}

	public boolean isEmpty() {
		return tags.isEmpty();
	}

	public void clear() {
		tags.clear();
	}

	public boolean contains(String name) {
		return tags.containsKey(name);
	}

	/*------------------------------------------------------------*/

	public void put(String name, Tag tag) {
		tags.put(name, tag.setName(name));
	}

	public void putByte(String name, byte value) {
		tags.put(name, new TAG_Byte(name, value));
	}

	public void putShort(String name, short value) {
		tags.put(name, new TAG_Short(name, value));
	}

	public void putInt(String name, int value) {
		tags.put(name, new TAG_Int(name, value));
	}

	public void putLong(String name, long value) {
		tags.put(name, new TAG_Long(name, value));
	}

	public void putFloat(String name, float value) {
		tags.put(name, new TAG_Float(name, value));
	}

	public void putDouble(String name, double value) {
		tags.put(name, new TAG_Double(name, value));
	}

	public void putString(String name, String value) {
		tags.put(name, new TAG_String(name, value));
	}

	public void putByteArray(String name, byte[] value) {
		tags.put(name, new TAG_Byte_Array(name, value));
	}

	public void putIntArray(String name, int[] value) {
		tags.put(name, new TAG_Int_Array(name, value));
	}

	public void putCompound(String name, TAG_Compound value) {
		tags.put(name, value.setName(name));
	}

    public void putBoolean(String name, boolean value) {
        putByte(name, value ? (byte) 1 : 0);
    }

	public Tag get(String name) {
		return tags.get(name);
	}

	/*------------------------------------------------------------*/

	public byte getByte(String name) throws TagException {
		if (tags.containsKey(name)) {
			return ((TAG_Byte)tags.get(name)).data;
		} else {
			throw new TagException("Tag nonexistent " + name);
		}
	}

    public boolean getBoolean(String name) throws TagException {
    	try {
    		return getByte(name) != 0;
    	} catch (TagException e) {
    		throw e;
		}
    }

	public short getShort(String name) throws TagException {
		if (tags.containsKey(name)) {
			return ((TAG_Short)tags.get(name)).data;
		} else {
			throw new TagException("Tag nonexistent " + name);
		}
	}

	public int getInt(String name) throws TagException {
		if (tags.containsKey(name)) {
			return ((TAG_Int)tags.get(name)).data;
		} else {
			throw new TagException("Tag nonexistent " + name);
		}
	}

	public long getLong(String name) throws TagException {
		if (tags.containsKey(name)) {
			return ((TAG_Long)tags.get(name)).data;
		} else {
			throw new TagException("Tag nonexistent " + name);
		}
	}

	public float getFloat(String name) throws TagException {
		if (tags.containsKey(name)) {
			return ((TAG_Float)tags.get(name)).data;
		} else {
			throw new TagException("Tag nonexistent " + name);
		}
	}

	public double getDouble(String name) throws TagException {
		if (tags.containsKey(name)) {
			return ((TAG_Double)tags.get(name)).data;
		} else {
			throw new TagException("Tag nonexistent " + name);
		}
	}

	public String getString(String name) throws TagException {
		if (tags.containsKey(name)) {
			return ((TAG_String)tags.get(name)).data;
		} else {
			throw new TagException("Tag nonexistent " + name);
		}
	}

	public byte[] getByteArray(String name) throws TagException {
		if (tags.containsKey(name)) {
			return ((TAG_Byte_Array)tags.get(name)).data;
		} else {
			throw new TagException("Tag nonexistent " + name);
		}
	}

	public int[] getIntArray(String name) throws TagException {
		if (tags.containsKey(name)) {
			return ((TAG_Int_Array)tags.get(name)).data;
		} else {
			throw new TagException("Tag nonexistent " + name);
		}
	}

	public TAG_Compound getCompound(String name) {
		return (TAG_Compound)tags.get(name);
	}

	public TAG_List<? extends Tag> getList(String name) {
		return (TAG_List<?>)tags.get(name);
	}

	/*------------------------------------------------------------*/

	public Tag get(String name, Tag defaultValue) {
		if (tags.containsKey(name)) {
			return tags.get(name);
		} else {
			return defaultValue;
		}
	}

	public byte getByte(String name, byte defaultValue) {
		if (tags.containsKey(name)) {
			return ((TAG_Byte)tags.get(name)).data;
		} else {
			return defaultValue;
		}
	}

    public boolean getBoolean(String name, boolean defaultValue) {
    	try {
    		return getByte(name) != 0;
    	} catch (TagException e) {
    		return defaultValue;
		}
    }

	public short getShort(String name, short defaultValue) {
		if (tags.containsKey(name)) {
			return ((TAG_Short)tags.get(name)).data;
		} else {
			return defaultValue;
		}
	}

	public int getInt(String name, int defaultValue) {
		if (tags.containsKey(name)) {
			return ((TAG_Int)tags.get(name)).data;
		} else {
			return defaultValue;
		}
	}

	public long getLong(String name, long defaultValue) {
		if (tags.containsKey(name)) {
			return ((TAG_Long)tags.get(name)).data;
		} else {
			return defaultValue;
		}
	}

	public float getFloat(String name, float defaultValue) {
		if (tags.containsKey(name)) {
			return ((TAG_Float)tags.get(name)).data;
		} else {
			return defaultValue;
		}
	}

	public double getDouble(String name, double defaultValue) {
		if (tags.containsKey(name)) {
			return ((TAG_Double)tags.get(name)).data;
		} else {
			return defaultValue;
		}
	}

	public String getString(String name, String defaultValue) {
		if (tags.containsKey(name)) {
			return ((TAG_String)tags.get(name)).data;
		} else {
			return defaultValue;
		}
	}

	public byte[] getByteArray(String name, byte[] defaultValue) {
		if (tags.containsKey(name)) {
			return ((TAG_Byte_Array)tags.get(name)).data;
		} else {
			return defaultValue;
		}
	}

	public int[] getIntArray(String name, int[] defaultValue) {
		if (tags.containsKey(name)) {
			return ((TAG_Int_Array)tags.get(name)).data;
		} else {
			return defaultValue;
		}
	}

	public TAG_Compound getCompound(String name, TAG_Compound defaultValue) {
		if (tags.containsKey(name)) {
			return (TAG_Compound)tags.get(name);
		} else {
			return defaultValue;
		}
	}

	public TAG_List<? extends Tag> getList(String name, TAG_List<? extends Tag> defaultValue) {
		if (tags.containsKey(name)) {
			return (TAG_List<?>)tags.get(name);
		} else {
			return defaultValue;
		}
	}
}