package schcriher.nbt;

import java.util.HashMap;
import java.util.Map;

public enum Type {
	TAG_End,		//  0 - Used to mark the end of compound tags. This tag does not have a name.
	TAG_Byte,		//  1 - 1 byte / 8 bits, signed, (-128 to 127)
	TAG_Short,		//  2 - 2 bytes/16 bits, signed, big endian, (-32,768 to 32,767)
	TAG_Int,		//  3 - 4 bytes/32 bits, signed, big endian, (-2,147,483,648 to 2,147,483,647)
	TAG_Long,		//  4 - 8 bytes/64 bits, signed, big endian, (-9,223,372,036,854,775,808 to 9,223,372,036,854,775,807)
	TAG_Float,		//  5 - 4 bytes/32 bits, signed, big endian, IEEE 754-2008, binary32, (Single-precision floating-point format)
	TAG_Double,		//  6 - 8 bytes/64 bits, signed, big endian, IEEE 754-2008, binary64, (Double-precision floating-point format)
	TAG_Byte_Array,	//  7 - [TAG_Int length] An array of bytes of unspecified format. The length of this array is <length> bytes
	TAG_String,		//  8 - [TAG_Short length] An array of bytes defining a string in UTF-8 format.
	                //      The length of this array is <length> bytes
	TAG_List,		//  9 - [TAG_Byte tagId, TAG_Int length] A sequential list of Tags (not Named Tags), of type <typeId>.
	                //      The length of this array is <length> Tags (All tags share the same type.)
	TAG_Compound,	// 10 - A sequential list of Named Tags. This array keeps going until a TAG_End is found. [TAG_End end]
	                //      (No two tags may have the same name, depth < 512)
	TAG_Int_Array;	// 11 - [TAG_Int length] An array of int. The length of this array is <length> int


	private static Map<Byte, Type> map = new HashMap<Byte, Type>();

	static {
		for (Type t : values()) {
			map.put((byte)t.ordinal(), t);
		}
    }

	public byte id() {
		return (byte)ordinal();
	}

	public static Type id(int id) throws TagException {
		return id((byte)id);
	}

	public static Type id(byte id) throws TagException {
		if (map.containsKey(id)) {
			return map.get(id);
		} else {
			throw new TagException("Invalid tag type, id=" + id);
		}
	}
}