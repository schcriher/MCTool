package schcriher.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TAG_String extends Tag {

    public String data;

    public TAG_String(String name) {
        super(name);
    }

    public TAG_String(String name, String data) {
        super(name);
        this.data = (data == null) ? "" : data;
    }

	@Override
	void load(DataInput in) throws TagException, IOException {
		data = in.readUTF();
	}

	@Override
	void write(DataOutput out) throws IOException {
		out.writeUTF(data);
	}

	@Override
	public Object getData() {
		return data;
	}

	@Override
	public Tag copy() {
		return new TAG_String(getName(), data);
	}

    @Override
    public boolean equals(Object other) {
        if (super.equals(other)) {
        	TAG_String o = (TAG_String)other;
            return ((data == null && o.data == null) || (data != null && data.equals(o.data)));
        }
        return false;
    }
}