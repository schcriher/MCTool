package schcriher.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TAG_Float extends Tag {

    public float data;

    public TAG_Float(String name) {
        super(name);
    }

    public TAG_Float(String name, float data) {
        super(name);
        this.data = data;
    }

	@Override
	void load(DataInput in) throws TagException, IOException {
		data = in.readFloat();
	}

	@Override
	void write(DataOutput out) throws IOException {
		out.writeFloat(data);
	}

	@Override
	public Object getData() {
		return data;
	}

	@Override
	public Tag copy() {
		return new TAG_Float(getName(), data);
	}

    @Override
    public boolean equals(Object other) {
        if (super.equals(other)) {
        	TAG_Float o = (TAG_Float)other;
            return data == o.data;
        }
        return false;
    }
}