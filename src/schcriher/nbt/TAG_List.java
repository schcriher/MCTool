package schcriher.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TAG_List<T extends Tag> extends Tag {

    private final List<T> list = new ArrayList<T>();
    private Type contentType;

    public TAG_List(String name) {
        super(name);
    }

    @Override
	@SuppressWarnings("unchecked")
    void load(DataInput in) throws TagException, IOException {
    	list.clear();
        contentType = Type.id(in.readByte());
        int size = in.readInt();
        for (int i = 0; i < size; i++) {
            Tag tag = Tag.newTag(contentType, null);
            tag.load(in);
			list.add((T)tag);
        }
    }

    @Override
	void write(DataOutput out) throws IOException {
    	if (contentType == null || contentType == Type.TAG_End) {
    		contentType = DEFAULT_TYPE_FOR_TAG_LIST;
    		list.clear();
    	}
        out.writeByte(contentType.id());
        out.writeInt(list.size());
        for (T tag : list) {
        	tag.write(out);
        }
    }

	@Override
	public String toString() {
		String name = getName();
		String append = name.length() > 0 ? "(\"" + name + "\")" : "";
		StringBuilder sb = new StringBuilder();
		int n = list.size();
		String m = (n == 1) ? " entry of type " : " entries of type ";
		sb.append(type.name() + append + ": " + n + m + contentType.name());
		sb.append(LINESEP + "{" + LINESEP);
		for (Tag t : list) {
			sb.append(MARGIN);
			sb.append(t.toString().replaceAll(LINESEP, LINESEP + MARGIN));
			sb.append(LINESEP);
		}
		sb.append("}");
		return sb.toString();
	}

	@Override
	@SuppressWarnings("unchecked")
    public Tag copy() {
        TAG_List<T> copy = new TAG_List<T>(getName());
        copy.contentType = contentType;
        for (T t : list) {
            copy.list.add((T)t.copy());
        }
        return copy;
    }

    @Override
    public boolean equals(Object other) {
        if (super.equals(other)) {
            TAG_List<?> o = (TAG_List<?>)other;
            if (contentType == o.contentType) {
                return list.equals(o.list);
            }
        }
        return false;
    }

	@Override
	public Object getData() {
		return list;
	}

	private void validateAndSetContentType(T tag) throws TagException {
    	if (contentType == null) {
    		contentType = tag.type;
    	}
    	else if (tag == null || tag.type != contentType) {
    		throw new TagException("Invalid tag type " + tag.type + " for this " + type.name() + ", " + contentType + " expected");
    	}
	}

	public void add(int index, T tag) throws TagException {
		validateAndSetContentType(tag);
    	list.add(index, tag);
	}

    public void add(T tag) throws TagException {
    	validateAndSetContentType(tag);
    	list.add(tag);
    }

    public T get(int index) {
        return list.get(index);
    }

    public boolean isEmpty() {
    	return list.isEmpty();
    }

    public int indexOf(T tag) {
    	return list.indexOf(tag);
    }

    public int lastIndexOf(T tag) {
    	return list.lastIndexOf(tag);
    }

    public void clear() {
    	list.clear();
    	contentType = null;
    }

    public T remove(int index) {
    	return list.remove(index);
    }

    public boolean remove(T tag) {
    	return list.remove(tag);
    }

    public T set(int index, T tag) {
    	return list.set(index, tag);
    }

    public int size() {
        return list.size();
    }
}