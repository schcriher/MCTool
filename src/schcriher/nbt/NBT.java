package schcriher.nbt;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class NBT {
	/*
	 * Named Binary Tag
	 *
	 * An NBT file is a GZip'd Compound tag, name and tag ID included. Some of the files
	 * utilized by Minecraft may be uncompressed, but in most cases the files follow
	 * Notch's original specification and are compressed with GZip
	 *
	 * Minecraft's use of the NBT format is odd at times. In some instances, empty lists
	 * may be represented as a list of Byte tags rather than a list of the correct type,
	 * or as a list of End tags in newer versions of Minecraft, which can break some older
	 * NBT tools. Additionally, almost every root tag has an empty name string and
	 * encapsulates only one Compound tag with the actual data and a name. For instance:
	 *
	 * level.dat		is stored in compressed NBT format
	 * <player>.dat		is stored in compressed NBT format
	 * idcounts.dat		is stored in uncompressed NBT format
	 * villages.dat		is stored in compressed NBT format
	 * map_<#>.dat		is stored in compressed NBT format
	 * servers.dat		which is used to store the list of saved multiplayer servers as uncompressed NBT
	 * Chunks			is stored in compressed NBT format within Region files
	 * scoreboard.dat	is stored in compressed NBT format
	 *
	 * http://web.archive.org/web/20110723210920/http://www.minecraft.net/docs/NBT.txt
	 * http://minecraft.gamepedia.com/NBT_format
	 * http://wiki.vg/NBT
	 * http://wiki.vg/Protocol
	 *
	 * BASED ON: http://assets.minecraft.net/12w07a/Minecraft.AnvilConverter.zip
	 *
	 */

	public boolean compressed;
	public Tag rootTag;

	public NBT(){
		compressed = true;
	}

	public NBT(boolean compressed){
		this.compressed = compressed;
	}

	public TAG_Compound getRootTagCompound() throws TagException {
		if (rootTag != null && rootTag.type == Type.TAG_Compound) {
			return (TAG_Compound)rootTag;
		}
		throw new TagException("rootTag is not TAG_Compound");
	}

	public Map<String, Tag> getRootTagMap() throws TagException {
		return getRootTagCompound().getMap();
	}

	public void load(String file) throws IOException, TagException {
		load(new File(file));
	}

	public void load(File file) throws IOException, TagException {
		try (FileInputStream fis = new FileInputStream(file);
			BufferedInputStream bis = new BufferedInputStream(fis)){
			load(bis);
		}
		catch (IOException e) {
			throw e;
		}
	}

	public void load(InputStream in) throws IOException, TagException {
		if (compressed) {
			try (GZIPInputStream gis = new GZIPInputStream(in);
				DataInputStream dis = new DataInputStream(gis)){
				rootTag = Tag.readTag(dis);
			}
			catch (IOException e) {
				throw e;
			}
		} else {
			try (DataInputStream dis = new DataInputStream(in)){
				rootTag = Tag.readTag(dis);
			}
			catch (IOException e) {
				throw e;
			}
		}
	}

	public void write(String file) throws IOException, TagException {
		write(new File(file));
	}

	public void write(File file) throws IOException, TagException {
		try (FileOutputStream fos = new FileOutputStream(file);
			BufferedOutputStream bos = new BufferedOutputStream(fos)){
			write(bos);
		}
		catch (IOException e) {
			throw e;
		}
	}

	public void write(OutputStream out) throws IOException, TagException {
		if (compressed) {
			try (GZIPOutputStream gos = new GZIPOutputStream(out);
				DataOutputStream dos = new DataOutputStream(gos)){
				Tag.writeTag(rootTag, dos);
			}
			catch (IOException e) {
				throw e;
			}
		} else {
			try (DataOutputStream dos = new DataOutputStream(out)){
				Tag.writeTag(rootTag, dos);
			}
			catch (IOException e) {
				throw e;
			}
		}
	}

	public void reset() {
		compressed = true;
		rootTag = null;
	}
}