package schcriher.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TAG_Short extends Tag {

	public short data;

	public TAG_Short(String name) {
		super(name);
	}

	public TAG_Short(String name, short data) {
		super(name);
		this.data = data;
	}

	@Override
	void load(DataInput in) throws TagException, IOException {
		data = in.readShort();
	}

	@Override
	void write(DataOutput out) throws IOException {
		out.writeShort(data);
	}

	@Override
	public Object getData() {
		return data;
	}

	@Override
	public Tag copy() {
		return new TAG_Short(getName(), data);
	}

	@Override
	public boolean equals(Object other) {
		if (super.equals(other)) {
			TAG_Short o = (TAG_Short)other;
			return data == o.data;
		}
		return false;
	}
}