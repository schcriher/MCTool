package schcriher.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TAG_Long extends Tag {

	public long data;

	public TAG_Long(String name) {
		super(name);
	}

	public TAG_Long(String name, long data) {
		super(name);
		this.data = data;
	}

	@Override
	void load(DataInput in) throws TagException, IOException {
		data = in.readLong();
	}

	@Override
	void write(DataOutput out) throws IOException {
		out.writeLong(data);
	}

	@Override
	public Object getData() {
		return data;
	}

	@Override
	public Tag copy() {
		return new TAG_Long(getName(), data);
	}

	@Override
	public boolean equals(Object other) {
		if (super.equals(other)) {
			TAG_Long o = (TAG_Long)other;
			return data == o.data;
		}
		return false;
	}
}