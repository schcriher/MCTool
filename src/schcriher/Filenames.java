package schcriher;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import schcriher.utils.Archives;

public class Filenames extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private static final String title = "Nombres existentes";
	private static final boolean modal = true;

	private final JPanel contentPanel = new JPanel();
	private final JPanel buttonPanel = new JPanel();

	private final JButton buttonAccept = new JButton("Aceptar");
    private final JButton buttonCancel = new JButton("Cancelar");

	private final Map<Integer, JCheckBox> checks = new HashMap<Integer, JCheckBox>();
	private final Map<Integer, JTextField> names = new HashMap<Integer, JTextField>();
	private final Map<Integer, String> extwdot = new HashMap<Integer, String>();
	private final Map<Integer, Boolean> valid = new HashMap<Integer, Boolean>();
	private final Map<Integer, File> files;

	private boolean canceled;

	public Filenames(Frame mcparent, final Map<Integer, File> files) {
		super(mcparent, title, modal);
		this.files = files;
		initComponents();
		pack();
		setLocationRelativeTo(mcparent);
	}

	public void initComponents() {
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);

		GridBagLayout layout = new GridBagLayout();
		layout.columnWeights = new double[]{0.0, 1.0, 1.0};
		contentPanel.setLayout(layout);

		JCheckBox check;
		JLabel path;
		JTextField name;

		GridBagConstraints c = new GridBagConstraints();
		c.gridy = 0;
		c.insets = new Insets(0, 0, 0, 0);

		class TextFieldDocumentListener implements DocumentListener {
			private final int index;
			TextFieldDocumentListener(int index){
				this.index = index;
			}
			@Override public void changedUpdate(DocumentEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						validate(index);
					}
				});
			}
			@Override public void insertUpdate(DocumentEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						validate(index);
					}
				});
			}
			@Override public void removeUpdate(DocumentEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						validate(index);
					}
				});
			}
		}

		for (Map.Entry<Integer, File> rename: files.entrySet()) {
			int index = rename.getKey();
			File file = rename.getValue();
			String sindex = String.format("%d", index);

			c.gridwidth = 1;

			check = new JCheckBox();
			check.setName(sindex);
			check.setSelected(true);
			check.addActionListener(this);
			c.gridx = 0;
			c.weightx = 0.0;
			c.insets.right = 5;
			c.fill = GridBagConstraints.NONE;
			contentPanel.add(check, c);

			c.weightx = 1.0;
			c.fill = GridBagConstraints.BOTH;

			path = new JLabel();
			path.setText(file.getParentFile().getPath() + File.separator);
			path.setHorizontalAlignment(SwingConstants.RIGHT);
			c.gridx = 1;
			c.insets.right = 2;
			contentPanel.add(path, c);

			name = new JTextField();
			name.setText(file.getName());
			name.setToolTipText(file.getName());
			name.getDocument().addDocumentListener(new TextFieldDocumentListener(index));
			name.setForeground(Color.RED);

			c.gridx = 2;
			c.insets.right = 0;
			contentPanel.add(name, c);

			checks.put(index, check);
			names.put(index, name);
			valid.put(index, false);
			extwdot.put(index, "." + Archives.getExtension(file));

			c.gridy += 1;
		}

		contentPanel.validate();
		JScrollPane scroll = new JScrollPane(contentPanel);
		Dimension d = contentPanel.getPreferredSize();
		if (d.width > Resources.WIDTH) {
			d.width = Resources.WIDTH;
		}
		else {
			for (JTextField text: names.values()) {
				text.setColumns(26);
			}
			contentPanel.revalidate();
			d = contentPanel.getPreferredSize();
		}
		if (d.height > Resources.HEIGHT) {
			d.height = Resources.HEIGHT;
		}
        scroll.getViewport().setPreferredSize(d);
        scroll.getVerticalScrollBar().setUnitIncrement(contentPanel.getPreferredSize().height / files.size());
        scroll.setBorder(new EmptyBorder(6, 6, 6, 6));

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(scroll, BorderLayout.CENTER);

		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		buttonPanel.add(Box.createHorizontalGlue());
		JPanel buttonSubPanel = new JPanel(new GridLayout(1, 2, 3, 0));

		buttonCancel.addActionListener(this);
		buttonAccept.addActionListener(this);
		buttonSubPanel.add(buttonCancel);
		buttonSubPanel.add(buttonAccept);

		buttonPanel.add(buttonSubPanel);
		buttonPanel.setBorder(new EmptyBorder(0, 3, 3, 3));
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);

		buttonAccept.setEnabled(false);
	}

	private boolean run() {
		setVisible(true);
		if (canceled) {
			return false;
		} else {
			Iterator<Map.Entry<Integer, File>> entries = files.entrySet().iterator();
			Map.Entry<Integer, File> entry;
			while (entries.hasNext()) {
				entry = entries.next();
				int index = entry.getKey();
				File file = entry.getValue();

				if (checks.get(index).isSelected()) {
					String newName = names.get(index).getText();
					if (!file.getName().equals(newName)) {
						files.put(index, new File(file.getParentFile(), newName));
					}
				} else {
					files.put(index, null);
				}
			}
			return true;
		}
	}

	synchronized private void validate(int index) {
		boolean isValid = true;
		if (checks.get(index).isSelected()) {
			String name = names.get(index).getText();
			String ext = extwdot.get(index);
			if (ext.length() > 1) {
				if (!name.toLowerCase().endsWith(ext.toLowerCase())) {
					name = name + ext;
					names.get(index).setText(name);
				}
			}
			isValid = !exists(name, index);
		}
		valid.put(index, isValid);
		names.get(index).setForeground(isValid ? null : Color.RED);
		enabledButtonAccept();
	}

	private boolean exists(String name, int index) {
		for (int i: files.keySet()) {
			if (i != index && valid.get(i) && name.equals(names.get(i).getText())) {
				return true;
			}
		}
		if (name.equals("") || new File(files.get(index).getParentFile(), name).exists()) {
			return true;
		}
		return false;
	}

	private void enabledButtonAccept() {
		boolean enabled = true;
		for (boolean b: valid.values()) {
			if (!b) {
				enabled = false;
				break;
			}
		}
		buttonAccept.setEnabled(enabled);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source instanceof JCheckBox) {
			JCheckBox check = (JCheckBox) source;
			int index = Integer.parseInt(check.getName());
			names.get(index).setEnabled(check.isSelected());
			validate(index);
		}
		else if (source == buttonAccept) {
			canceled = false;
			dispose();
		}
		else if (source == buttonCancel) {
			canceled = true;
			dispose();
		}
	}

	static public boolean checkExistenceAndAskNewnames(Frame mcparent, final List<File> files) {
		final Map<Integer, File> mapFiles = new HashMap<Integer, File>();
		int size = files.size();
		boolean repeated;
		for (int i = 0; i < size; i++) {
			File f = files.get(i);
			repeated = false;
			for (int j = 0; j < i ; j++) {
				if (files.get(j).equals(f)) {
					repeated = true;
					break;
				}
			}
			if (repeated || f.exists()) {
				mapFiles.put(i, f);
			}
		}
		if (!mapFiles.isEmpty()) {
			Filenames dialog = new Filenames(mcparent, mapFiles);
			if (dialog.run()) {
				for (Map.Entry<Integer, File> e: mapFiles.entrySet()) {
					files.set(e.getKey(), e.getValue());
				}
			} else {
				return false;
			}
		}
		return true;
	}
}