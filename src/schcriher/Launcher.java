package schcriher;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.File;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import schcriher.utils.Utils;

public class Launcher implements Runnable {

	private static final String CNAME = Launcher.class.getName();
	private static final Logger LOGGER = Logger.getLogger(CNAME);
    private final JFrame frame;

    public Launcher() {
    	long start = System.currentTimeMillis();

    	//TOFIX "Comparison method violates its general contract"
    	System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");

    	try {
    		new File(System.getProperty("user.home") + "/.cache/schcriher/").mkdirs();
			LogManager.getLogManager().readConfiguration(Resources.getResourceAsStream("schcriher/logging.properties"));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
    	LOGGER.fine("Schcriher Launcher Starting...");

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            LOGGER.throwing(CNAME, "UIManager.setLookAndFeel", e);
        }

        frame = new JFrame();
        JPanel panel;
        try {
        	panel = new Schcriher(frame, Utils.getWorkingDirectory());
        }
        catch (Exception e) {
        	LOGGER.throwing(CNAME, "INIT Schcriher", e);
        	panel = new JPanel(new BorderLayout());
    		JLabel label = new JLabel("<html><body><center><b>ERROR FATAL INICIANDO LA APLICACION<br/>"+e+"</b></center></body></html>");
            label.setHorizontalAlignment(SwingConstants.CENTER);
            label.setVerticalAlignment(SwingConstants.CENTER);
            panel.add(label, BorderLayout.CENTER);
        }
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Tool for Minecraft - Schcriher");
        frame.setContentPane(panel);
        if (Resources.FAVICON != null) {
        	frame.setIconImage(((ImageIcon)Resources.FAVICON).getImage());
        }
        frame.pack();
        frame.setLocationRelativeTo(null);

        LOGGER.fine(String.format("Schcriher Launcher Initialized in %dms", System.currentTimeMillis() - start));
    }

    @Override
    public void run() {
    	frame.setVisible(true);
    }

    public static void main(String[] args) {
    	Launcher launcher = new Launcher();
    	EventQueue.invokeLater(launcher);
    }
}