package schcriher;

import static schcriher.waitdialogs.Publish.Type.COMPLETE;
import static schcriher.waitdialogs.Publish.Type.MAJOR;
import static schcriher.waitdialogs.Publish.Type.MAJORSTEP;
import static schcriher.waitdialogs.Publish.Type.SETSUBTEXT;
import static schcriher.waitdialogs.Publish.Type.SETTEXT;

import java.awt.EventQueue;
import java.awt.Frame;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import schcriher.utils.Archives;
import schcriher.utils.Archives.Operation;
import schcriher.utils.Combinations;
import schcriher.waitdialogs.Publisher;

public class DuplicateRemover extends Publisher {

	private final static String title = "Eliminador de repetidos";
	private final Frame mcparent;
	private final List<File> files;
	private final List<File> folders;

	volatile private boolean removed = false;

	public DuplicateRemover(Frame mcparent, List<File> files) {
		this(mcparent, files, null);
	}

	public DuplicateRemover(Frame mcparent, List<File> files, List<File> folders) {
		super(mcparent, title);
		this.mcparent = mcparent;
		this.files = files != null ? files : new ArrayList<File>(0);
		this.folders = folders != null ? folders : new ArrayList<File>(0);
	}

	@Override
	protected Void doInBackground() throws Exception {
		publish(MAJOR, files.size() + folders.size() + 2, "Filtrando archivos");

		Map<Long, List<File>> lengthFiles = new HashMap<Long, List<File>>();
		Map<Long, List<File>> lengthFolders = new HashMap<Long, List<File>>();
		List<File> deletes = new ArrayList<File>();

		groupedBySize(files, lengthFiles, false);
		groupedBySize(folders, lengthFolders, true);

		publish(MAJORSTEP, "Comparando archivos");

		findDeletes(lengthFiles, deletes, false);
		findDeletes(lengthFolders, deletes, true);

		publish(SETSUBTEXT);
		publish(SETTEXT);

		if (deletes.isEmpty()) {
			publish(COMPLETE, "Finalizado");
			EventQueue.invokeLater(new Runnable() {
				@Override
				public void run() {
					LAD.showInformationMessage(mcparent, "No hay archivos repetidos");
				}
			});
		} else {
			removed = true;
			publish(SETTEXT, "Eliminando archivos...");
			List<String> messages = new ArrayList<String>();
			messages.add("Serán eliminados los siguientes archivos, ¿continuar?");
			messages.add("");
			for (Iterator<File> it = deletes.iterator(); it.hasNext();) {
				File f = it.next();
				if (f.exists()) {
					messages.add(f.getPath());
				} else {
					it.remove();
				}
			}
			if (LAD.showConfirmDialogYesCancel(mcparent, messages)) {
				List<String> nonDelete = new ArrayList<String>();
				for (File f: deletes) {
					if (!Archives.recursively(f, Operation.DELETE)) {
						nonDelete.add(f.getPath());
					}
				}
				if (!nonDelete.isEmpty()) {
					publish(SETTEXT, "Errores eliminando los archivos");
					nonDelete.add(0, "Los siguientes archivos no se pudieron eliminar:");
					nonDelete.add(1, "");
					LAD.showErrorMessage(mcparent, nonDelete);
				}
			}
		}
		publish(COMPLETE, "Finalizado");
		return null;
	}

	private void groupedBySize(List<File> list, Map<Long, List<File>> map, final boolean folders) {
		Collections.sort(list);
		long length;
		for (File f: list) {
			if (folders) {
				length = Archives.getFullSize(f);
			} else {
				length = f.length();
			}
			if (map.containsKey(length)) {
				map.get(length).add(f);
			} else {
				List<File> subList = new ArrayList<File>();
				subList.add(f);
				map.put(length, subList);
			}
		}
	}

	private void findDeletes(Map<Long, List<File>> map, List<File> deletes, final boolean folders) throws InterruptedException {
		boolean dA, dB, equal;
		File fA, fB;
		int n;
		for (List<File> list: map.values()) {
			publish(MAJORSTEP);
			n = list.size();
			if (n > 1) {
				n--;
				for (List<File> pair: new Combinations<File>(list, 2)) {
					fA = pair.get(0);
					fB = pair.get(1);
					publish(SETSUBTEXT, String.format("%s vs %s", fA.getName(), fB.getName()));
					dA = deletes.contains(fA);
					dB = deletes.contains(fB);
					if (!(dA && dB)) {
						if (folders) {
							equal = Archives.equalFolders(fA, fB);
						} else {
							equal = Archives.equalFiles(fA, fB);
						}
						if (equal) {
							if (dA || dB) {
								if (dA) deletes.add(fB);
								if (dB) deletes.add(fA);
							} else {
								deletes.add(fB);
							}
						}
					}
					if (n > 0) {
						publish(MAJORSTEP);
						n--;
					}
				}
			}
		}
	}

	synchronized public boolean removed() {
		return removed;
	}
}