package schcriher;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.Queue;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import schcriher.generic.AlphabeticalOrder;
import schcriher.utils.Archives.Operation;

public class CfgRemover extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private static final boolean modal = true;
	private static final String title = "Configuraciones";

	private final JPanel contentPanel = new JPanel();
	private final JButton closeButton = new JButton("Cerrar");
	private final Frame mcparent;

	private final Path fCfg;

	public CfgRemover(Frame mcparent, File fCfg) {
		super(mcparent, title, modal);
		this.mcparent = mcparent;
		this.fCfg = fCfg.toPath();
		initComponents();
		pack();
		setLocationRelativeTo(mcparent);
	}

	class Remover implements ActionListener {

		private final JButton button;
		private final JLabel label;
		private final String relPath;

		public Remover(JButton button, JLabel label, String relPath) {
			this.button = button;
			this.label = label;
			this.relPath = relPath;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			String question = String.format("¿Realmente desea eliminar %s?", relPath);
			if (LAD.showConfirmDialogYesCancel(mcparent, question)) {
				button.setEnabled(false);
				label.setEnabled(false);

				File file = new File(fCfg.toFile(), relPath);
				String tooltip;

				if (Operation.DELETE.make(file)) {
					tooltip = "Eliminado";
				} else {
					tooltip = "Con errores al intenatr eliminarlo";
					String message = String.format("No se pudo eliminar %s", relPath);
					LAD.showErrorMessage(mcparent, message);
				}

				button.setToolTipText(tooltip);
				label.setToolTipText(tooltip);
			}
		}
	}

	private void initComponents() {
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();

		layout.columnWeights = new double[]{0.0, 1.0};

		contentPanel.setLayout(layout);
		contentPanel.setBorder(new EmptyBorder(8, 8, 8, 8));

		c.gridy = 0;
		c.insets = new Insets(0, 0, 0, 0);

		for (String relPath: listCfg()) {

			JButton button = new JButton("Eliminar");
			c.gridx = 0;
			c.insets.left = 0;
			c.fill = GridBagConstraints.NONE;
			contentPanel.add(button, c);

			JLabel label = new JLabel(relPath);
			c.gridx = 1;
			c.insets.left = 10;
			c.fill = GridBagConstraints.HORIZONTAL;
			contentPanel.add(label, c);

			button.addActionListener(new Remover(button, label, relPath));

			c.gridy += 1;
		}

		c.gridx = 0;
		c.gridwidth = 2;
		c.insets.top = 8;
		c.insets.left = 4;
		c.insets.right = 4;
		closeButton.addActionListener(this);
		contentPanel.add(closeButton, c);

		contentPanel.validate();
		Dimension d = contentPanel.getPreferredSize();
		if (d.height > Resources.HEIGHT) {
			int h = d.height;
			d.height = Resources.HEIGHT;
			JScrollPane scroll = new JScrollPane(contentPanel);
			scroll.getViewport().setPreferredSize(d);
			scroll.setBorder(new EmptyBorder(4, 4, 4, 4));
			scroll.getVerticalScrollBar().setUnitIncrement(h / c.gridy);
			add(scroll);
		} else {
			add(contentPanel);
		}
	}

	private SortedSet<String> listCfg() {
		SortedSet<String> relPaths = new TreeSet<String>(new AlphabeticalOrder<String>());
		Queue<File> folders = new LinkedList<File>();

		folders.add(fCfg.toFile());
		while (!folders.isEmpty()) {
			for (File f: folders.poll().listFiles()) {
				if (f.isDirectory()) {
					folders.add(f);
				} else if (f.isFile()) {
					relPaths.add(fCfg.relativize(f.toPath()).toString());
				}
			}
		}
		return relPaths;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
    	if (source == closeButton) {
    		dispose();
    	}
	}
}