package schcriher.importer;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import net.sf.sevenzipjbinding.ExtractAskMode;
import net.sf.sevenzipjbinding.ExtractOperationResult;
import net.sf.sevenzipjbinding.IArchiveExtractCallback;
import net.sf.sevenzipjbinding.ISequentialOutStream;
import net.sf.sevenzipjbinding.SevenZipException;

public class MemoryExtractCallback implements IArchiveExtractCallback, ISequentialOutStream {

    private final byte[] buffer;
    private int pointer;

    public MemoryExtractCallback(byte[] buffer) {
        this.buffer = buffer;
        pointer = 0;
    }

    @Override
    public ISequentialOutStream getStream(int index, ExtractAskMode mode) throws SevenZipException {
    	if (mode != ExtractAskMode.EXTRACT) {
    		return null;
    	}
    	return this;
    }

	@Override
	public void setOperationResult(ExtractOperationResult result) throws SevenZipException {
		if (result != ExtractOperationResult.OK) {
			throw new SevenZipException("Extraction error, result " + result);
        }
	}

	@Override
	public void prepareOperation(ExtractAskMode mode) throws SevenZipException {}

	@Override
	public void setCompleted(long value) throws SevenZipException {}

	@Override
	public void setTotal(long total) throws SevenZipException {}

	@Override
	public int write(byte[] data) throws SevenZipException {
		try {
			int length = data.length;
			System.arraycopy(data, 0, buffer, pointer, length);
			pointer += length;
			return length;
		}
		catch (Exception e) {
			throw new SevenZipException(e);
		}
	}

	public void dump(File dst) throws IOException {
		FileOutputStream fos = new FileOutputStream(dst);
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		bos.write(buffer);
		bos.close();
		fos.close();
	}
}