package schcriher.importer;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import net.sf.sevenzipjbinding.ExtractAskMode;
import net.sf.sevenzipjbinding.ExtractOperationResult;
import net.sf.sevenzipjbinding.IArchiveExtractCallback;
import net.sf.sevenzipjbinding.ISequentialOutStream;
import net.sf.sevenzipjbinding.ISevenZipInArchive;
import net.sf.sevenzipjbinding.PropID;
import net.sf.sevenzipjbinding.SevenZipException;

public class FileExtractCallback implements IArchiveExtractCallback, ISequentialOutStream {

	private static final String CNAME = FileExtractCallback.class.getName();
	private static final Logger LOGGER = Logger.getLogger(CNAME);

	private final FileOutputStream     fos;
	private final BufferedOutputStream bos;
	private final ZipOutputStream      zos;
	private final ISevenZipInArchive   zia;

	private final boolean zip;
	private final Path base;
	private final File file;
	private final int nfz;

	private boolean newEntry;
	private boolean isFolder;
	private String strPath;
	private Date modified;

    public FileExtractCallback(ISevenZipInArchive zia, File file, Path base, boolean zip) throws FileNotFoundException, SevenZipException {
    	this.zia = zia;
    	this.zip = zip;
    	this.file = file;
    	this.base = base;
    	nfz = zia.getNumberOfItems();
    	fos = zip ? new FileOutputStream(file)    : null;
    	bos = zip ? new BufferedOutputStream(fos) : null;
    	zos = zip ? new ZipOutputStream(bos)      : null;
    }

    @Override
    public ISequentialOutStream getStream(int index, ExtractAskMode mode) throws SevenZipException {
    	if (mode != ExtractAskMode.EXTRACT) {
    		return null;
    	}
    	newEntry = true;
    	isFolder = (boolean) zia.getProperty(index, PropID.IS_FOLDER);
    	modified = (Date) zia.getProperty(index, PropID.LAST_WRITE_TIME);
    	strPath = base.relativize(Paths.get("/" + zia.getProperty(index, PropID.PATH))).toString() + (isFolder ? "/" : "");
    	return this;
    }

    @Override
    public void setOperationResult(ExtractOperationResult result) throws SevenZipException {
    	if (result != ExtractOperationResult.OK) {
    		throw new SevenZipException("Extraction error, result " + result);
    	}
    }

	@Override
	public void prepareOperation(ExtractAskMode mode) throws SevenZipException {}

	@Override
	public void setCompleted(long value) throws SevenZipException {}

	@Override
	public void setTotal(long total) throws SevenZipException {}

	@Override
	public int write(byte[] data) throws SevenZipException {
		if (zip) {
			try {
				if (newEntry) {
					ZipEntry entry = new ZipEntry(strPath);
					entry.setTime(modified.getTime());
					zos.putNextEntry(entry);
					newEntry = false;
				}
				if (!isFolder) {
					zos.write(data);
				}
			}
			catch (Exception e) {
				String error = "Extraction error (zip) " + strPath;
				LOGGER.throwing(CNAME, error, e);
				throw new SevenZipException(error, e);
			}
		} else {
			File fOut = (nfz == 1 && !file.isDirectory()) ? file : new File(file, strPath);
			if (isFolder) {
				fOut.mkdirs();
				if (!fOut.isDirectory()) {
					String error = "Could not create the folder " + fOut;
					LOGGER.severe(error);
					throw new SevenZipException(error);
				}
			} else {
				File parent = fOut.getParentFile();
				parent.mkdirs();
				if (!parent.isDirectory()) {
					String error = "Could not create the folder " + parent;
					LOGGER.severe(error);
					throw new SevenZipException(error);
				}
				try (
					FileOutputStream     fos = new FileOutputStream(fOut);
					BufferedOutputStream bos = new BufferedOutputStream(fos);
				){
					bos.write(data);
				}
				catch (Exception e) {
					String error = "Extraction error (non-zip) " + fOut;
					LOGGER.throwing(CNAME, error, e);
					throw new SevenZipException(error, e);
				}
			}
		}
		return data.length;
	}

	public void close() throws SevenZipException {
		if (zos != null) {try {zos.close();} catch (Exception e) {throw new SevenZipException(e);}}
		if (bos != null) {try {bos.close();} catch (Exception e) {throw new SevenZipException(e);}}
		if (fos != null) {try {fos.close();} catch (Exception e) {throw new SevenZipException(e);}}
	}
}