package schcriher.importer;

import net.sf.sevenzipjbinding.IInStream;
import net.sf.sevenzipjbinding.SevenZipException;
import schcriher.utils.Utils;

public class RandomAccessMemoryInStream implements IInStream {

	private final byte[] buffer;
	private int pointer;

	public RandomAccessMemoryInStream(byte[] buffer) {
		this.buffer = buffer;
		pointer = 0;
	}

	@Override
	public long seek(long offsetLong, int seekOrigin) throws SevenZipException {
		int offset = Utils.safeLongToInt(offsetLong);
		try {
			switch (seekOrigin) {
			case SEEK_SET:  // 0
				pointer = offset;
				break;
			case SEEK_CUR:  // 1
				pointer += offset;
				break;
			case SEEK_END:  // 2
				pointer = buffer.length + offset;
				break;
			default:
				throw new RuntimeException("Seek: unknown origin: " + seekOrigin);
			}
			return pointer;
		}
		catch (Exception e) {
			throw new SevenZipException("Error while seek operation", e);
		}
	}

	@Override
	public int read(byte[] data) throws SevenZipException {
		try {
			int length = Math.min(data.length, buffer.length - pointer);
			if (length > 0) {
				System.arraycopy(buffer, pointer, data, 0, length);
				pointer += length;
				return length;
			} else {
				return 0;
			}
		}
		catch (Exception e) {
			throw new SevenZipException("Error reading random access memory", e);
		}
	}
}