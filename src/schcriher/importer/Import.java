package schcriher.importer;

import static schcriher.importer.Importer.Type.AMBIGUOUS;
import static schcriher.importer.Importer.Type.WORLD;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.file.Path;
import java.util.logging.Logger;

import net.sf.sevenzipjbinding.ISevenZipInArchive;
import net.sf.sevenzipjbinding.SevenZip;
import net.sf.sevenzipjbinding.impl.RandomAccessFileInStream;
import schcriher.utils.Archives;
import schcriher.utils.Utils;

public class Import {

	private static final String CNAME = Import.class.getName();
	private static final Logger LOGGER = Logger.getLogger(CNAME);

	public final File file;
	public final Path base;
	public final String name;
	public final Importer.Type type;
	public final boolean isZip;
	public final boolean isTmp;
	public final int[] indexes;

	public Import(File file, Path base, String name, Importer.Type type, boolean isZip, boolean isTmp, int[] indexes){
		if (type == AMBIGUOUS) {
			throw new RuntimeException("Import: incorrect type: " + type);
		}
		this.file = file;
		this.base = base;          // quitar del path de zia esta base
		this.name = name;          // el destino es la carpeta segun type mas este nombre
		this.type = type;
		this.isZip = isZip;        // referido a file
		this.isTmp = isTmp;        // referido a file
		this.indexes = indexes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((file == null) ? 0 : file.hashCode());
		result = prime * result + ((base == null) ? 0 : base.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Import))
			return false;
		Import other = (Import) obj;
		if (base == null) {
			if (other.base != null)
				return false;
		}
		else if (!base.equals(other.base))
			return false;
		if (file == null) {
			if (other.file != null)
				return false;
		}
		else if (!file.equals(other.file))
			return false;
		return true;
	}

	public boolean dump(File dst) {
		boolean success;
		if (indexes == null && isZip && type != WORLD) {
			if (isTmp) {
				success = Archives.move(file, dst);
			} else {
				success = Archives.copy(file, dst);
			}
		} else {
			int[] ind;
			RandomAccessFile    raf = null;
			ISevenZipInArchive  zia = null;
			FileExtractCallback ecb = null;
			try {
				raf = new RandomAccessFile(file, "r");
				zia = SevenZip.openInArchive(null, new RandomAccessFileInStream(raf));
				ind = (indexes == null) ? Utils.range(zia.getNumberOfItems()) : indexes;
				ecb = new FileExtractCallback(zia, dst, base, type != WORLD);
				zia.extract(ind, false, ecb);
				success = true;
			}
			catch (Exception e) {
				LOGGER.throwing(CNAME, null, e);
				success = false;
			}
			finally {
				try {ecb.close();} catch (Exception e) {}
				try {zia.close();} catch (Exception e) {}
				try {raf.close();} catch (Exception e) {}
				if (isTmp && !file.delete()) {
					file.deleteOnExit();
				}
			}
		}
		return success;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(type);
		sb.append(":");
		sb.append(file);
		if (isTmp) {
			sb.append(" {TMP}");
		}
		if (isZip) {
			sb.append(" {ZIP}");
		}
		sb.append(" [");
		sb.append(name);
		sb.append(":");
		if (indexes != null) {
			sb.append(indexes[0]);
			for (int i = 1; i < indexes.length; i++) {
				sb.append(",");
				sb.append(indexes[i]);
			}
		} else {
			sb.append("full");
		}
		sb.append("]");
		return sb.toString();
	}
}