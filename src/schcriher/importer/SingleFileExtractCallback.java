package schcriher.importer;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import net.sf.sevenzipjbinding.ExtractAskMode;
import net.sf.sevenzipjbinding.ExtractOperationResult;
import net.sf.sevenzipjbinding.IArchiveExtractCallback;
import net.sf.sevenzipjbinding.ISequentialOutStream;
import net.sf.sevenzipjbinding.SevenZipException;

public class SingleFileExtractCallback implements IArchiveExtractCallback, ISequentialOutStream {

	private final BufferedOutputStream bos;
	private final FileOutputStream fos;

    public SingleFileExtractCallback(File file) throws FileNotFoundException {
    	fos = new FileOutputStream(file);
		bos = new BufferedOutputStream(fos);
    }

    @Override
    public ISequentialOutStream getStream(int index, ExtractAskMode mode) throws SevenZipException {
    	if (mode != ExtractAskMode.EXTRACT) {
    		return null;
    	}
    	return this;
    }

	@Override
	public void setOperationResult(ExtractOperationResult result) throws SevenZipException {
		if (result != ExtractOperationResult.OK) {
			throw new SevenZipException("Extraction error, result " + result);
        }
	}

	@Override
	public void prepareOperation(ExtractAskMode mode) throws SevenZipException {}

	@Override
	public void setCompleted(long value) throws SevenZipException {}

	@Override
	public void setTotal(long total) throws SevenZipException {}

	@Override
	public int write(byte[] data) throws SevenZipException {
		try {
			bos.write(data);
		}
		catch (IOException e) {
			throw new SevenZipException(e);
		}
		return data.length;
	}

	public void close() throws SevenZipException {
		if (bos != null) {try {bos.close();} catch (Exception e) {throw new SevenZipException(e);}}
		if (fos != null) {try {fos.close();} catch (Exception e) {throw new SevenZipException(e);}}
	}
}