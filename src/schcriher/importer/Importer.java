package schcriher.importer;

import static schcriher.waitdialogs.Publish.Type.COMPLETE;
import static schcriher.waitdialogs.Publish.Type.MAJOR;
import static schcriher.waitdialogs.Publish.Type.MAJORSTEP;
import static schcriher.waitdialogs.Publish.Type.MINOR;
import static schcriher.waitdialogs.Publish.Type.MINOREXTEND;
import static schcriher.waitdialogs.Publish.Type.MINORFLUSH;
import static schcriher.waitdialogs.Publish.Type.MINORSTEP;
import static schcriher.waitdialogs.Publish.Type.SETINFO;
import static schcriher.waitdialogs.Publish.Type.SETSUBTEXT;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.sevenzipjbinding.ArchiveFormat;
import net.sf.sevenzipjbinding.IInStream;
import net.sf.sevenzipjbinding.ISevenZipInArchive;
import net.sf.sevenzipjbinding.PropID;
import net.sf.sevenzipjbinding.SevenZip;
import net.sf.sevenzipjbinding.SevenZipException;
import net.sf.sevenzipjbinding.impl.RandomAccessFileInStream;
import schcriher.Filenames;
import schcriher.LAD;
import schcriher.Schcriher;
import schcriher.generic.AlphabeticalOrder;
import schcriher.packs.ConverterDialog;
import schcriher.utils.Archives;
import schcriher.utils.Archives.Operation;
import schcriher.utils.Utils;
import schcriher.utils.Zip;
import schcriher.waitdialogs.Publisher;

public class Importer extends Publisher {

	public static enum Type {WORLD, RESOURCE, TEXTURE, MOD, AMBIGUOUS}

	private static final String CNAME = Importer.class.getName();
	private static final Logger LOGGER = Logger.getLogger(CNAME);
	private static final int MAX_DEPTH = 3;

	private static final Path RESOURCE_PARTICLES_PATH = Paths.get("/assets/minecraft/textures/particle/particles.png");
	private static final Path RESOURCE_TERRAIN_PATH = Paths.get("/assets/minecraft/terrain.png");
	private static final Path ROOT_PATH = Paths.get("/");

	private static final Pattern IGNORED;
	static {
		IGNORED = Pattern.compile(
			String.format("\\b(?:(?<dev>%s)|(?<forge>(?:minecraft)?forge))\\b", Utils.regexForPartialName("development", 3)),
			Pattern.CASE_INSENSITIVE
		);
	}

	private final Map<Integer, Import> mapWorlds = new HashMap<Integer, Import>();
	private final Map<Integer, Import> mapResources = new HashMap<Integer, Import>();
	private final Map<Integer, Import> mapTextures = new HashMap<Integer, Import>();
	private final Map<Integer, Import> mapMods = new HashMap<Integer, Import>();

	private final List<String> messages = new ArrayList<String>();
	private final List<String> unrecognized = new ArrayList<String>();
	private final List<String> errors = new ArrayList<String>();
	private final List<File> files = new ArrayList<File>();

	@SuppressWarnings("rawtypes")
	private final Map<Path, Map> ignored = new HashMap<Path, Map>();

	private final Stack<Path> names = new Stack<Path>();
	private final Runtime runtime = Runtime.getRuntime();

	private final Schcriher core;
	private final List<File> sFiles;

	private File rootFile;
	private boolean runtimeException = false;

	public Importer(Schcriher core, String title, File[] sFiles) {
		super(core.mcparent, title);
		this.core = core;
		this.sFiles = Arrays.asList(sFiles);
	}

	@Override
	protected Void doInBackground() throws Exception {
		publish(MAJOR, sFiles.size() + 1);

		boolean develop = false;
		boolean forge = false;
		Matcher match;
		int nimport;

		Collections.sort(sFiles, new AlphabeticalOrder<File>());

		for (File f: sFiles) {
			publish(MAJORSTEP, f.getName());
			publish(SETINFO);

			match = IGNORED.matcher(f.getPath());
			if (match.find()) {
				if (match.group("dev") != null)
					develop = true;
				if (match.group("forge") != null)
					forge = true;
				ignored.put(f.toPath(), null);
				continue;
			}
			try {
				nimport = 0;
				for (Import imp: zip(f)) {
					switch (imp.type) {
					case WORLD:
						mapWorlds.put(files.size(), imp);
						files.add(new File(core.fSaves, imp.name));
						break;

					case RESOURCE:
						mapResources.put(files.size(), imp);
						files.add(new File(core.fResourcePacks, imp.name));
						break;

					case TEXTURE:
						mapTextures.put(files.size(), imp);
						files.add(new File(core.fTexturePacks, imp.name));
						break;

					case MOD:
						mapMods.put(files.size(), imp);
						files.add(new File(core.fMods, imp.name));
						break;

					case AMBIGUOUS:
						throw new RuntimeException("Error type");
					}
					LOGGER.fine(String.format("Importado con éxito %s (%s)", f, imp.file));
					nimport += 1;
				}
				if (nimport == 0) {
					unrecognized.add(f.getPath());
				}
			} catch (Exception e) {
				if (isCancelled() || runtimeException) {
					String message;
					if (runtimeException) {
						message = "Ocurrio un error interno en la biblioteca SevenZipJBinding";
						LAD.showErrorMessageNotLog(mcparent, message);
					} else {
						message = "Cancelado";
					}
					publish(COMPLETE, message);
					Archives.recursivelyWitoutOwner(core.fTemporal, Operation.DELETE);
					return null;
				}
				unrecognized.add(f.getPath());
			}
		}

		publish(SETINFO);
		publish(SETSUBTEXT);
		publish(MAJORSTEP, "Análisis terminado, copiando archivos");

		if (Filenames.checkExistenceAndAskNewnames(core.mcparent, files)) {
			publish(MINOR, 7);

			dumpFiles(mapWorlds);    publish(MINORSTEP);
			dumpFiles(mapResources); publish(MINORSTEP);
			dumpFiles(mapTextures);  publish(MINORSTEP);
			dumpFiles(mapMods);      publish(MINORSTEP);

			if (!mapWorlds.isEmpty()) {
				core.worlds.listFiles();
			}
			publish(MINORSTEP);
			if (!mapMods.isEmpty()) {
				core.mods.listFiles();
			}
			publish(MINORSTEP);

			String question = "¿Desea convertir los paquetes de texturas a paquetes de recursos?";
			if (!mapTextures.isEmpty() && LAD.showConfirmDialogYesNo(core.mcparent, question)) {
				List<File> src = new ArrayList<File>();
				List<File> dst = new ArrayList<File>();
				File oldFile;
				File newFile;
				for (Integer index: mapTextures.keySet()) {
					oldFile = files.get(index);
					if (oldFile != null && oldFile.exists()) {
						newFile = new File(core.fResourcePacks, oldFile.getName());
						src.add(oldFile);
						dst.add(newFile);
					}
				}
				if (Filenames.checkExistenceAndAskNewnames(core.mcparent, dst)) {
					ConverterDialog dialog = new ConverterDialog(core, src, dst);
					dialog.run();
					core.packs.listFiles();
				}
			}
			publish(MINORSTEP);

			if (!mapResources.isEmpty() || !mapTextures.isEmpty()) {
				core.packs.listFiles();
			}
		}

		publish(COMPLETE, "Finalizado");

		setMessages("Estos archivos tubieron problemas", errors);
		setMessages("Estos archivos no fueron reconocidos", unrecognized);
		setMessages("Estos archivos fueron ignorados", extractIgnoredFiles(ignored));
		if (!ignored.isEmpty()) {
			if (!messages.isEmpty()) {
				messages.add("");
			}
			messages.add("Estos subarchivos fueron ignorados:");
			setIgnoredSubFilesInMessages(ignored, "");
		}
		if (develop || forge) {
			messages.add("");
			messages.add("NOTA:");
			if (develop) messages.add("  * Los paquetes 'development' son para desarrollo.");
			if (forge)   messages.add("  * Forge debe instalarse con su propio instalador.");
		}
		if (!messages.isEmpty()) {
			if (errors.isEmpty()) {
				LAD.showInformationMessage(core.mcparent, messages);
			} else {
				LAD.showErrorMessage(core.mcparent, messages);
			}
		}

		Archives.recursivelyWitoutOwner(core.fTemporal, Operation.DELETE);
		return null;
	}

	private void setMessages(String string, List<String> list) {
		if (!list.isEmpty()) {
			if (!messages.isEmpty()) {
				messages.add("");
			}
			messages.add(string + ":");
			messages.addAll(list);
		}
	}

	@SuppressWarnings({ "rawtypes" })
	private List<String> extractIgnoredFiles(Map<Path, Map> map) {
		List<String> ignoredFiles = new ArrayList<String>();
		for (Iterator<Map.Entry<Path, Map>> it = map.entrySet().iterator(); it.hasNext();) {
			Map.Entry<Path, Map> entry = it.next();
			Map m = entry.getValue();
			if (m == null || m.isEmpty()) {
				ignoredFiles.add(entry.getKey().toString());
				it.remove();
			}
		}
		return ignoredFiles;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void setIgnoredSubFilesInMessages(Map<Path, Map> map, String offset) {
		int n = map.size();
		int i = 0;
		for (Entry<Path, Map> entry: map.entrySet()) {
			String sym = ++i < n ? " ├─" : " └─";
			messages.add(String.format("%s%s%s", offset, sym, entry.getKey()));
			Map subMap = entry.getValue();
			String sep = i < n ? " │ " : "   ";
			if (subMap != null) {
				setIgnoredSubFilesInMessages(subMap, offset + sep);
			}
		}
	}

	private void dumpFiles(Map<Integer, Import> map) {
		File dst;
		for (Map.Entry<Integer, Import> entry: map.entrySet()) {
			dst = files.get(entry.getKey());
			if (dst != null) {
				if (!entry.getValue().dump(dst)) {
					errors.add(dst.getPath());
				}
			}
		}
	}

	public boolean isRuntimeException() {
		return runtimeException;
	}

	private List<Import> zip(File file) throws IOException, InterruptedException, SevenZipException {
		RandomAccessFile raf = null;
		try {
			rootFile = file;
			names.clear();
			names.push(file.toPath());
			if (file.isDirectory()) {
				File tmp = File.createTempFile("SevenZipExtract.", ".tmp");
				if (Zip.compressor(file, tmp)) {
					file = tmp;
				} else {
					throw new IOException("Could not create the temporary file to the folder " + file);
				}
			}
			raf = new RandomAccessFile(file, "r");
			return zip(file, 0, new RandomAccessFileInStream(raf));
		}
		finally {
			try {raf.close();} catch (Exception e) {}
		}
	}

	private List<Import> zip(File file, int depth, IInStream randomAccessInStream) throws InterruptedException, SevenZipException {

		final Map<Path, List<Import>> possibleSubImports = new HashMap<Path, List<Import>>();
		final Map<Path, Type> possibleImports = new HashMap<Path, Type>();
		final Map<Path, Integer> foldersIndexes = new HashMap<Path, Integer>();
		final Map<Path, Integer> filesIndexes = new HashMap<Path, Integer>();
		final Map<Path, Long> filesSizes = new HashMap<Path, Long>();

		final Set<Path> folderClassFiles = new HashSet<Path>();
		final Set<Path> folderJavaFiles = new HashSet<Path>();
		final Set<Path> ignoredFolders = new HashSet<Path>();

		final List<Integer> possibleIgnoredIndexes = new ArrayList<Integer>();
		final List<Import> imports = new ArrayList<Import>();

		ISevenZipInArchive zia = null;

		try {
			Path keyPath;
			String strPath;

			zia = SevenZip.openInArchive(null, randomAccessInStream);
			boolean isZip = zia.getArchiveFormat() == ArchiveFormat.ZIP;
			boolean isTmp = file != rootFile;

			int nitems = zia.getNumberOfItems();
			possibleIgnoredIndexes.addAll(Utils.rangeList(nitems));

			if (depth == 0) {
				publish(MINOR, nitems + 6, "");
			} else {
				publish(MINOREXTEND, nitems + 6, "depth="+depth);
			}

			for (int i = 0; i < nitems; i++) {
				publish(MINORSTEP);
				strPath = (String) zia.getProperty(i, PropID.PATH);
				if (strPath != null) {
					//possibleIgnoredIndexes.add(i);
					keyPath = Paths.get("/" + strPath);
					if ((boolean) zia.getProperty(i, PropID.IS_FOLDER)) {
						foldersIndexes.put(keyPath, i);
						if (IGNORED.matcher(strPath).find()) {
							ignoredFolders.add(keyPath);
						}
						if (keyPath.getFileName().equals("META-INF")) {
							possibleImports.put(keyPath.getParent(), Type.MOD);
						}
					} else {
						filesIndexes.put(keyPath, i);
						filesSizes.put(keyPath, (long) zia.getProperty(i, PropID.SIZE));
						if (strPath.endsWith(".class")) {
							folderClassFiles.add(keyPath.getParent());
						}
						else if (strPath.endsWith(".java")) {
							folderJavaFiles.add(keyPath.getParent());
						}
					}
				}
			}

			boolean isData;
			String name;
			Path folder;
			Path root = Paths.get("/");

			int n = filesIndexes.size();
			publish(MINOREXTEND, n);
			List<Path> paths = new ArrayList<Path>(n);
			paths.addAll(filesIndexes.keySet());
			Collections.sort(paths, new AlphabeticalOrder<Path>());

			for (Path path: paths) {

				publish(MINORSTEP);
				publish(SETINFO, String.format("%s%s", depth == 0 ? "" : String.format("[%s]: ", names.lastElement()), path));

				if (!isChild(path, possibleImports.keySet()) && !isChild(path, ignoredFolders)) {

					name = path.getFileName().toString();
					folder = path.getParent();
					isData = false;

					switch(name) {
					case "level.dat":
					case "level.dat_old":
						possibleImports.put(folder, Type.WORLD);
						isData = true;
						break;

					case "mcmod.info":
						possibleImports.put(folder, Type.MOD);// puede ser un MOD o su código fuente
						break;

					case "pack.txt":
						possibleImports.put(folder, Type.TEXTURE);
						isData = true;
						break;

					case "pack.mcmeta":
						possibleImports.put(folder, Type.AMBIGUOUS);// puede ser un RESOURCE o un MOD
						break;

					case "particles.png":
						if (path.endsWith(RESOURCE_PARTICLES_PATH)) {
							int c = path.getNameCount() - RESOURCE_PARTICLES_PATH.getNameCount();
							Path f = Paths.get("/", (c > 0) ? path.subpath(0, c).toString() : "");
							possibleImports.put(f, Type.RESOURCE);
						} else {
							possibleImports.put(folder, Type.TEXTURE);
						}
						isData = true;
						break;

					case "terrain.png":
						if (path.endsWith(RESOURCE_TERRAIN_PATH)) {
							int c = path.getNameCount() - RESOURCE_TERRAIN_PATH.getNameCount();
							Path f = Paths.get("/", (c > 0) ? path.subpath(0, c).toString() : "");
							possibleImports.put(f, Type.RESOURCE);
						} else {
							possibleImports.put(folder, Type.TEXTURE);
						}
						isData = true;
						break;

					default:
						if (depth < MAX_DEPTH) {
							names.push(path);
							File dst = null;
							try {
								dst = File.createTempFile("SevenZipExtract.", ".tmp", core.fTemporal);
								long size = filesSizes.get(path);
								int[] ind = new int[] {filesIndexes.get(path)};
								List<Import> subImports;
								if (runtime.freeMemory() > size) {
									byte[] buffer = new byte[Utils.safeLongToInt(size)];
									MemoryExtractCallback callback = new MemoryExtractCallback(buffer);
									zia.extract(ind, false, callback);
									subImports = zip(dst, depth + 1, new RandomAccessMemoryInStream(buffer));
									if (!subImports.isEmpty()) {
										callback.dump(dst);
									}
								} else {
									SingleFileExtractCallback callback = new SingleFileExtractCallback(dst);
									zia.extract(ind, false, callback);
									callback.close();
									RandomAccessFile raf = new RandomAccessFile(dst, "r");
									subImports = zip(dst, depth + 1, new RandomAccessFileInStream(raf));
								}
								if (subImports.isEmpty()) {
									throw new Exception("Path unrecognized");
								} else {
									possibleSubImports.put(path, subImports);
								}
							}
							catch (Exception e) {
								if (dst != null && !dst.delete()) {
									dst.deleteOnExit();
								}
							}
							names.pop();
						}
					}
					if (isData && folder.equals(root)) {
						publish(MINORFLUSH);
						break;
					}
				}
			}

			Path path;
			Path namePath;
			Type type;

			publish(MINORSTEP);

			for (Iterator<Map.Entry<Path, Type>> it = possibleImports.entrySet().iterator(); it.hasNext();) {
				Map.Entry<Path, Type> entry = it.next();
				path = entry.getKey();
				for (Path _path: possibleImports.keySet()) {
					if (!path.equals(_path) && path.startsWith(_path)) {
						it.remove();
					}
				}
			}

			publish(MINORSTEP);

			for (Iterator<Map.Entry<Path, Type>> it = possibleImports.entrySet().iterator(); it.hasNext();) {
				Map.Entry<Path, Type> entry = it.next();
				path = entry.getKey();
				type = entry.getValue();
				if (isParent(path, folderJavaFiles) || isChild(path, possibleSubImports.keySet())) {
					it.remove();
				} else if (type == Type.AMBIGUOUS) {
					possibleImports.put(path, isParent(path, folderClassFiles) ? Type.MOD : Type.RESOURCE);
				}
			}

			publish(MINORSTEP);

			if (possibleImports.isEmpty() && !folderClassFiles.isEmpty()) {
				possibleImports.put(ROOT_PATH, Type.MOD);
			}

			publish(MINORSTEP);

			for (Entry<Path, List<Import>> entry: possibleSubImports.entrySet()) {
				path = entry.getKey();
				if (!isChild(path, possibleImports.keySet())) {
					imports.addAll(entry.getValue());
					possibleIgnoredIndexes.remove(filesIndexes.get(path));
				}
			}

			publish(MINORSTEP);

			for (Map.Entry<Path, Type> entry: possibleImports.entrySet()) {
				path = entry.getKey();
				type = entry.getValue();
				namePath = path.getFileName();
				name = (namePath != null) ? namePath.toString() : names.lastElement().getFileName().toString();
				if (type == Type.WORLD)
					name = Archives.extractExtension(name);
				int[] indexes = getIndexes(path, nitems, filesIndexes, foldersIndexes);
				imports.add(new Import(file, path, name, type, isZip, isTmp, indexes.length == nitems ? null : indexes));
				possibleIgnoredIndexes.removeAll(Utils.integerArrayToList(indexes));
			}

			publish(MINORSTEP);

			List<Path> ignoredPaths = getPaths(possibleIgnoredIndexes, filesIndexes);
			int nIgnored = ignoredPaths.size();
			if (nIgnored > 0) {
				setIgnored(nIgnored >= filesIndexes.size() ? null : ignoredPaths);
			}
		}
		catch (RuntimeException e) {
			LOGGER.throwing(CNAME, null, e);
			runtimeException = true;
			throw e;
		}
		catch (InterruptedException e) {
			throw e;
		}
		catch (SevenZipException e) {
			// Posiblemente este archivo no se puede descomprimir
		}
		finally {
			try {zia.close();} catch (Exception e) {}
		}
		return imports;
	}

	private List<Path> getPaths(List<Integer> indexes, final Map<Path, Integer> map) {
		List<Path> paths = new ArrayList<Path>();
		for (Map.Entry<Path, Integer> entry: map.entrySet()) {
			if (indexes.contains(entry.getValue())) {
				paths.add(entry.getKey());
			}
		}
		return paths;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void setIgnored(List<Path> paths) {
		Map<Path, Map> map = ignored;
		for (Path path: names) {
			if (map.containsKey(path)) {
				map = map.get(path);
			} else {
				Map<Path, Map> m = new HashMap<Path, Map>();
				map.put(path, m);
				map = m;
			}
		}
		if (paths != null) {
			for (Path path: paths) {
				map.put(path, null);
			}
		}
	}

	public static boolean isParent(final Path path, final Set<Path> set) {
		for (Path p : set) {
			if (p.startsWith(path)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isChild(final Path path, final Set<Path> set) {
		for (Path p : set) {
			if (path.startsWith(p)) {
				return true;
			}
		}
		return false;
	}

	private int[] getIndexes(final Path path, final int n, final Map<Path, Integer> map1, final Map<Path, Integer> map2) {
		List<Integer> indexes = new ArrayList<Integer>();
		getIndexes(path, map1, indexes);
		getIndexes(path, map2, indexes);
		int[] array = new int[indexes.size()];
		int i = 0;
		for (Integer e: indexes) {
			array[i++] = e;
		}
		Arrays.sort(array);
		return array;
	}

	private void getIndexes(final Path path, final Map<Path, Integer> map, final List<Integer> list) {
		for (Map.Entry<Path, Integer> entry: map.entrySet()) {
			if (entry.getKey().startsWith(path)) {
				list.add(entry.getValue());
			}
		}
	}
}