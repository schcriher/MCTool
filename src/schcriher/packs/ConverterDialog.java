package schcriher.packs;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Cursor;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import javax.swing.JDialog;
import javax.swing.JPanel;

import net.minecraft.util.gui.UnstitcherGui;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import schcriher.LAD;
import schcriher.Schcriher;
import schcriher.utils.Archives;
import schcriher.utils.Zip;
import schcriher.waitdialogs.WaitDialog;

import com.google.common.collect.Lists;
import com.mojang.minecraft.textureender.ConverterGui;
import com.mojang.minecraft.textureender.ConverterTask;
import com.mojang.minecraft.textureender.tasks.ConvertTxtToMcmetaTask;
import com.mojang.minecraft.textureender.tasks.DeleteEmptyDirectoriesTask;

public class ConverterDialog extends JDialog implements Runnable {

	private static final long serialVersionUID = 1L;
	private static final String title = "Conversor de paquetes de texturas";
	private static final boolean modal = true;

	private static final String CNAME = ConverterDialog.class.getName();
	private static final Logger LOGGER = Logger.getLogger(CNAME);

	private final WaitDialog cancelDialog;
	volatile private boolean canceled;

	private final Schcriher core;
	private final List<File> src;
	private final List<File> dst;
	private final JPanel panels = new JPanel();
	private final CardLayout layout = new CardLayout(0, 0);
	private final UnstitcherGui unstitcher = new UnstitcherGui();  // to 1.5
	private final ConverterGui converter = new ConverterGui();     // to 1.6
	private final List<ConverterTask> CONVERTER_TASKS = Lists.newArrayList();

	public ConverterDialog(final Schcriher core, List<File> src, List<File> dst) {
		super(core.mcparent, title, modal);

		this.core = core;
		this.src = src;
		this.dst = dst;

		panels.setLayout(layout);
		panels.add(unstitcher, "unstitcher");
		panels.add(converter, "converter");

		cancelDialog = new WaitDialog(core.mcparent, "Cancelando conversión, espere...");
		canceled = false;

		CONVERTER_TASKS.add(new ConvertTxtToMcmetaTask());
		CONVERTER_TASKS.add(new DeleteEmptyDirectoriesTask());

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(panels, BorderLayout.CENTER);

		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setResizable(false);
		pack();
		setSize(854, 480);
		setLocationRelativeTo(core.mcparent);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (LAD.showConfirmDialogYesNo(core.mcparent, "¿Desea cancelar la conversión?")) {
					canceled = true;
					new Thread(cancelDialog).start();
				}
			}
		});
	}

	@Override
	public void run() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				exec();
				dispose();
				cancelDialog.dispose();
			}
		}).start();
		setVisible(true);
	}

	private void exec() {
		List<File> convert = new ArrayList<File>();
		List<File> zipunzip = new ArrayList<File>();
		List<File> okeyfile = new ArrayList<File>();
		List<File> deletefile = new ArrayList<File>();
		List<String> errors = new ArrayList<String>();

		int n = src.size();
		for (int i = 0; i < n; i++) {
			File oldFile = src.get(i);
			File newFile = dst.get(i);
			if (canceled) {
				for (File f: convert) {
					Archives.Operation.DELETE.make(f);
				}
				return;
			}
			if (newFile != null) {
				if (oldFile.isDirectory()) {
					File tmp;
					try {
						tmp = File.createTempFile("ConverterDirZip.", ".zip", core.fTemporal);
					}
					catch (IOException e) {
						e.printStackTrace();
						errors.add(oldFile.getPath());
						continue;
					}
					if (Zip.compressor(oldFile, tmp)) {
						zipunzip.add(newFile);
						deletefile.add(tmp);
						oldFile = tmp;
					} else {
						errors.add(oldFile.getPath());
						continue;
					}
				}
				if (exec_unstitcher(oldFile, newFile)) {
					convert.add(newFile);
				} else {
					errors.add(oldFile.getPath());
				}
			}
		}
		for (File file: convert) {
			if (canceled) {return;}
			if (exec_converter(file)) {
				okeyfile.add(file);
			} else {
				errors.add(file.getPath());
			}
		}
		for (File file: okeyfile) {
			if (zipunzip.contains(file)) {
				File tmp;
				try {
					tmp = Files.createTempDirectory(core.fTemporal.toPath(), "ConverterZipDir.").toFile();
				}
				catch (IOException e) {
					e.printStackTrace();
					errors.add(file.getPath());
					continue;
				}
				if (tmp.exists()) {
					Archives.recursively(tmp, Archives.Operation.DELETE);
				}
				if (!(Zip.decompressor(file, tmp) && Archives.move(tmp, file, true))) {
					errors.add(file.getPath());
				}
			}
		}
		for (File file: deletefile) {
			Archives.recursively(file, Archives.Operation.DELETE);
		}
		if (!errors.isEmpty()) {
			errors.add(0, "Los siguientes archivos no se pudieron convertir:");
			errors.add(1, "");
			LAD.showErrorMessage(core.mcparent, errors);
		}
	}

	private boolean exec_unstitcher(File oldFile, File newFile) {
		layout.show(panels, "unstitcher");
		try {
			unstitcher.unstitch(oldFile, newFile);
			unstitcher.log("All done!");
			unstitcher.log("Your items.png and terrain.png have been replaced with any images not cut from the image.");
			unstitcher.log("The unstitched images can be found in textures/blocks/*.png and textures/items/*.png respectively.");
		}
		catch (Throwable t) {
			unstitcher.log("Error unstitching file!");
			unstitcher.log(unstitcher.getExceptionMessage(t));
			unstitcher.log("Stop.");
			LOGGER.throwing(CNAME, "exec_unstitcher", t);
			return false;
		}
		return true;
	}

	private boolean exec_converter(File file) {
		layout.show(panels, "converter");
		File tmp;
		try {
			tmp = Files.createTempDirectory(core.fTemporal.toPath(), "Converter.").toFile();
		}
		catch (IOException e) {
			ConverterGui.logLine("Couldn't created the temp folder in " + core.fTemporal, e);
			return false;
		}
		try {
			ConverterGui.logLine("Extracting " + file);
			extractZip(file, tmp);
		}
		catch (IOException e) {
			ConverterGui.logLine("Couldn't extract " + file + " to " + tmp, e);
			return false;
		}
		runConversions(tmp);
		try {
			ConverterGui.logLine("Re-zipping " + tmp);
			createZip(tmp, file);
			ConverterGui.logLine("Cleaning up, deleting " + tmp);
			FileUtils.deleteQuietly(tmp);
		}
		catch (IOException e) {
			ConverterGui.logLine("Couldn't zip " + tmp + " to " + file, e);
		}
		return true;
	}

	private void extractZip(File source, File output) throws IOException {
		ZipFile zip = new ZipFile(source);
		Enumeration<?> entries = zip.entries();
		while (entries.hasMoreElements()) {

			ZipEntry entry = (ZipEntry) entries.nextElement();
			File dst = new File(output, entry.getName());

			File parent = dst.getParentFile();
			if (parent != null)
				parent.mkdirs();

			if (!entry.isDirectory()) {
				FileUtils.copyInputStreamToFile(zip.getInputStream(entry), dst);
			}
		}
		try {zip.close();} catch (Exception e) {}
	}

	private void runConversions(File folder) {
		ConverterGui.logLine("Starting conversion on " + folder.getAbsolutePath());
		List<ConverterTask> tasks = Lists.newArrayList(CONVERTER_TASKS);
		for (int i = 0; i < tasks.size(); ++i) {
			ConverterTask task = tasks.get(i);
			ConverterGui.logLine("Starting task '" + task.getTaskName() + "'");
			List<ConverterTask> extraTasks = task.run(folder);
			if (extraTasks != null) {
				tasks.addAll(i + 1, extraTasks);
			}
			ConverterGui.logLine("Finished task '" + task.getTaskName() + "'");
		}
		ConverterGui.logLine("Finished converting " + folder.getAbsolutePath());
	}

	private void createZip(File source, File output) throws IOException {
		if (!Archives.Operation.DELETE.make(output)) {
			Archives.Operation.SET_READABLE.make(output);
			Archives.Operation.SET_WRITABLE.make(output);
			if (!Archives.Operation.DELETE.make(output)) {
				throw new IOException("Can not delete file " + output);
			}
		}
		ZipOutputStream out = new ZipOutputStream(FileUtils.openOutputStream(output));
		zipDirectory("", source, out);
		IOUtils.closeQuietly(out);
	}

	private void zipDirectory(String path, File directory, ZipOutputStream out) throws IOException {
		File[] files = directory.listFiles();
		if (files == null)
			return;
		for (File file: files) {
			if (file.isDirectory()) {
				String subpath = path + file.getName() + "/";
				out.putNextEntry(new ZipEntry(subpath));
				zipDirectory(subpath, file, out);
				out.closeEntry();
			}
			else {
				out.putNextEntry(new ZipEntry(path + file.getName()));
				FileUtils.copyFile(file, out);
				out.closeEntry();
			}
		}
	}
}