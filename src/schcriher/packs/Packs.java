package schcriher.packs;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import schcriher.DuplicateRemover;
import schcriher.Exporter;
import schcriher.Filenames;
import schcriher.LAD;
import schcriher.Resources;
import schcriher.Schcriher;
import schcriher.generic.AlphabeticalOrder;
import schcriher.generic.UnselectionTableMouseListener;
import schcriher.utils.Archives;
import schcriher.utils.Archives.Operation;

public class Packs extends JPanel implements ActionListener, ListSelectionListener {

	// http://minecraft.gamepedia.com/Resource_pack   (MC >= 1.6)  pack.mcmeta
	// http://minecraft.gamepedia.com/Texture_pack    (MC <= 1.5)  texture.txt

	private static final long serialVersionUID = 1L;

	private static final String CNAME = Packs.class.getName();
	private static final Logger LOGGER = Logger.getLogger(CNAME);

	private PacksTableModel modelResources;
	private PacksTableModel modelTextures;
	private JScrollPane scrollTextures;
	private JTable tableResources;
	private JTable tableTextures;
	private JButton button;

	public final Schcriher core;

    public Packs(Schcriher core) {
    	long start = System.currentTimeMillis();
    	this.core = core;
    	initComponents();
    	listFiles();
    	LOGGER.fine(String.format("Schcriher Packs Initialized in %dms", System.currentTimeMillis() - start));
    }

    public void listFiles() {
    	try {
    		modelResources.clear();
    		listPacks(core.fResourcePacks, true, modelResources);
    		listPacks(core.fResourcePacksBackups, false, modelResources);
    		modelTextures.clear();
    		listPacks(core.fTexturePacks, true, modelTextures);
    		listPacks(core.fTexturePacksBackups, false, modelTextures);
    		setTextureVisible();
    	}
    	catch (Exception e) {
    		LAD.showErrorMessage(core.mcparent, e.getLocalizedMessage());
    	}
    }

	private void listPacks(File file, boolean enable, PacksTableModel model) throws Exception {
		if (!file.exists()) {
    		file.mkdirs();
    	}
		if (!file.isDirectory()) {
    		throw new IOException("Could not create the folder " + file.getAbsolutePath());
    	}
		List<File> files = Arrays.asList(file.listFiles(new PacksFileFilter()));
		Collections.sort(files, new AlphabeticalOrder<File>());
		for (File f: files) {
			// Se asume que son todos Packs válidos, el boton de importación hace las comprobaciones
			model.addRow(enable, f, false);
		}
	}

    private void initComponents() {
        GridBagLayout layout = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();

        layout.columnWeights = new double[]{1.0, 0.0, 1.0};
        layout.rowWeights = new double[]{1.0, 0.0, 1.0};

        setLayout(layout);

        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.weighty = 1.0;
        c.weightx = 1.0;
        c.insets = new Insets(0, 0, 0, 0);
        c.fill = GridBagConstraints.BOTH;

        modelResources = new PacksTableModel("Nombre de los paquetes de recursos (MC > 1.6)");
        tableResources = new PacksTable(modelResources, "Resources");
        tableResources.addMouseListener(new UnselectionTableMouseListener());
        tableResources.setToolTipText("Unstitched packs");
        tableResources.setFillsViewportHeight(true);
        tableResources.setGridColor(Resources.TABLE_GRID_COLOR);
        tableResources.setShowGrid(true);
        tableResources.setShowVerticalLines(false);
        tableResources.setRowSelectionAllowed(true);
        tableResources.setColumnSelectionAllowed(false);
        tableResources.getSelectionModel().addListSelectionListener(this);
        tableResources.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        tableResources.getColumnModel().getColumn(0).setResizable(false);
        tableResources.getColumnModel().getColumn(0).setMaxWidth(27);
        tableResources.getColumnModel().getColumn(0).setCellEditor(new AvailableCellEditor(this));
        tableResources.getColumnModel().getColumn(1).setCellEditor(new FileNameCellEditor(this));
        tableResources.getColumnModel().getColumn(1).setCellRenderer(new FileNameCellRenderer());
        c.gridx = 0;
        c.gridheight = 3;
        add(new JScrollPane(tableResources), c);

        modelTextures = new PacksTableModel("Nombre de los paquetes de texturas (MC < 1.6)");
        tableTextures = new PacksTable(modelTextures, "Textures");
        tableTextures.addMouseListener(new UnselectionTableMouseListener());
        tableTextures.setToolTipText("Stitched packs");
        tableTextures.setFillsViewportHeight(true);
        tableTextures.setGridColor(Resources.TABLE_GRID_COLOR);
        tableTextures.setShowGrid(true);
        tableTextures.setShowVerticalLines(false);
        tableTextures.setRowSelectionAllowed(true);
        tableTextures.setColumnSelectionAllowed(false);
        tableTextures.getSelectionModel().addListSelectionListener(this);
        tableTextures.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        tableTextures.getColumnModel().getColumn(0).setResizable(false);
        tableTextures.getColumnModel().getColumn(0).setMaxWidth(27);
        tableTextures.getColumnModel().getColumn(0).setCellEditor(new AvailableCellEditor(this));
        tableTextures.getColumnModel().getColumn(1).setCellEditor(new FileNameCellEditor(this));
        tableTextures.getColumnModel().getColumn(1).setCellRenderer(new FileNameCellRenderer());
        c.gridx = 2;
        c.gridheight = 3;
        scrollTextures = new JScrollPane(tableTextures);
        add(scrollTextures, c);

        Component vglue = Box.createVerticalGlue();
        c.gridx = 1;
        c.gridy = 0;
        c.gridheight = 1;
        c.weighty = 0.0;
        c.weightx = 0.0;
        c.insets = new Insets(0, 5, 0, 5);
        add(vglue, c);

        button = new JButton();
        button.setIcon(Resources.ARROW_LEFT_ICON);
        button.setToolTipText("Convertir a paquetes de recursos");
        button.addActionListener(this);
        button.setEnabled(false);
        c.insets = new Insets(0, 5, 0, 5);
        c.gridy = 1;
        add(button, c);

        c.gridx = 1;
        c.gridy = 2;
        add(vglue, c);
    }

    public void setTextureVisible() {
    	boolean b = modelTextures.getRowCount() > 0;
    	scrollTextures.setVisible(b);
    	button.setVisible(b);
    }

    public boolean setAvailableResourcePack(boolean available, int rowIndex) {
    	return setAvailablePack(available, rowIndex, modelResources, core.fResourcePacks, core.fResourcePacksBackups);
    }

    public boolean setAvailableTexturePack(boolean available, int rowIndex) {
    	return setAvailablePack(available, rowIndex, modelTextures, core.fTexturePacks, core.fTexturePacksBackups);
    }

    public boolean setAvailablePack(boolean available, int rowIndex, PacksTableModel model, File fPacks, File fPacksBackups) {
    	if (!model.getLockRow(rowIndex)) {
    		File oldFile = model.getFile(rowIndex);
    		File newFile = new File(available ? fPacks : fPacksBackups, oldFile.getName());
    		if (!oldFile.equals(newFile)) {
    			if (Archives.move(oldFile, newFile, true)) {
    				model.setAvailable(available, rowIndex);
    				model.setFile(newFile, rowIndex);
    				return true;
    			} else {
    				model.setLockRow(true, rowIndex);
    				LAD.showRenameErrorMessageNotLog(core.mcparent, oldFile, newFile);
    			}
    		}
    	}
    	return false;
    }

    public void deleteRepeated() {
    	List<File> folders = new ArrayList<File>();
    	List<File> files = new ArrayList<File>();

    	int nr = modelResources.getRowCount();
    	for (int i = 0; i < nr; i++) {
    		File f = modelResources.getFile(i);
    		if (f.isDirectory()) {
    			folders.add(f);
    		} else {
    			files.add(f);
    		}
    	}

    	int nt = modelTextures.getRowCount();
    	for (int i = 0; i < nt; i++) {
    		File f = modelTextures.getFile(i);
    		if (f.isDirectory()) {
    			folders.add(f);
    		} else {
    			files.add(f);
    		}
    	}

    	DuplicateRemover remover = new DuplicateRemover(core.mcparent, files, folders);
    	remover.execute();
    	remover.setVisible();
    	if (remover.removed()) {
    		listFiles();
    	}
    }

	public void deleteSelected() {
		if (LAD.showConfirmDialogYesNo(core.mcparent, "¿Eliminar los archivos seleccionados?")) {
	    	List<Integer> deleteRowIndexes = new ArrayList<Integer>();
	    	List<String> nonDelete = new ArrayList<String>();

	    	for (int i: tableTextures.getSelectedRows()) {
	    		File file = modelTextures.getFile(i);
	    		if (Archives.recursively(file, Operation.DELETE)) {
	    			deleteRowIndexes.add(i);
	    		} else {
	    			modelTextures.setLockRow(true, i);
	    			nonDelete.add(file.getPath());
	    		}
	    	}
	    	modelTextures.removeRows(deleteRowIndexes);

	    	deleteRowIndexes.clear();
			for (int i: tableResources.getSelectedRows()) {
				File file = modelResources.getFile(i);
				if (Archives.recursively(file, Operation.DELETE)) {
					deleteRowIndexes.add(i);
				} else {
					modelResources.setLockRow(true, i);
					nonDelete.add(file.getPath());
				}
			}
			modelResources.removeRows(deleteRowIndexes);

			if (!nonDelete.isEmpty()) {
				nonDelete.add(0, "Los siguientes archivos no se pudieron eliminar:");
				nonDelete.add(1, "");
	    		LAD.showErrorMessage(core.mcparent, nonDelete);
	    	}
		}
	}

    public void exportSelected(File dst) {
    	final List<File> src = new ArrayList<File>();
    	for (int i: tableResources.getSelectedRows()) {
    		src.add(modelResources.getFile(i));
    	}
    	if (core.fTexturePacks != null) {
    		for (int i: tableTextures.getSelectedRows()) {
    			src.add(modelTextures.getFile(i));
    		}
    	}
    	Exporter exporter = new Exporter(core.mcparent, dst, src);
    	exporter.execute();
    	exporter.setVisible();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    	Object source = e.getSource();
    	if (source == button) {
    		if (LAD.showConfirmDialogYesNo(core.mcparent, "¿Convertir a paquetes de recursos?")) {
    			List<File> src = new ArrayList<File>();
    			List<File> dst = new ArrayList<File>();
    			File oldFile;
				File dirName;
				File newFile;
    			for (int i: tableTextures.getSelectedRows()) {
    				oldFile = modelTextures.getFile(i);
    				dirName = modelTextures.getAvailable(i) ? core.fResourcePacks : core.fResourcePacksBackups;
    				newFile = new File(dirName, oldFile.getName());
    				src.add(oldFile);
    				dst.add(newFile);
    			}
    			if (Filenames.checkExistenceAndAskNewnames(core.mcparent, dst)) {
    				ConverterDialog dialog = new ConverterDialog(core, src, dst);
        			dialog.run();
        			listFiles();
    			}
    		}
    	}
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
    	if (!e.getValueIsAdjusting()) {
    		Object source = e.getSource();
    		if (source == tableTextures.getSelectionModel()) {
    			if (tableTextures.getSelectedRowCount() > 0) {
    				button.setEnabled(true);
    			} else {
    				button.setEnabled(false);
    			}
    		}
    		core.setEnabledExportButton(tableResources.getSelectedRowCount() + tableTextures.getSelectedRowCount() > 0);
    	}
    }
}