package schcriher.packs;

import java.io.File;
import java.io.FileFilter;

public class PacksFileFilter implements FileFilter {

	private static final String[] paths = new String[] {
		"assets",
		"pack.txt",
		"pack.png",
		"pack.mcmeta",
		"texture.txt",
		"terrain.png",
		"particles.png",
		"assets/minecraft/terrain.png",
		"assets/minecraft/textures/particle/particles.png",
	};

	@Override
	public boolean accept(File file) {
		if (file.isFile()) {
			String name = file.getName().toLowerCase();
			return name.endsWith(".jar") || name.endsWith(".zip");
		}
		else if (file.isDirectory()) {
			for (String path: paths) {
				File f = new File(file, path);
				if (f.exists()) {
					return true;
				}
			}
		}
		return false;
	}
}