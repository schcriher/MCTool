# MCTools
Basic tool to manage Minecraft [_Herramienta básica para administrar Minecraft_].

## Functionalities [_Funcionalidades_]
* Backups of all worlds [_Copias de seguridad de los mundos_].
* Importing worlds, packs and mods [_Importación de mundos, paquetes y mods_].
* Export worlds, packs and mods [_Exportación de mundos, paquetes y mods_].
* Enable/disable worlds, packs and mods [_Activar/desactivar mundos, paquetes y mods_].
* Deleting worlds packs, mods and settings [_Borrado de mundos, paquetes, mods y configuraciones_].
* Texture pack converter [_Conversor de paquetes de texturas_].
* Basic editor worlds [_Editor básico de mundos_].

## External Tools [_Herramientas externas_]
* 7-Zip-JBinding <http://sevenzipjbind.sourceforge.net/>
* TextureEnder <https://mojang.com/2013/06/minecraft-snapshot-13w24a/>
* Texture Unstitcher <http://assets.minecraft.net/unstitcher/unstitcher.jar>
